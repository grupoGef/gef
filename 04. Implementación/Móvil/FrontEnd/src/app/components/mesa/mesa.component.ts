import {Component, Input, OnInit} from '@angular/core';
import {Mesa} from '../../interfaces/interfaces';
import {ActionSheetController} from '@ionic/angular';
import {DataLocalService} from '../../services/data-local.service';

@Component({
  selector: 'app-mesa',
  templateUrl: './mesa.component.html',
  styleUrls: ['./mesa.component.scss'],
})
export class MesaComponent implements OnInit {

  @Input() mesa: Mesa;
  @Input() enFavoritos;
  @Input() novedad;

  constructor(private actionSheetCtrl: ActionSheetController,
              private datalocalService: DataLocalService ) { }

  ngOnInit() {
    console.log('Favoritos', this.enFavoritos);
    console.log('novedad', this.novedad);
  }

  async lanzarMenu() {

    let guardarBorrarBtn;
    if(this.enFavoritos) {
      guardarBorrarBtn = {
        text: 'Borrar Favorito',
        icon: 'trash',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Borrar Favorito');
          this.datalocalService.borrarMesa( this.mesa );
        }
      };
    }else{
      guardarBorrarBtn = {
        text: 'Favorito',
          icon: 'star',
        cssClass: 'action-dark',
        handler: () => {
        console.log('Favorite clicked');
        this.datalocalService.guardarMesa( this.mesa );
        }
      };
    }
    const actionSheet = await this.actionSheetCtrl.create({
      buttons: [
        guardarBorrarBtn,
        {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
}
