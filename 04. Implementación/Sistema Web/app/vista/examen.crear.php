<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARMESAEXAMEN);
include_once '../modelo/ColeccionDocentes.php';
include_once '../modelo/ColeccionAsignaturas.php';
include_once '../modelo/ColeccionLlamado.php';
include_once '../modelo/ColeccionCarrera.php';




?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2-bootstrap.min.css">
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/select2.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/i18n/es.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/jszip.min.js"></script>
    <script src="../../lib/dataTables/pdfmake.min.js"></script>
    <script src="../../lib/dataTables/vfs_fonts.js"></script>
    <script src="../../lib/dataTables/buttons.flash.min.js"></script>
    <script src="../../lib/dataTables/buttons.html5.min.js"></script>
    <script src="../../lib/dataTables/buttons.print.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/gestionexamen.js"></script>

        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Crear Mesa de Examen</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="examen.crear.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                        <p><center><h3>Agregar nueva Mesa de Examen</h3></center></p>
                        
                        <p>
                            Por favor, complete los campos a continuaci&oacute;n.
                            Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                            Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                        </p>
                    </div>
                    <div class="card-body">
                        <h4>Información de la Mesa</h4><br>
                        <div class="form-row">
                             <label for="selectTipoBusqueda">Seleccione Carrera</label>
                             <select class="selectCarrera form-control" name="selectCarrera">
                            
                            </select>
                        </div>
                        <br>
                        <div class="form-row">
                             <label for="selectTipoBusqueda">Seleccione Asignatura</label>
                             <select class="selectAsignatura form-control" name="selectAsignatura" id="selectAsignatura">
                         
                            </select>
                        </div>
<br>
                        <div id="resultado" name="resultado">

                        </div>
                        <div class="form-row">
                             <label for="selectTipoBusqueda">Seleccione llamado</label>
                             <select class="selectllamado form-control" name="selectllamado" id="selectllamado">
                             <option value="1">Primer llamado</option>
                             <option value="2">Segundo llamado</option>
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                        <label for="selectTipoBusqueda">Seleccione fecha</label>
                             <select class="selectfecha form-control" name="selectfecha" id="selectfecha">
                             <option value="2022-02-14">2022/02/14</option>
                             <option value="2022-02-15">2022/02/15</option>
                             <option value="2022-02-16">2022/02/16</option>
                             <option value="2022-02-17">2022/02/17</option>
                             <option value="2022-02-18">2022/02/18</option>
                         
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="inputNombre">Hora</label>
                            <input type="time" name="hora" class="form-control" id="hora" placeholder="Ingrese hora" required=""min="13:00" max="21:00">
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <span class="oi oi-check"></span> Confirmar
                        </button>
                        <a href="gestionExamen.php">
                            <button type="button" class="btn btn-outline-danger">
                                <span class="oi oi-x"></span> Cancelar
                            </button>
                        </a>
                    </div>
                </div>
            </form>
       
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
