<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARNOVEDADES);

$nombredocente=null;
$fechafinalstring=null;
$fechainiciostring=null;
$cuerpodiv= '';




if(isset($_POST['docenteb'])) 
{
    $nombredocente=$_POST['docenteb'];

}
if(isset($_POST['fechainiciob'])) 
{
    if ($_POST['fechainiciob'] != '')
    {
    $fecha1 = new DateTime($datetimecomparable= $_POST['fechainiciob']);
    $fechainiciostring = $fecha1->format('d-m-Y');
    }
}
if(isset($_POST['fechafinalb'])) 
{   
    if ($_POST['fechafinalb'] != '')
    {
    $fecha2 = new DateTime($datetimecomparable= $_POST['fechafinalb']);
    $fechafinalstring = $fecha2->format('d-m-Y');
    }
}









if($nombredocente != null OR $fechainiciostring !=null OR  $fechafinalstring != null )
{


    if($nombredocente!= null and $fechainiciostring == null and $fechafinalstring == null)
    {

        $SQL = "Select p.id,CONCAT(p.nombre,' ',p.apellido) as nombredocente,p.nombre,p.apellido,d.nombre as departamento,d.id as iddepartamento,l.id as idlicencia,l.fechaInicio,l.fechaFinal, tl.idLicencia as tipolicenciaid,tl.nombre as tipolicencianombre, tl.descripcion FROM licencia l left join profesor p on l.idProfesor=p.id left JOIN departamento d on p.idDepartamento=d.id left join tipo_licencia tl ON l.id=tl.idLicencia
        WHERE CONCAT(p.nombre,' ',p.apellido) like '%$nombredocente%'
        ";

    }
    elseif($nombredocente!= null and $fechainiciostring != null and $fechafinalstring == null)
    {

        $SQL = "Select p.id,CONCAT(p.nombre,' ',p.apellido) as nombredocente,p.nombre,p.apellido,d.nombre as departamento,d.id as iddepartamento,l.id as idlicencia,l.fechaInicio,l.fechaFinal, tl.idLicencia as tipolicenciaid,tl.nombre as tipolicencianombre, tl.descripcion FROM licencia l left join profesor p on l.idProfesor=p.id left JOIN departamento d on p.idDepartamento=d.id left join tipo_licencia tl ON l.id=tl.idLicencia WHERE CONCAT(p.nombre,' ',p.apellido) like '%$nombredocente%' and l.fechaInicio >=
        STR_TO_DATE($fechainiciostring, '%d-%m-%Y') 
         ";

    }
    elseif($nombredocente!= null and $fechainiciostring != null and $fechafinalstring != null)
    {

        $SQL = "Select p.id,CONCAT(p.nombre,' ',p.apellido) as nombredocente,p.nombre,p.apellido,d.nombre as departamento,d.id as iddepartamento,l.id as idlicencia,l.fechaInicio,l.fechaFinal, tl.idLicencia as tipolicenciaid,tl.nombre as tipolicencianombre, tl.descripcion FROM licencia l left join profesor p on l.idProfesor=p.id left JOIN departamento d on p.idDepartamento=d.id left join tipo_licencia tl ON l.id=tl.idLicencia WHERE CONCAT(p.nombre,' ',p.apellido) like '%$nombredocente%' and l.fechaInicio >=  STR_TO_DATE($fechainiciostring, '%d-%m-%Y')
         and l.fechaFinal  <=  STR_TO_DATE($fechafinalstring, '%d-%m-%Y') ";

    }
    elseif($nombredocente== null and $fechainiciostring != null and $fechafinalstring == null)
    {

        $SQL = "Select p.id,p.nombre,p.apellido,d.nombre as departamento,d.id as iddepartamento,l.id as idlicencia,l.fechaInicio,l.fechaFinal, tl.idLicencia as tipolicenciaid,tl.nombre as tipolicencianombre, tl.descripcion FROM licencia l left join profesor p on l.idProfesor=p.id left JOIN departamento d on p.idDepartamento=d.id left join tipo_licencia tl ON l.id=tl.idLicencia WHERE  l.fechaInicio >=  STR_TO_DATE($fechainiciostring, '%d-%m-%Y')
         ";

    }
    elseif($nombredocente== null and $fechainiciostring == null and $fechafinalstring != null)
    {

        $SQL = "Select p.id,p.nombre,p.apellido,d.nombre as departamento,d.id as iddepartamento,l.id as idlicencia,l.fechaInicio,l.fechaFinal, tl.idLicencia as tipolicenciaid,tl.nombre as tipolicencianombre, tl.descripcion FROM licencia l left join profesor p on l.idProfesor=p.id left JOIN departamento d on p.idDepartamento=d.id left join tipo_licencia tl ON l.id=tl.idLicencia WHERE  l.fechaFinal <=  STR_TO_DATE($fechafinalstring, '%d-%m-%Y')
        ";
    }
    elseif($nombredocente== null and $fechainiciostring != null and $fechafinalstring != null)
    {

        $SQL = "Select p.id,p.nombre,p.apellido,d.nombre as departamento,d.id as iddepartamento,l.id as idlicencia,l.fechaInicio,l.fechaFinal, tl.idLicencia as tipolicenciaid,tl.nombre as tipolicencianombre, tl.descripcion FROM licencia l left join profesor p on l.idProfesor=p.id left JOIN departamento d on p.idDepartamento=d.id left join tipo_licencia tl ON l.id=tl.idLicencia WHERE  l.fechaInicio >=  STR_TO_DATE($fechainiciostring, '%d-%m-%Y')
        and l.fechaFinal  <=  STR_TO_DATE($fechafinalstring, '%d-%m-%Y') ";
    }
    elseif($nombredocente!= null and $fechainiciostring == null and $fechafinalstring != null)
    {

        $SQL = "Select p.id,CONCAT(p.nombre,' ',p.apellido) as nombredocente,p.nombre,p.apellido,d.nombre as departamento,d.id as iddepartamento,l.id as idlicencia,l.fechaInicio,l.fechaFinal, tl.idLicencia as tipolicenciaid,tl.nombre as tipolicencianombre, tl.descripcion FROM licencia l left join profesor p on l.idProfesor=p.id left JOIN departamento d on p.idDepartamento=d.id left join tipo_licencia tl ON l.id=tl.idLicencia WHERE  CONCAT(p.nombre,' ',p.apellido) like '%$nombredocente%'
        and l.fechaFinal  <=  STR_TO_DATE($fechafinalstring, '%d-%m-%Y') ";
    }

   
 




$reportebuscar= BDConexion::getInstancia()->query($SQL);
                                            
          
if($reportebuscar !=false)
{  
            if ($reportebuscar->num_rows != 0)
                {
                    $filas='';
                    while($row = $reportebuscar->fetch_array())
                    {
                        $datetime = date_create(date("d-m-Y"));
                        $Color="";
                        $datetimecomparable= $row['fechaFinal'];
                        $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                        if ($datetime2 < $datetime)
                         {
                                  $Color="#9c9c9c";

                         }
                         else
                         {
                                  $Color="#ffffff";
                         }
                        $fecha = new DateTime($datetimecomparable= $row['fechaInicio']);
                        $fecha_d_m_y1 = $fecha->format('d-m-Y');
                        $fecha = new DateTime($datetimecomparable= $row['fechaFinal']);
                        $fecha_d_m_y2 = $fecha->format('d-m-Y'); 

                     $filas.='
                     <tr>
      
                     <td style="background-color: '.$Color.'" >'.$row['nombre'].'</td>
                     <td style="background-color:'.$Color.'" >'.$row['apellido'].'</td>
                     <td style="background-color:'.$Color.'" >'.$row['departamento'].'</td>
                     <td style="background-color:'.$Color.'"  width="700">'. $fecha_d_m_y1.'/'.$fecha_d_m_y2.'</td>
                     <td style="background-color:'.$Color.'" >'.$row['tipolicencianombre'].'</td>
                     <td style="background-color:'.$Color.'" >
                         <a title="Ver detalle" href="novedad.ver.php?id='.$row['idlicencia'].'&id1='.$row['tipolicenciaid'].'&id2='.$row['id'].'">
                             <button type="button" class="btn btn-outline-info">
                                 <span class="oi oi-zoom-in"></span>
                             </button></a>

                     </td>
                     <td style="background-color:'.$Color.'" >
                         <a title="Modificar" href="novedad.modificar.php?id='.$row['idlicencia'].'&id1='.$row['tipolicenciaid'].'&id2='.$row['id'].'">
                             <button type="button" class="btn btn-outline-warning">
                                 <span class="oi oi-pencil"></span>
                             </button></a>
                     </td>
                     <td style="background-color:'.$Color.'">
                         <a title="Eliminar" href="novedades.eliminar.php?id='.$row['idlicencia'].'&id1='.$row['tipolicenciaid'].'&id2='.$row['id'].'">
                           <button class="btn btn-outline-danger" class="btn btn-outline-info">
                              <span class="oi oi-trash"></span>
                           </button></a>
                     </td>
                     
                     </tr>   
                     ';

                    }


                    $cuerpodiv='                    
                    <table name="novedades" id="novedades" class ="table table-hover table-condensed table-bordered">
                            <thead>
                            <tr scope="row">
                              <td>Nombre</td>
                              <td>Apellido</td>
                              <td>Departamento</td>
                              <td>Dias</td>
                              <td>Regimen</td>
                              <td>Ver mas</td>
                              <td>Editar</td>
                              <td>Eliminar</td>
                            </tr>
                            </thead>
                      
                            '.$filas.'
                                     
                    </table>';

                }
                else 
                {
                    
                    $cuerpodiv='<div id="vacio" class="alert alert-warning" role="alert">No se encontraron resultados con los filtros ingresados. <strong>Por favor, intente nuevamente </strong></strong></div>';

    
                }
    
            }
            else{
                $cuerpodiv='<div id="vacio" class="alert alert-warning" role="alert">Error al consultar la base de datos. <strong>Por favor, intente nuevamente </strong></div>';
            }
    



}
else
{

    $cuerpodiv='<div id="param" class="alert alert-warning" role="alert">No se encontraron resultados con los filtros ingresados. <strong>Por favor, intente nuevamente </strong></div>';

}



                echo $cuerpodiv;

 ?>
