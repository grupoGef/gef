<?php

include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_INICIO);

//echo $_POST['NombreMesa'];
$tipo_mesa=  $_POST['NombreMesa'];
$tipo_llamado='';


switch($tipo_mesa)
{
    case('Febrero-Marzo'):
    $tipo_llamado="general";
    break;
    case('Mayo'):
    $tipo_llamado="extraordinaria";    
    case('Abril'):
    $tipo_llamado="todo tiempo";    
    break;   
    case('Junio'):
    $tipo_llamado="todo tiempo";      
    break; 
    case('Julio-Agosto'):
    $tipo_llamado="general";     
    break;
    case('Octubre'):
     $tipo_llamado="extraordinaria";   
    break; 
    case('Diciembre'):
    $tipo_llamado="general";
        
    break;       
    
    
}



?>

<html>
    <head>
        <!--  Scripts / Links necesarios Pertenecientes al Bootstrap y al Calendario-->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/calendar2.js"></script>

    <script>
 $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '< Ant',
 nextText: 'Sig >',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
 weekHeader: 'Sm',
 dateFormat: 'dd/mm/yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);

</script>

    




    <!-- <script type="text/javascript" src="../../lib/JQuery/inicio.js"></script>  

        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Gestion de Mesa de Examen</title>
    </head>
    <body>
<?php include_once '../gui/navbar.php'; ?>

<!-- Se pasa el id del llamado de la pagina Llamado por medio de un formulario y se guarda en la variable $Mesa-->
<?php @$Mesa=$_POST['NombreMesa']; 

    //Consulta a la Base de Datos para recuperar el ID, Tipo y nombre del LLAMADO
    // $Consulta="SELECT L.id AS identifica, L.tipo AS tipo, L.nombre AS nombre FROM LLAMADO L WHERE L.id LIKE '%".$Mesa."%'";
    // $Consultas=BDConexion::getInstancia()->query($Consulta);
    // $row = $Consultas->fetch_assoc();
    // $Llamado = new Llamado($row['identifica']);
    // $Llamado->setTipo($row['tipo']);
    // $Llamado->setId($row['identifica']);
    // $Llamado->setNombre($row['nombre']);
  
    //Si se aprieta el Boton "Calendario" se recarga la pagina y cuando llega al if si esta apretado entra, en el caso deque no lo salta y no se realiza ninguna operacion

?>
<div class="container-fluid "> 
    <div class="container-fluid">
        <div class="row justify-content-around">
          <div class=" col-sm-3">
                <div class="card">
                    <div class="card-header alert-info">
                        Seleccionar otro mes
                    </div>  
                    <div class="card-body">
                    <a href="inicio.php">
                        <button type="button" class="btn btn-primary">
                            <span class="oi oi-account-logout"></span> Volver
                        </button>
                    </a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header alert-success">
                        <span class="card-title">Ingreso de Fecha</span>
                    </div>
                    <div class="card-body">
                        <div id="resultado"></div>
                        <!-- Formulario que nos permite generar el Calendario -->
                        <form action="calendario.procesar.php" id="formcalendario" name="formcalendario" method="POST">
                            <div class="container">
                                <h3 align="center">Mes Elegido: <?=$tipo_mesa  ?></h3>
                                <h5 align="center">Tipo de Mesa <?=$tipo_llamado?></h5>
                                <input type=hidden id="meses" name="meses" value="<?=$tipo_mesa?>"></input>
                                <input type=hidden id="tipomesa" name="tipomesa" value="<?=$tipo_llamado?>"></input>

	                            <div class="card-body date" align="center">
                                            <div class="row">
                                                        <div class="col border" id="llamado1">
                                            <br>
                                                            <h5>Primer llamado</h5>
                                                            <br>
                                                            
                                                            <label for="fechainicio1">Fecha Inicio </label>
                                                            <input id="fechainicio1" class="text ui-widget-content ui-corner-all" type="text" name="fechainicio1" />
                                                            <br>
                                                            <label for="fechafin1">Fecha Final </label>
                                                            <input id="fechafin1" class="text ui-widget-content ui-corner-all" type="text" name="fechafin1" />
                                                        </div>
                                                        <div class="col border"  name="llamado2" id="llamado2">
                                                          <br>  
                                                        <h5>Segundo llamado</h5>
                                                            <br>
                                                            
                                                            <label for="fechainicio2">Fecha Inicio </label>
                                                            <input id="fechaincio2" class="text ui-widget-content ui-corner-all" type="text" name="fechaincio2" />
                                                            <br>
                                                            <label for="fechafin2">Fecha Final </label>
                                                            <input id="fechafin2" class="text ui-widget-content ui-corner-all" type="text" name="fechafin2" />
                                                            <br>
                                                        <br>
                                                        </div>
                                                        

                                            </div>
                                    </div>
                           
                               
                               
                                <div align="center">


                                <button  type="submit"  name="calendario" class="btn btn-outline-info">
                                        <span  class="oi oi-check"></span> Guardar
                                    </button>    
                                </div>
                            </div>                                       
                        </form>
                    </div>
                </div>   
            </div>
        </div>    
        <br>
    </div>
</div>
        <?php include_once '../gui/footer.php'; ?>
        <!-- SCRIPTS Necesarios para el funcionamiento del Calendario -->
        
        
        <!-- <script src="../lib/calendar/bootstrap-datepicker.js"></script>
        <script src="../lib/calendar/bootstrap-datepicker.es.min.js"></script>
        <script src="../lib/calendar/script.js"></script> -->

        
    </body>

</html>

