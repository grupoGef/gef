import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MesasComponent } from './mesas/mesas.component';
import { IonicModule } from '@ionic/angular';
import {MesaComponent} from './mesa/mesa.component';
import {HeaderComponent} from './header/header.component';
import {NotificacionesComponent} from './notificaciones/notificaciones.component';
import {NotificacionComponent} from './notificacion/notificacion.component';

@NgModule({
  declarations: [
    MesasComponent,
    MesaComponent,
    HeaderComponent,
    NotificacionesComponent,
    NotificacionComponent
  ],
  exports: [MesasComponent, MesaComponent, HeaderComponent, NotificacionesComponent, NotificacionComponent],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ComponentsModule { }
