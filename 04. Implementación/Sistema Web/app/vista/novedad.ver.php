<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARNOVEDADES);




$idlicencia=$_GET["id"];
$idtipolicencia=$_GET["id1"];
$iddocente=$_GET["id2"];

$query="Select p.id,p.nombre,p.apellido,d.nombre as departamento,d.id as iddepartamento,l.id as idlicencia,l.fechaInicio,l.fechaFinal, tl.idLicencia as tipolicenciaid,tl.nombre as tipolicencianombre, tl.descripcion FROM licencia l left join profesor p on l.idProfesor=p.id left JOIN departamento d on p.idDepartamento=d.id left join tipo_licencia tl ON l.id=tl.idLicencia WHERE 
p.id=$iddocente and l.id=$idlicencia and tl.idLicencia  = $idtipolicencia";


$reportebuscar= BDConexion::getInstancia()->query($query);



if ($reportebuscar->num_rows != 0)
{
   
    while($row = $reportebuscar->fetch_array())
    {
        $docente=$row['nombre'];
        $departamento=$row['departamento'];
        $nombrelicencia=$row['tipolicencianombre'];
        $fecha = new DateTime($datetimecomparable= $row['fechaInicio']);
        $fecha_d_m_y1 = $fecha->format('d-m-Y');
        $fecha = new DateTime($datetimecomparable= $row['fechaFinal']);
        $fecha_d_m_y2 = $fecha->format('d-m-Y'); 
        $licenciadesc=$row['descripcion'];


    }

}






?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Acerca de la Licencia</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <p></p>
            <div class="card">
                <div class="card-header">
                <p><center><h3>Información de la licencia</h3></center></p>
                  
                </div>
                <div class="card-body">
                    
                    <h4 class="card-text">Nombre de Docente</h4>
                    <p> <?= $docente ?></p>
                    <hr />
                     <h4 class="card-text">Departamento</h4>
                    <p> <?=$departamento ?></p>
                    <hr />
                    <h4 class="card-text">Nombre de Licencia</h4>
                    <p> <?= $nombrelicencia ?></p>
                    <hr />
                     <h4 class="card-text">Fecha de Inicio</h4>
                    <p> <?= $fecha_d_m_y1 ?></p>
                    <hr />
                    <h4 class="card-text">Fecha de Final</h4>
                    <p> <?= $fecha_d_m_y2 ?></p>
                    <hr />
                    <h4 class="card-text">Descripcion</h4>
                    <p> <?= $licenciadesc ?></p>
                    <hr />
                    <h5 class="card-text">Opciones</h5>
                    <a href="Novedades.php">
                        <button type="button" class="btn btn-primary">
                            <span class="oi oi-account-logout"></span> Salir
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
