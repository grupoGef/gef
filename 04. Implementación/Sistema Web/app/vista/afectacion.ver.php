<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARAFECTACION);

include_once  '../modelo/ColeccionMesaExamen.php';
include_once  '../modelo/ColeccionTribunal.php';
include_once  '../modelo/ColeccionAsignaturas.php';
include_once  '../modelo/ColeccionDocentes.php';

if(isset($_GET["id"]))
{
    $idtribunal=$_GET["id"];

}
if(isset($_GET["id1"]))
{
    $idtribunalasig=$_GET["id1"];
}

if(isset($_GET["id2"]))
{
    $idasignatura=$_GET["id2"];

}

?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Acerca del Tribunal</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>

        <?php
        
        $sql='SELECT a.nombre,a.codAsignatura,t.id as tribunalid,ta.id as tribunalasignaturaid,p.id as idpresidente,t.preferenciapresidente as presidentepreferencias , CONCAT(p.nombre, " ",p.apellido) as presidente,p1.id as idvocal,t.preferenciavocal as vocalpreferencias,CONCAT(p1.nombre, " ",p1.apellido)as vocal ,p2.id as idvocal1,t.preferenciavocal1 as vocal1preferencias, CONCAT(p2.nombre, " ",p2.apellido)as vocal1,p3.id as idsuplente,t.preferenciasuplente as suplentepreferencias,CONCAT(p3.nombre, " ",p3.apellido)as suplente from tribunal t left join tribunalasignatura ta ON t.id=ta.idTribunal left join asignatura a ON a.codAsignatura= ta.idAsignatura left join profesor p on t.presidente = p.id left join profesor p1 on t.vocal = p1.id left join profesor p2 on t.vocal1 = p2.id left join profesor p3 on t.suplente = p3.id
        where t.id='.$idtribunal.' and ta.id='.$idtribunalasig.' and a.codAsignatura='.$idasignatura.' 
        ';
     
        $consulta2 = BDConexion::getInstancia()->query($sql);                           
        
        
        while($row = $consulta2->fetch_array())
        {
          
          if ($row['idsuplente']== NULL && $row['suplente']== NULL )
          {
            $datos='
            <div class="container">
            <p></p>
            <div class="card">
                <div class="card-header">
                <p><center><h3>Información del Tribunal</h3></center></p>
                </div>
                <div class="card-body">
                      <h4 class="card-text">Asignatura</h4>
                    <p>'.$row['nombre'].'  </p>
                    <hr />
                      <h4 class="card-text">Presidente</h4>
                    <p>'.$row['presidente'].'</p>
                    <hr />';
            if($row['presidentepreferencias'] != null)
            {
            $datos.='
            <h4 class="card-text">Preferencias del Presidente</h4>
            <p>'.$row['presidentepreferencias'].'</p>
            <hr />
            ';
            }        
            $datos.='<h4 class="card-text">Vocal</h4>
            <p>'.$row['vocal'].'</p>
            <hr />';
            if($row['vocalpreferencias'] != null)
            {
                $datos.='
                <h4 class="card-text">Preferencias del Vocal</h4>
                <p>'.$row['vocalpreferencias'].'</p>
                <hr />
                ';
            } 
            $datos.='<h4 class="card-text">Segundo Vocal</h4>
            <p>'.$row['vocal1'].'</p>
            <hr />';
            if($row['vocal1preferencias'] != null)
            {
                $datos.='
                <h4 class="card-text">Preferencias del Segundo Vocal</h4>
                <p>'.$row['vocal1preferencias'].'</p>
                <hr />
                ';
            } 
            }
        else
        {
            $datos='
            <div class="container">
            <p></p>
            <div class="card">
                <div class="card-header">
                p><center><h3>Información del Tribunal</h3></center></p>
                </div>
               
            <div class="card-body">
                  <h4 class="card-text">Asignatura</h4>
                <p>'.$row['nombre'].'  </p>
                <hr />
                  <h4 class="card-text">Presidente</h4>
                <p>'.$row['presidente'].'</p>
                <hr />';
        if($row['presidentepreferencias'] != null)
        {
        $datos.='
        <h4 class="card-text">Preferencias del Presidente</h4>
        <p>'.$row['presidentepreferencias'].'</p>
        <hr />
        ';
        }        
        $datos.='<h4 class="card-text">Vocal</h4>
        <p>'.$row['vocal'].'</p>
        <hr />';
        if($row['vocalpreferencias'] != null)
        {
            $datos.='
            <h4 class="card-text">Preferencias del Vocal</h4>
            <p>'.$row['vocalpreferencias'].'</p>
            <hr />
            ';
        } 
        $datos.='<h4 class="card-text">Segundo Vocal</h4>
        <p>'.$row['vocal1'].'</p>
        <hr />';
        if($row['vocal1preferencias'] != null)
        {
            $datos.='
            <h4 class="card-text">Preferencias del Segundo Vocal</h4>
            <p>'.$row['vocal1preferencias'].'</p>
            <hr />
            ';
        } 
        $datos.='<h4 class="card-text">Suplente</h4>
        <p>'.$row['suplente'].'</p>
        <hr />';
        if($row['suplentepreferencias'] != null)
        {
            $datos.='
            <h4 class="card-text">Preferencias del Suplente</h4>
            <p>'.$row['suplentepreferencias'].'</p>
            <hr />
            ';
        } 
 

        }
        $datos.='
        <h5 class="card-text">Opciones</h5>
                    <a href="gestionarAfectacion.php">
                        <button type="button" class="btn btn-primary">
                            <span class="oi oi-account-logout"></span> Salir
                        </button>
                    </a>
                </div>
            </div>
        </div>
        ';

        echo $datos;

            



          }

        
        ?>
        
    
    </body>
</html>