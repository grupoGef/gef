<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARDOCENTE);
include_once '../modelo/ColeccionDocentes.php';
include_once '../modelo/ColeccionCargo.php';
$id=$_GET["id"];
$id1=$_GET["id1"];
$Docente = new Docente($id);
$Cargo = new Cargo($id1);

?>
<html>
    <head>
      <meta charset="UTF-8">
      <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
      <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
      <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
      <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="../../lib/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Actualizar Docente</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="docente.modificar.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                    <p><center><h3>Modificar Docente</h3></center></p>
                        <p>
                            Por favor, complete los campos que desea modificar.
                            Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                            Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="inputNombree">Nombre</label>
                            <input type="text" name="nombre" class="form-control" id="inputNombre" value="<?= $Docente->getNombre(); ?>" placeholder="Ingrese el nombre del usuario"  oninput="validar('inputNombre')" required="" pattern="^[a-zA-Z]{4,}\s?([a-zA-Z]{3,})?">
                        </div>
                        <div class="form-group">
                            <label for="inputApellidoo">Apellido</label>
                            <input type="text" name="apellido" class="form-control" id="inputApellido" value="<?= $Docente->getApellido(); ?>" placeholder="Ingrese el nombre del usuario"  oninput="validar('inputApellido')" required="" pattern="^[a-zA-Z]{4,}\s?([a-zA-Z]{3,})?">
                        </div>
                        <div class="form-group">
                            <label for="inputDni">Dni</label>
                            <input type="text" name="dni" class="form-control" id="inputDni" value="<?= $Docente->getDni(); ?>" placeholder="Ingrese el nombre del usuario" oninput="validar('inputDni')" required="" pattern="[0-9]{8}">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Email</label>
                            <input type="email" name="email" class="form-control" id="inputEmail" value="<?= $Docente->getEmail() ?>" placeholder="Ingrese el email del usuario" oninput="validar('inputEmail')"required="">
                        </div>
                        <div class "form-group">
                            <label for="categoria">Categoria</label>
                          <select required='' class="form-control " name="categoria" >
                              <?php
                                $opcion0= $Docente->getCategoria();
                                if (strcmp($opcion0,"Titular") ==0)
                                {
                                    echo '<option selected="selected" value="Titular">Titular</option>
                                    <option value="Asociado">Asociado</option>
                                    <option value="Adjunto">Adjunto</option>
                                    <option value="Adjunto">Asistente</option>
                                    <option value="Ayudante">Ayudante</option>';
                                }
                              elseif (strcmp($opcion0,"Asociado")==0)
                              {
                                echo '<option selected="selected" value="Titular">Titular</option>
                                <option selected="selected" value="Asociado">Asociado</option>
                                <option value="Adjunto">Adjunto</option>
                                <option value="Adjunto">Asistente</option>
                                <option value="Ayudante">Ayudante</option>';
                              }  
                              elseif (strcmp($opcion0,"Adjunto")==0)
                              {
                                echo '<option selected="selected" value="Titular">Titular</option>
                                <option value="Asociado">Asociado</option>
                                <option  selected="selected" value="Adjunto">Adjunto</option>
                                <option value="Adjunto">Asistente</option>
                                <option value="Ayudante">Ayudante</option>';
                              }
                              elseif (strcmp($opcion0,"Asistente")==0)
                              {
                                echo '<option selected="selected" value="Titular">Titular</option>
                                <option value="Asociado">Asociado</option>
                                <option  value="Adjunto">Adjunto</option>
                                <option selected="selected" value="Adjunto">Asistente</option>
                                <option value="Ayudante">Ayudante</option>';
                              }   
                              elseif (strcmp($opcion0,"Ayudante")==0)
                              {
                                echo '<option selected="selected" value="Titular">Titular</option>
                                <option value="Asociado">Asociado</option>
                                <option  value="Adjunto">Adjunto</option>
                                <option  value="Adjunto">Asistente</option>
                                <option selected="selected" value="Ayudante">Ayudante</option>';
                              }         
                              ?>
                          </select>
                        </div>
                        <div class "form-group">
                          <label for"Dedicacion">Dedicacion</label>
                    
                          <select  required=''  class="form-control" name="dedicacion" >
                            <?php
                             $opcion =$Docente->getDedicacion();
                             if (strcmp($opcion,"Exclusiva") ==0)
                                {
                                    echo '<option selected="selected" value="Exclusiva">Exclusiva</option>
                                    <option value="Parcial">Parcial</option>
                                    <option value="Simple">Simple</option>';
                                }
                              elseif (strcmp($opcion,"Parcial")==0)
                              {
                                  echo '<option  value="Exclusiva">Exclusiva</option>
                                  <option selected="selected" value="Parcial">Parcial</option>
                                  <option value="Simple">Simple</option>';
                              }  
                              elseif (strcmp($opcion,"Simple")==0)
                              {
                                  echo '<option  value="Exclusiva">Exclusiva</option>
                                  <option  value="Parcial">Parcial</option>
                                  <option selected="selected" value="Simple">Simple</option>';
                              }       
                            ?>
                          </select>
                        </div>
                        <div class="form-group">
                            <label for="inputDepartamento">Departamento</label>
                            <select  class="form-control form-control" name="departamento">
                            <?php   
                              $opcion1 =strval( $Docente->getIdDepartamento());

                              if (strcmp($opcion1,"1") ==0)
                              {
                                echo '<option selected="selected"  value="1">Exactas/Naturales</option>
                                <option value="2">Sociales</option>';
                              }
                              elseif (strcmp($opcion1,"2")==0)
                              {
                                  echo '<option  value="1">Exactas/Naturales</option>
                                  <option selected="selected" value="2">Sociales</option>';

                              }
                            ?>
                            </select>
                        </div>
                    <input type="hidden" name="id" class="form-control" id="id" value="<?= $Docente->getId(); ?>" >
                    <input type="hidden" name="id1" class="form-control" id="id1" value="<?= $Cargo->getId(); ?>" >
                      </div>
                      <div class="class-footer">
                          <button type="submit" class="btn btn-outline-success">
                              <span class="oi oi-check"></span>
                              Confirmar
                          </button>
                          <a href="docente.php">
                              <button type="button" class="btn btn-outline-danger">
                                  <span class="oi oi-x"></span>
                                  Cancelar
                              </button>
                          </a>
                      </div>
                </div>
            </form>
        </div>
          <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
