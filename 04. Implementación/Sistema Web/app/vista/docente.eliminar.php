<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARDOCENTE);
include_once '../modelo/Docente.Class.php';
include_once '../modelo/Cargo.Class.php';
$id= $_GET["id"];
$id1= $_GET["id1"];
$Docente = new Docente($id);
$Cargo = new Cargo($id1);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
        <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
        <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Eliminar Docente</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="docente.eliminar.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                    <p><center><h3>Eliminar del Docente</h3></center></p>
                    </div>
                    <div class="card-body">
                        <p class="alert alert-warning ">
                            <span class="oi oi-warning"></span> ATENCI&Oacute;N. Esta operaci&oacute;n no puede deshacerse.
                        </p>
                        <p>¿Est&aacute; seguro que desea eliminar el Docente <b><?= $Docente->getNombre(); ?></b>?

                    </div>
                    <input type="hidden" name="id" class="form-control" id="id" value="<?= $Docente->getId(); ?>">
                    <input type="hidden" name="id1" class="form-control" id="id1" value="<?= $Cargo->getId(); ?>">
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <span class="oi oi-check"></span> Sí, deseo eliminar
                        </button>
                        <a href="docente.php">
                            <button type="button" class="btn btn-outline-danger">
                                <span class="oi oi-x"></span> NO (Salir de esta pantalla)
                            </button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
