<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARMESAEXAMEN);
// include_once '../modelo/ColeccionCarrera.php';
// $ColeccionCarrera = new ColeccionCarrera();







$nombredocente=null;
$idcarrera=null;
$idasignatura=null;
$cuerpodiv= '';




if(isset($_POST['docente'])) 
{
    $nombredocente=$_POST['docente'];

}
if(isset($_POST['carrera'])) 
{
    $idcarrera=$_POST['carrera'];

}
if(isset($_POST['asignatura'])) 
{
    $idasignatura=$_POST['asignatura'];

}
if($nombredocente != null OR $idcarrera !=null OR  $idasignatura != null )
{
    if($nombredocente!= null and $idcarrera == null and $idasignatura == null)
    {

        $SQL = 'Select a.nombre,c.nombre,c.codCarrera,a.nombre as nombreasignatura,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal,l2.id as vocal1licencia,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on a.idCarrera=c.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
        licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
        licencia l3 on p3.id=l3.idProfesor
        where tm.id is not null  
        and (p.id ='.$nombredocente.' or p1.id = '.$nombredocente.' or p2.id='.$nombredocente.' or p3.id='.$nombredocente.' )   ';

    }
    elseif($nombredocente!= null and $idcarrera != null and $idasignatura == null)
    {

        $SQL = 'Select a.nombre,c.nombre,c.codCarrera,a.nombre as nombreasignatura,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal,l2.id as vocal1licencia,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on a.idCarrera=c.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
        licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
        licencia l3 on p3.id=l3.idProfesor
        where tm.id is not null  
        and (p.id ='.$nombredocente.' or p1.id = '.$nombredocente.' or p2.id='.$nombredocente.' or p3.id='.$nombredocente.' ) and c.codCarrera = '.$idcarrera.'  ';


    }
    elseif($nombredocente!= null and $idcarrera != null and $idasignatura != null)
    {

        $SQL = 'Select a.nombre,c.nombre,c.codCarrera,a.nombre as nombreasignatura,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal,l2.id as vocal1licencia,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on a.idCarrera=c.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
        licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
        licencia l3 on p3.id=l3.idProfesor
        where tm.id is not null  
        and (p.id ='.$nombredocente.' or p1.id = '.$nombredocente.' or p2.id='.$nombredocente.' or p3.id='.$nombredocente.' ) and c.codCarrera = '.$idcarrera.'   and a.codAsignatura = '.$idasignatura.' ';

    }
    elseif($nombredocente== null and $idcarrera != null and $idasignatura == null)
    {

        $SQL = 'Select a.nombre,c.nombre,c.codCarrera,a.nombre as nombreasignatura,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal,l2.id as vocal1licencia,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on a.idCarrera=c.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
        licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
        licencia l3 on p3.id=l3.idProfesor
        where tm.id is not null  
        and c.codCarrera = '.$idcarrera.'  ';


    }
    elseif($nombredocente== null and $idcarrera == null and $idasignatura != null)
    {

        $SQL = 'Select a.nombre,c.nombre,c.codCarrera,a.nombre as nombreasignatura,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal,l2.id as vocal1licencia,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on a.idCarrera=c.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
        licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
        licencia l3 on p3.id=l3.idProfesor
        where tm.id is not null  
        and a.codAsignatura = '.$idasignatura.'';
    }
    elseif($nombredocente== null and $idcarrera != null and $idasignatura != null)
    {

        $SQL = 'Select a.nombre,c.nombre,c.codCarrera,a.nombre as nombreasignatura,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal,l2.id as vocal1licencia,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on a.idCarrera=c.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
        licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
        licencia l3 on p3.id=l3.idProfesor
        where tm.id is not null  
        and a.codAsignatura = '.$idasignatura.' and c.codCarrera = '.$idcarrera.' ';
    }
    elseif($nombredocente!= null and $idcarrera == null and $idasignatura != null)
    {

        $SQL = 'Select a.nombre,c.nombre,c.codCarrera,a.nombre as nombreasignatura,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal,l2.id as vocal1licencia,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on a.idCarrera=c.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
        licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
        licencia l3 on p3.id=l3.idProfesor
        where tm.id is not null  
        and (p.id ='.$nombredocente.' or p1.id = '.$nombredocente.' or p2.id='.$nombredocente.' or p3.id='.$nombredocente.' )   and a.codAsignatura = '.$idasignatura.'';
    }
  

$reportebuscar= BDConexion::getInstancia()->query($SQL);
                                            
if ($reportebuscar != false)
{
      
  if ($reportebuscar->num_rows != 0)
      {
          $filas='';
          while($row = $reportebuscar->fetch_array())
          {
            $filas.='
            <tr>
          
            <td>'.$row['nombre'].'</td>
            <td>'.$row['nombreasignatura'].'</td>
            ';
            $Colorpresidente='';
            $Colorvocal='';
            $Colorvocal1='';
            $Colorsuplente='';
       
            if ($row['presidentelicencia']!=null )
            {   

            $fechafinal=$row['fecha'];
            $datetime1=date_create_from_format('Y-m-d', $fechafinal);
            $datetimecomparable= $row['presidentefinal'];
            $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
            
            if ($datetime1 < $datetime2)
            {
                $Colorpresidente="#FF0000";

            }
             

            }
            if ($row['vocallicencia']!=null )
            {   

            $fechafinal=$row['fecha'];
            $datetime1=date_create_from_format('Y-m-d', $fechafinal);
            $datetimecomparable= $row['vocalfinal'];
            $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
            
            if ($datetime1 < $datetime2)
            {
                $Colorvocal="#FF0000";

            }
             

            }
            if ($row['vocal1licencia']!=null )
            {   

            $fechafinal=$row['fecha'];
            $datetime1=date_create_from_format('Y-m-d', $fechafinal);
            $datetimecomparable= $row['vocal1final'];
            $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
            
            if ($datetime1 < $datetime2)
            {
                $Colorvocal1="#FF0000";

            }
             

            }
            if ($row['suplentelicencia']!=null )
            {   

            $fechafinal=$row['fecha'];
            $datetime1=date_create_from_format('Y-m-d', $fechafinal);
            $datetimecomparable= $row['suplentefinal'];
            $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
            
            if ($datetime1 < $datetime2)
            {
                $Colorsuplente="#FF0000";

            }
             

            }
            $filas.='
            <td style="background-color: '.$Colorpresidente.'">'.$row['presidente'].'</td>
            <td style="background-color: '.$Colorvocal.'">'.$row['vocal'].'</td>
            <td style="background-color: '.$Colorvocal1.'">'.$row['vocal1'].'</td>
            ';

            if($row['suplente'] == null)
            {

                $filas.='
                <td> </td>
                <td>'.$row['fecha'].'</td>
                <td>'.$row['Hora'].'</td>
                <td>
                    <a title="Ver detalle" href="examen.ver.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                        <button type="button" class="btn btn-outline-info">
                            <span class="oi oi-zoom-in"></span>
                        </button></a>

                </td>
                <td>
                    <a title="Modificar" href="examen.modificar.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                        <button type="button" class="btn btn-outline-warning">
                            <span class="oi oi-pencil"></span>
                        </button></a>
                </td>
                <td>
                    <a title="Eliminar" href="examen.eliminar.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                      <button class="btn btn-outline-danger" class="btn btn-outline-info">
                         <span class="oi oi-trash"></span>
                      </button></a>
                </td>
            </tr>
                
                ';
            }
            else
            {
                $filas.='
                <td style="background-color: '.$Colorsuplente.'">'.$row['suplente'].' </td>
                <td>'.$row['fecha'].'</td>
                <td>'.$row['Hora'].'</td>
                <td>
                    <a title="Ver detalle" href="examen.ver.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                        <button type="button" class="btn btn-outline-info">
                            <span class="oi oi-zoom-in"></span>
                        </button></a>

                </td>
                <td>
                    <a title="Modificar" href="examen.modificar.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                        <button type="button" class="btn btn-outline-warning">
                            <span class="oi oi-pencil"></span>
                        </button></a>
                </td>
                <td>
                    <a title="Eliminar" href="examen.eliminar.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                      <button class="btn btn-outline-danger" class="btn btn-outline-info">
                         <span class="oi oi-trash"></span>
                      </button></a>
                </td>
            </tr>
                
                ';
            }
            $cuerpodiv='    <table id="gestionmesaexamen" class ="table table-hover table-condensed table-bordered">
            <thead>
            <tr scope="row">
           
                <td>Carrera</td>
                <td>Asignatura</td>
                <td>Presidente</td>
                <td>Vocal1</td>
                <td>Vocal2</td>
                <td>Suplente</td>
                <td>Fecha</td>
                <td>Hora</td>
                <td>Ver Mas</td>
                <td>Modificar</td>
                <td>Eliminar</td>
            </tr>
            </thread>
            <tbody>    
            '.$filas.'
            </table>';





          }
     }
     else 
{
    
    $cuerpodiv='<div id="vacio" class="alert alert-warning" role="alert">No se encontraron resultados con los filtros ingresados. <strong>Por favor, intente nuevamente </strong></div>';


}   
} 
else 
{
    
    $cuerpodiv='<div id="vacio" class="alert alert-warning" role="alert">No se encontraron casos con los filtros ingresados. <strong>Por favor, intente nuevamente </strong></div>';


}   


}
else
{

    $cuerpodiv='<div id="param" class="alert alert-warning" role="alert">No se encontraron casos con los filtros ingresados. <strong>Por favor, intente nuevamente </strong></div>';

}

echo $cuerpodiv;



?>
