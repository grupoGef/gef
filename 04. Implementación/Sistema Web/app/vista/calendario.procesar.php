<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_INICIO);


$DatosFormulario = $_POST;
$meses=$DatosFormulario["meses"];
$tipomesa=$DatosFormulario["tipomesa"];
$fechainicio1=$DatosFormulario["fechainicio1"];
$fechafin1=$DatosFormulario["fechafin1"];
if (!empty($DatosFormulario["fechaincio2"])&& !empty($DatosFormulario["fechafin2"]))
{
$fechainicio2 = $DatosFormulario["fechaincio2"];
$fechafin2 = $DatosFormulario["fechafin2"];
}
date_default_timezone_set('America/Argentina/Buenos_Aires');

$respuesta='realizado';
function compararFechas($primera, $segunda,$dia)
{
    $valoresPrimera = explode ("/", $primera);   
    $valoresSegunda = explode ("/", $segunda); 
  
    $diaPrimera    = $valoresPrimera[2];  
    $mesPrimera  = $valoresPrimera[1];  
    $anyoPrimera   = $valoresPrimera[0]; 
  
    $diaSegunda   = $valoresSegunda[2];  
    $mesSegunda = $valoresSegunda[1];  
    $anyoSegunda  = $valoresSegunda[0];
  
    $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);  
    $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);     
    
    $diferenciadedias=0;
    if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
      // "La fecha ".$primera." no es v&aacute;lida";
       $diferenciadedias=0;
    }elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
      // "La fecha ".$segunda." no es v&aacute;lida";
       $diferenciadedias=0;
    }else{
       
        $diferenciadedias= ($diasSegundaJuliano - $diasPrimeraJuliano);
       
    } 
   

    if($dia =='Lunes')
    {

     return $diferenciadedias+1;
    }
    else
    {
        return $diferenciadedias-1;
    }



}


if(!empty($fechainicio1) && !empty($fechafin1) && !empty($fechainicio2) && !empty($fechafin2) )
{

        

        $fechao1 = new DateTime($DatosFormulario["fechainicio1"]);
        $fecha_d_m_y1 = $fechao1->format('Y/m/d');
        $fechao2 = new DateTime($DatosFormulario["fechafin1"]);
        $fecha_d_m_y2 = $fechao2->format('Y/m/d');
        $fechao3 = new DateTime($DatosFormulario["fechaincio2"]);
        $fecha_d_m_y3 = $fechao3->format('Y/m/d');
        $fechao4 = new DateTime($DatosFormulario["fechafin2"]);
        $fecha_d_m_y4 = $fechao4->format('Y/m/d');
        $dias = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
        $sql="UPDATE  turno 
        SET fechainicio1 = '$fecha_d_m_y1',
         fechafin1= '$fecha_d_m_y2',
         periodollamado= 1 
        where idTurno =1  ";
   
        $consulta = BDConexion::getInstancia()->query($sql);

        $sql2="UPDATE  turno 
        SET fechainicio1 = '$fecha_d_m_y3',
         fechafin1= '$fecha_d_m_y4',
         periodollamado= 2 
        where idTurno =2 ";
        $consulta2 = BDConexion::getInstancia()->query($sql2);
 
        $dia = $dias[(date('N', strtotime($fecha_d_m_y1))) - 1];
        $dia2 = $dias[(date('N', strtotime($fecha_d_m_y3))) - 1];
        $valor1=compararFechas($fecha_d_m_y1,$fecha_d_m_y2,$dia);
        $valor2 = compararFechas($fecha_d_m_y3,$fecha_d_m_y4,$dia2);
       
        if ($valor1 ==5  && $valor2 ==5)
        {
            
            $fechao2->modify('+1 day');
            $period1 = new DatePeriod( new datetime($fecha_d_m_y1)
            , new DateInterval('P1D'),
            $fechao2
            );
            $dias = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
            $fechasprimerllamado=array();
            foreach ($period1 as $key => $value) { 
                $value->format('Y-m-d');
                $fecha_d_m_y = $value->format('Y-m-d');
                $dia = $dias[(date('N', strtotime($fecha_d_m_y))) - 1];
               
                if (strcmp($dia,'Sabado')!= 0 && strcmp($dia,'Domingo')!= 0)
                {
                array_push($fechasprimerllamado,$fecha_d_m_y);
                }
             }
            $fecha1=$fechasprimerllamado[0];
            $dia1=$dias[(date('N', strtotime($fecha1))) - 1];
            $fecha2=$fechasprimerllamado[1];
            $dia2=$dias[(date('N', strtotime($fecha2))) - 1];
            $fecha3=$fechasprimerllamado[2];
            $dia3=$dias[(date('N', strtotime($fecha3))) - 1];
            $fecha4=$fechasprimerllamado[3];
            $dia4=$dias[(date('N', strtotime($fecha4))) - 1];
            $fecha5=$fechasprimerllamado[4];
            $dia5=$dias[(date('N', strtotime($fecha5))) - 1];
        
            
            $llamado =1;
            $tipom='General';
            $stmt = BDConexion::getInstancia()->prepare("CALL `CargaPrimerllamado` (?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->bind_param("ssssssssssis", $dia1,$fecha1,$dia2,$fecha2,$dia3,$fecha3,$dia4,$fecha4,$dia5,$fecha5,$llamado,$tipom);
            $stmt->execute();

            if(!$stmt)
                {
                    echo "asd";
                     echo mysqli_errno(BDConexion::getInstancia()) . ": " . mysqli_error(BDConexion::getInstancia()) . "\n";
                }
           
 
            $fechao4->modify('+1 day');
            $period2 = new DatePeriod( new datetime($fecha_d_m_y3)
            , new DateInterval('P1D'),
            $fechao4
            );
            $dias = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
            $fechassegundollamado=array();
            foreach ($period2 as $key => $value) { 
                $value->format('Y-m-d');
                $fecha_d_m_ysegundo = $value->format('Y-m-d');
                $dia = $dias[(date('N', strtotime($fecha_d_m_ysegundo))) - 1];
                if (strcmp($dia,'Sabado')!= 0 && strcmp($dia,'Domingo')!= 0)
                {
                array_push($fechassegundollamado,$fecha_d_m_ysegundo);
                }
            }
            
            $fecha1=$fechassegundollamado[0];
            $dia1=$dias[(date('N', strtotime($fecha1))) - 1];
            $fecha2=$fechassegundollamado[1];
            $dia2=$dias[(date('N', strtotime($fecha2))) - 1];
            $fecha3=$fechassegundollamado[2];
            $dia3=$dias[(date('N', strtotime($fecha3))) - 1];
            $fecha4=$fechassegundollamado[3];
            $dia4=$dias[(date('N', strtotime($fecha4))) - 1];
            $fecha5=$fechassegundollamado[4];
            $dia5=$dias[(date('N', strtotime($fecha5))) - 1];

            $llamado =2;
            $tipom='General';

            $stmt2 = BDConexion::getInstancia()->query("CALL `CargaSegundollamado`('$dia1', '$fecha1', '$dia2', '$fecha2', '$dia3', '$fecha3', '$dia4','$fecha4', '$dia5', '$fecha5', '$llamado','$tipom')");
            // $stmt2 = BDConexion::getInstancia()->prepare("CALL `CargaSegundollamado` (?,?,?,?,?,?,?,?,?,?,?,?)");
            if(!$stmt2)
            {
                echo mysqli_errno(BDConexion::getInstancia()) . ": " . BDConexion::getInstancia()->error  . "\n";
            }    
            // $stmt2->bind_param("ssssssssssis", $dia1,$fecha1,$dia2,$fecha2,$dia3,$fecha3,$dia4,$fecha4,$dia5,$fecha5,$llamado,$tipom);
            // $stmt2->execute();
            if(!$stmt2)
            {
                echo mysqli_errno(BDConexion::getInstancia()) . ": " . BDConexion::getInstancia()->error . "\n";
            }    

            


            


        }
        else
        {
            $respuesta='Fecha Incorrecta. Recuerde que cada llamado debe contar con 5 días hábiles.';
           
        }








}
else
{
    if(!empty($fechainicio1) && !empty($fechafin1))
    {
       
        $fecha1 = new DateTime($DatosFormulario["fechainicio1"]);
        $fecha_d_m_y1 = $fecha1->format('Y/m/d');
        $fecha2 = new DateTime($DatosFormulario["fechafin1"]);
        $fecha_d_m_y2 = $fecha2->format('Y/m/d');
        $sql="UPDATE  turno 
        SET fechainicio1 = '$fecha_d_m_y1',
        SET fechafin1= '$fecha_d_m_y2',
        SET periodollamado= 1 
        where id =1  ";
        $consulta = BDConexion::getInstancia()->query($sql);
        $dias = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
        $dia = $dias[(date('N', strtotime($fecha_d_m_y1))) - 1];
        $dia2 = $dias[(date('N', strtotime($fecha_d_m_y2))) - 1];
        $valor1=compararFechas($fecha_d_m_y1,$fecha_d_m_y2,$dia);
        if ($valor1 =5  )
        {
            $fecha2->modify('+1 day');
            $period1 = new DatePeriod( new datetime($fecha_d_m_y1)
            , new DateInterval('P1D'),
            $fecha2
            );
            $dias = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
            $fechasprimerllamado=array();
            foreach ($period1 as $key => $value) { 
                $value->format('Y-m-d');
                $fecha_d_m_y = $value->format('Y-m-d');
                $dia = $dias[(date('N', strtotime($fecha_d_m_y))) - 1];
               
                if (strcmp($dia,'Sabado')!= 0 && strcmp($dia,'Domingo')!= 0)
                {
                array_push($fechasprimerllamado,$fecha_d_m_y);
                }
             }
            $fecha1=$fechasprimerllamado[0];
            $dia1=$dias[(date('N', strtotime($fecha1))) - 1];
            $fecha2=$fechasprimerllamado[1];
            $dia2=$dias[(date('N', strtotime($fecha2))) - 1];
            $fecha3=$fechasprimerllamado[2];
            $dia3=$dias[(date('N', strtotime($fecha3))) - 1];
            $fecha4=$fechasprimerllamado[3];
            $dia4=$dias[(date('N', strtotime($fecha4))) - 1];
            $fecha5=$fechasprimerllamado[4];
            $dia5=$dias[(date('N', strtotime($fecha5))) - 1];
            $stmt = BDConexion::getInstancia()->prepare("CALL `CargaPrimerllamado` (?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->bind_param("ssssssssssis", $dia1,$fecha1,$dia2,$fecha2,$dia3,$fecha3,$dia4,$fecha4,$dia5,$fecha5,$llamado,$tipom);
            $stmt->execute();

            if(!$stmt)
                {
                     echo mysql_errno(BDConexion::getInstancia()) . ": " . mysql_error(BDConexion::getInstancia()) . "\n";
                }

        }   
        else{
            $respuesta = 'Se ingresaron incorrectamente los parametros';
            
          
        } 

    }
    else
    {
        $respuesta = 'Por favor, complete todos los campos.';

    }


}


// if($tipomesa=='general')
// {
//  $sql="Insert into turno (fechainicio1,fechafin1,periodollamado) values ('$fecha_d_m_y1','$fecha_d_m_y2',1) ";
//  $consulta = BDConexion::getInstancia()->query($sql);

//  $sql2="Insert into turno (fechainicio1,fechafin1,periodollamado) values ('$fecha_d_m_y3','$fecha_d_m_y4',2) ";
//  $consulta2 = BDConexion::getInstancia()->query($sql2);

// }
// else
// {
//     $sql="Insert into turno (fechainicio1,fechafin1,periodollamado) values ('$fecha_d_m_y1','$fecha_d_m_y2',1) ";
//     $consulta = BDConexion::getInstancia()->query($sql);

// }


echo $respuesta;
















?>