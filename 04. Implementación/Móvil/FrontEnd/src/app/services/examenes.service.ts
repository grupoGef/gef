import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import {RespuestaTopHeadlines} from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ExamenesService {

  // URL DEL LOCALHOST
  private baseURL = environment.apiURL;
  constructor( private http: HttpClient ) { }

  private ejecutarQuery<T>(query: string, body: string) {
    query = this.baseURL + query;
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    });
    if(body == null){
      return this.http.get<T>(query);
    }
    return this.http.post<T>(query,body,{headers});
  }

  //Obtener listado de carreras
  cargarCarrreras(): Observable<RespuestaTopHeadlines> {
    return this.ejecutarQuery<RespuestaTopHeadlines>('/gef/api/listar_carreras_mesa.php',null);
  }

  //Obtener listado de asignaturas por carrera
  cargarAsignaturas(carrera: number): Observable<RespuestaTopHeadlines> {
    const body: string = "&idCarrera=" + carrera;
    return this.ejecutarQuery<RespuestaTopHeadlines>('/gef/api/listar_asignaturas_mesa.php',body);
  }

  //Obtener mesa de examen
  consultarMesas(body: string): Observable<RespuestaTopHeadlines> {
    return this.ejecutarQuery<RespuestaTopHeadlines>('/gef/api/listar_mesa.php',body);
  }

  //Obtener mesa de examen
  consultarMesasCarrera(body: string): Observable<RespuestaTopHeadlines> {
    return this.ejecutarQuery<RespuestaTopHeadlines>('/gef/api/listar_mesa_carrera.php',body);
  }

  consultarNotificacion(): Observable<RespuestaTopHeadlines> {
    return this.ejecutarQuery<RespuestaTopHeadlines>('/gef/api/listar_notificaciones_por_materia.php', null);
  }

  actualizarFavorito(body: string): Observable<RespuestaTopHeadlines> {
    return this.ejecutarQuery<RespuestaTopHeadlines>('/gef/api/actualizar_Favorito.php',body);
  }

}
