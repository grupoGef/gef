<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARMESAEXAMEN);

$DatosFormulario = $_POST;
$inputFecha = $DatosFormulario["fecha"];
$inputHora = $DatosFormulario["hora"];
$aula = $DatosFormulario["aula"];
$idtribunalmesa = $DatosFormulario['idtribunalmesa'];
$idMesa = $DatosFormulario['idMesa'];
$idasignatura = $DatosFormulario['idasignatura'];
$horadefault = $DatosFormulario['horadefault'];
$fecha = new DateTime($DatosFormulario["fechamesa"]);
$fecha_d_m_y1 = $fecha->format('Y/m/d');

$fecha2 = new DateTime($DatosFormulario["fecha"]);
$fecha_d_m_y2 = $fecha2->format('Y/m/d');







$sql="UPDATE turno_mesaexamen 
set fecha='{$fecha_d_m_y1}',
hora='{$inputHora}',
codAula={$aula}
where id=$idtribunalmesa  and idMesa=$idMesa
";

$consulta1 = BDConexion::getInstancia()->query($sql);



if ($consulta1 != false)
{
    $sql2="INSERT INTO novedad (idTurno,fechaVieja,fechaNueva,horaVieja,horaNueva)
    VALUES ($idtribunalmesa,'$fecha_d_m_y2','$fecha_d_m_y1','$horadefault','$inputHora')
     ";
     $consulta2 = BDConexion::getInstancia()->query($sql2);
     


}








?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/jszip.min.js"></script>
    <script src="../../lib/dataTables/pdfmake.min.js"></script>
    <script src="../../lib/dataTables/vfs_fonts.js"></script>
    <script src="../../lib/dataTables/buttons.flash.min.js"></script>
    <script src="../../lib/dataTables/buttons.html5.min.js"></script>
    <script src="../../lib/dataTables/buttons.print.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Actualizar Mesa de Examen</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h3>Actualizar Examen</h3>
                </div>
                <div class="card-body">
                    <?php if ($consulta1) { ?>
                        <div class="alert alert-success" role="alert">
                            Operaci&oacute;n realizada con &eacute;xito.
                        </div>
                    <?php } ?>
                    <?php if (!$consulta1) { ?>
                        <div class="alert alert-danger" role="alert">
                            Ha ocurrido un error.
                        </div>
                    <?php } ?>
                    <hr />
                    <h5 class="card-text">Opciones</h5>
                    <a href="gestionExamen.php">
                        <button type="button" class="btn btn-primary">
                            <span class="oi oi-account-logout"></span> Salir
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
