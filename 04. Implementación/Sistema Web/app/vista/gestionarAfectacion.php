<?php  
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARAFECTACION);

// $ColeccionDocente = new ColeccionDocentes();
// $ColeccionMesaExamen = new ColeccionMesaExamen();
// $ColeccionTribunal = new ColeccionTribunal();
// $ColeccionAsignaturas = new ColeccionAsignaturas();
// $ColeccionTribunalAsignatura = new ColeccionTribunalAsignatura();
?>
<html>
    <head>
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2-bootstrap.min.css">
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/select2.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/i18n/es.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/afectaciones.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Gestionar Afectaciones</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
<div class="container-fluid">
    <div class="container">
        <div class="row justify-content-around">
          <div class=" col-sm-3">
                <div class="card">
                    <div class="card-header alert-info">
                        B&uacute;squeda Avanzada
                    </div>
                    <div class="card-body">
                    <form id="buscarafectacion" action="afectacion.buscar.php" method="post">
                        <div class="form-group">
                            <div class="form-row">
                            <small>Filtrar resultados por: </small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <label for="inputExpediente">Docente</label>
                            </div>
                           <div class="form-row">
                           <select class="js-example-basic-single form-control" name="docente">
                         </select>
                          </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <label for="inputDesdeIL">Asignatura</label>
                            </div>
                                <div class="form-row">
                                <select class="asignatura form-control" name="asignatura">
                                </select>
                              </div>
                        </div>
                        <br>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-lg" name="Buscar">Realizar B&uacute;squeda</button>
                        </div>
                    </form>
                    <p>
                        <a href="afectacion.crear.php">
                        <button type="button" class="btn btn-success btn-block btn-lg">
                            <span class="oi oi-plus"></span> Agregar Afectación
                        </button>
                        </a>
                    </p>
                    </div>
                </div>
            </div>
            <div class="col-md-9 ml-md-auto">
                <div class="card">
                   <div class="card-header alert-success">
                   <span class="card-title">Listado de Tribunales</span>
                    </div>
                    <div class="card-body" id="resultadotabla">
                        <div class="table-responsive" id= "resultado">
                        <table id="afectaciones" class ="table table-hover table-condensed table-bordered">
                            <thead>
                            <tr scope="row">
                              <td>Asignatura</td>
                              <td>Presidente</td>
                              <td>Vocal 1</td>
                              <td>Vocal 2</td>
                              <td>Suplente</td>
                              <td>Ver más...</td>
                              <td>Editar</td>
                              <td>Eliminar</td>
                            </tr>
                            </thead>
                            <?php
                            
                            $sql='SELECT a.nombre,a.codAsignatura,t.id as tribunalid,ta.id as tribunalasignaturaid,p.id as idpresidente, CONCAT(p.nombre, " ",p.apellido) as presidente,p1.id as idvocal,CONCAT(p1.nombre, " ",p1.apellido)as vocal ,p2.id as idvocal1, CONCAT(p2.nombre, " ",p2.apellido)as vocal1,p3.id as idsuplente,CONCAT(p3.nombre, " ",p3.apellido)as suplente from tribunal t left join tribunalasignatura ta ON t.id=ta.idTribunal left join asignatura a ON a.codAsignatura= ta.idAsignatura left join profesor p on t.presidente = p.id left join profesor p1 on t.vocal = p1.id left join profesor p2 on t.vocal1 = p2.id left join profesor p3 on t.suplente = p3.id';
  
                            $consulta2 = BDConexion::getInstancia()->query($sql);
                            if ($consulta2->num_rows != 0)
                            {
                                $filas='';
                                while($row = $consulta2->fetch_array())
                                {
                                  
                                  if ($row['idsuplente']== NULL && $row['suplente']== NULL )
                                  {
                                    $filas.='
                                    <tr>
                                    <td>'.$row['nombre'].'</td>
                                    <td>'.$row['presidente'].'</td>
                                    <td>'.$row['vocal'].'</td>
                                    <td>'.$row['vocal1'].'</td>
                                    <td> </td>
                                    <td>
                                        <a title="Ver detalle" href="afectacion.ver.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
                                            <button type="button" class="btn btn-outline-info">
                                                <span class="oi oi-zoom-in"></span>
                                            </button></a>

                                    </td>
                                    <td>
                                        <a title="Modificar" href="afectacion.modificar.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
                                            <button type="button" class="btn btn-outline-warning">
                                                <span class="oi oi-pencil"></span>
                                            </button></a>
                                    </td>
                                    <td>
                                        <a title="Eliminar" href="afectacion.eliminar.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
                                          <button class="btn btn-outline-danger" class="btn btn-outline-info">
                                             <span class="oi oi-trash"></span>
                                          </button></a>
                                    </td>
                                </tr>
                                    
                                    
                                    ';



                                  }
                                  else
                                  {
                                      $filas.='
                                      <tr>
                                      <td>'.$row['nombre'].'</td>
                                      <td>'.$row['presidente'].'</td>
                                      <td>'.$row['vocal'].'</td>
                                      <td>'.$row['vocal1'].'</td>
                                      <td>'.$row['suplente'].'</td>
                                     
                                      <td>
                                          <a title="Ver detalle" href="afectacion.ver.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
                                              <button type="button" class="btn btn-outline-info">
                                                  <span class="oi oi-zoom-in"></span>
                                              </button></a>
  
                                      </td>
                                      <td>
                                          <a title="Modificar" href="afectacion.modificar.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
                                              <button type="button" class="btn btn-outline-warning">
                                                  <span class="oi oi-pencil"></span>
                                              </button></a>
                                      </td>
                                      <td>
                                          <a title="Eliminar" href="afectacion.eliminar.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
                                            <button class="btn btn-outline-danger" class="btn btn-outline-info">
                                               <span class="oi oi-trash"></span>
                                            </button></a>
                                      </td>
                                  </tr>
                                      


                                      ';





                                  }












                                }
                            }
                            else
                            {
                                $filas='<div class="alert alert-warning" role="alert">No se encontraron resultados</div>';
                            }
                            echo $filas;









                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            ?>
                           
                      
                    </table>
                    </div>
                  </div>
              </div>
   
            </div>
        </div>
        <br>
    </div>
</div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>




