$(document).ready(function () {
    $.fn.select2.defaults.set( "theme", "bootstrap" );
    $.fn.select2.defaults.set('language', 'es');
    $('.js-example-basic-single').select2({
        placeholder: 'Ingrese el Aula',
        language: {
noResults: function() {

return "No hay resultados";        
},
searching: function() {

return "Buscando..";
}
},
        ajax: {
               url: 'buscaraula.php',
               dataType: 'json',
               delay: 250,
               data: function (data) {
              return { 
                  query: data.term // search term
              };
          },
              processResults: function (data) {
              
                  return {
                        results: data
                         };
              },
                cache: true
              }
  });

  $('.asignatura').select2({
    placeholder: 'Ingrese la Asignatura',
      language: {
noResults: function() {

return "No hay resultados";        
},
searching: function() {

return "Buscando..";
}
},
ajax: {
             url: 'buscarasignatura2.php',
             dataType: 'json',
             delay: 250,
             data: function (data) {
                
            return { 
                query1: data.term // search term
            };
        },
            processResults: function (data) {
                console.log(data)
                return {
                      results: data
                       };
            },
              cache: true
            }


});


$('.carrera').select2({
    placeholder: 'Ingrese la Carrera',
      language: {
noResults: function() {

return "No hay resultados";        
},
searching: function() {

return "Buscando..";
}
},
ajax: {
             url: 'buscarcarrera.php',
             dataType: 'json',
             delay: 250,
             data: function (data) {
                
            return { 
                query3: data.term // search term
            };
        },
            processResults: function (data) {
                console.log(data)
                return {
                      results: data
                       };
            },
              cache: true
            }


});
 

    console.log('asd');
    $('#gestionmesaexamen').DataTable({
        responsive: true,
        dom: 'Bfrtip',
        buttons: [{
            extend: 'excelHtml5',
            filename: 'Mesa de exámenes',
            title: 'Mesa de exámenes ',
            messageTop: 'Mesas',
            autoFilter: true,
            exportOptions: {
                columns: [0,1,2,3,4,5,6,7]
            },
            customize: function (xlsx) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                $('row:first c', sheet).attr('s', '2');
                $('row c[r="A2"]', sheet).attr('s', '2');
                $('row:nth-child(3) c', sheet).each(function () {
                    $(this).attr('s', '22');
                });
            }
        },
        {
            extend: "print",
            text: 'Imprimir',
            title: 'Gerencia de Sistemas y Tecnología - APLICACIONES',
            customize: function (win) {
                $(win.document.body).find('h1').css('font-size', '0pt');
                var css = '@page { size: landscape; }',
                    head = win.document.head || win.document.getElementsByTagName('head')[0],
                    style = win.document.createElement('style');
                style.type = 'text/css';
                style.media = 'print';
                if (style.styleSheet) {
                    style.styleSheet.cssText = css;
                } else {
                    style.appendChild(win.document.createTextNode(css));
                }
                head.appendChild(style);
            },
            exportOptions: {
                columns: ':visible',
                autoPrint: true
            }

        }
        ],
        "language": {
            "decimal": ".",
            "emptyTable": "No hay datos para mostrar",
            "info": "Mostrando resultados del _START_ al _END_ (_TOTAL_ total)",
            "infoEmpty": "del 0 al 0 (0 total)",
            "infoFiltered": "(filtrado de todas las _MAX_ entradas)",
            "infoPostFix": "",
            "thousands": "'",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "pageLength": 5,
            "zeroRecords": "No hay resultados",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": ordenar de manera Ascendente",
                "sortDescending": ": ordenar de manera Descendente ",
            }
        }
    });
    
   

    
    
   
   $('#buscarmesa').on('submit',function (event) {
       console.log('asd');
       event.preventDefault();
    
       $.ajax({
           type: "POST",
           url: "./examen.buscar.php",
           data: new FormData(this),
           processData: false,
           contentType: false,
           beforeSend: function(){

               console.log('asd');
               // $('.modal').show();
               // $('.modal-backdrop').add();
               // $("body").addClass("modal-open");
               // $('#mdCargar').modal('static');
                   $('#staticBackdrop').modal('show');
                
                },
           success: function (data) {
            document.getElementById("resultado").innerHTML = "";
            console.log(data)
            if (data.substring(5,10)=='id="param"')
            {
                alertify.error('No se recibieron los parametros necesarios para la busqueda');
                $('#resultado').html(data);
            }
            else
            {
               $('#resultado').html(data);
               $('#tabladocente').DataTable({
                responsive: true,
                searching: true,
                dom: 'Bfrtip',
                buttons: ['excel'],
                "language": {
                    "decimal": ".",
                    "emptyTable": "No hay datos para mostrar",
                    "info": "del _START_ al _END_ (_TOTAL_ total)",
                    "infoEmpty": "del 0 al 0 (0 total)",
                    "infoFiltered": "(filtrado de todas las _MAX_ entradas)",
                    "infoPostFix": "",
                    "thousands": "'",
                    "lengthMenu": "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "No hay resultados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Último",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "aria": {
                        "sortAscending": ": ordenar de manera Ascendente",
                        "sortDescending": ": ordenar de manera Descendente ",
                    }
                }
            });

            }
               

        
           },
           complete: function () {
               $('#imgCli').hide();
               setTimeout(function () {
                   $('#staticBackdrop').modal('hide');
               }, 1000);  
             },
           error: function (msg) {
               console.log(msg);
               
           }
       });
   });


    //   //formData=new FormData(document.getElementById("addproduct")) 
    //     //console.log(formData);
    //    $('#buscardocente').on('submit',function (event) {
    //        event.preventDefault();
        
    //        $.ajax({
    //            type: "POST",
    //            url: "./docente.buscar.php",
    //            data: new FormData(this),
    //            processData: false,
    //            contentType: false,
    //            beforeSend: function(){
   
    //                console.log('asd');
    //                // $('.modal').show();
    //                // $('.modal-backdrop').add();
    //                // $("body").addClass("modal-open");
    //                // $('#mdCargar').modal('static');
    //                    $('#staticBackdrop').modal('show');
                    
    //                 },
    //            success: function (data) {
    //             document.getElementById("resultado").innerHTML = "";
    //             console.log(data)
    //             if (data.substring(5,10)=='id="param"')
    //             {
    //                 alertify.error('No se recibieron los parametros necesarios para la busqueda');
    //                 $('#resultado').html(data);
    //             }
    //             else
    //             {
    //                $('#resultado').html(data);
    //                $('#tabladocente').DataTable({
    //                 responsive: true,
    //                 searching: true,
    //                 dom: 'Bfrtip',
    //                 buttons: ['excel'],
    //                 "language": {
    //                     "decimal": ".",
    //                     "emptyTable": "No hay datos para mostrar",
    //                     "info": "del _START_ al _END_ (_TOTAL_ total)",
    //                     "infoEmpty": "del 0 al 0 (0 total)",
    //                     "infoFiltered": "(filtrado de todas las _MAX_ entradas)",
    //                     "infoPostFix": "",
    //                     "thousands": "'",
    //                     "lengthMenu": "Mostrar _MENU_ entradas",
    //                     "loadingRecords": "Cargando...",
    //                     "processing": "Procesando...",
    //                     "search": "Buscar:",
    //                     "zeroRecords": "No hay resultados",
    //                     "paginate": {
    //                         "first": "Primero",
    //                         "last": "Último",
    //                         "next": "Siguiente",
    //                         "previous": "Anterior"
    //                     },
    //                     "aria": {
    //                         "sortAscending": ": ordenar de manera Ascendente",
    //                         "sortDescending": ": ordenar de manera Descendente ",
    //                     }
    //                 }
    //             });

    //             }
                   

            
    //            },
    //            complete: function () {
    //                $('#imgCli').hide();
    //                setTimeout(function () {
    //                    $('#staticBackdrop').modal('hide');
    //                }, 1000);  
    //              },
    //            error: function (msg) {
    //                console.log(msg);
                   
    //            }
    //        });
    //    });
   

   
   });