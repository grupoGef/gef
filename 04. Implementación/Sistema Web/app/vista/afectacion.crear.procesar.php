<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARAFECTACION);
include_once '../modelo/BDConexion.Class.php';
include_once '../modelo/ColeccionLicencia.php';
include_once '../modelo/ColeccionDocentes.php';
include_once '../modelo/ColeccionAsignaturas.php';
include_once '../modelo/ColeccionTribunal.php';

$DatosFormulario = $_POST;
$nombreasignatura=$DatosFormulario["asignatura"];
$presidente=$DatosFormulario["presidente"];
$vocal=$DatosFormulario["vocal"];
$vocal1=$DatosFormulario["vocal1"];
$preferenciasprecidente = $DatosFormulario["preferenciapresidente"];
$preferenciasvocal = $DatosFormulario["preferenciavocal"];
$preferenciasvocal1 = $DatosFormulario["preferenciavocal1"];
$preferenciassuplente = $DatosFormulario["preferenciasuplente"];






if (!empty($DatosFormulario["suplente"]))
{
$suplente=$DatosFormulario["suplente"]; 






 
$query = "INSERT INTO TRIBUNAL (presidente,vocal,vocal1,suplente) VALUES (".$presidente.",".$vocal.",".$vocal1.",".$suplente.")";
$consulta = BDConexion::getInstancia()->query($query);
$ColeccionTribunal = new ColeccionTribunal();



$querytribunal="Select id from tribunal where presidente=$presidente and vocal=$vocal and vocal1=$vocal1 and suplente=$suplente";
$consulta = BDConexion::getInstancia()->query($querytribunal);
if ($consulta->num_rows != 0)
{
    $filas='';
    while($row = $consulta->fetch_array())
    {
      $tribunalid=$row['id'];
    }
}

$queryupdate ="UPDATE tribunal SET preferenciapresidente = '".$preferenciasprecidente."' WHERE id = ".$tribunalid."";
$consulta1 = BDConexion::getInstancia()->query($queryupdate);
$queryupdate ="UPDATE tribunal SET preferenciavocal = '".$preferenciasvocal."' WHERE id = ".$tribunalid."";
$consulta1 = BDConexion::getInstancia()->query($queryupdate);
$queryupdate ="UPDATE tribunal SET preferenciavocal1 = '".$preferenciasvocal1."' WHERE id = ".$tribunalid."";
$consulta1 = BDConexion::getInstancia()->query($queryupdate);
$queryupdate ="UPDATE tribunal SET preferenciasuplente = '".$preferenciassuplente."' WHERE id = ".$tribunalid."";
$consulta1 = BDConexion::getInstancia()->query($queryupdate);




$queryasignatura="Select * from asignatura where nombre='$nombreasignatura'";
$consulta = BDConexion::getInstancia()->query($queryasignatura);
if ($consulta->num_rows != 0)
{
    $filas='';
    while($row = $consulta->fetch_array())
    {
      $idasignatura=$row['codAsignatura'];
    }
}
$query = "INSERT INTO tribunalasignatura (idTribunal,idAsignatura) VALUES (".$tribunalid.",".$idasignatura.")";
$consulta2 = BDConexion::getInstancia()->query($query);
}
else
{


   
  $query = "INSERT INTO TRIBUNAL (presidente,vocal,vocal1) VALUES (".$presidente.",".$vocal.",".$vocal1.")";
  $consulta = BDConexion::getInstancia()->query($query);
  $ColeccionTribunal = new ColeccionTribunal();
  
  
  
  $querytribunal="Select * from tribunal where presidente=$presidente and vocal=$vocal and vocal1=$vocal1 ";
  $consulta = BDConexion::getInstancia()->query($querytribunal);
  if ($consulta->num_rows != 0)
  {
      $filas='';
      while($row = $consulta->fetch_array())
      {
        $tribunalid=$row['id'];
      }
  }
  $queryupdate ="UPDATE tribunal SET preferenciapresidente = '".$preferenciasprecidente."' WHERE id = ".$tribunalid."";
$consulta1 = BDConexion::getInstancia()->query($queryupdate);
$queryupdate ="UPDATE tribunal SET preferenciavocal = '".$preferenciasvocal."' WHERE id = ".$tribunalid."";
$consulta1 = BDConexion::getInstancia()->query($queryupdate);
$queryupdate ="UPDATE tribunal SET preferenciavocal1 = '".$preferenciasvocal1."' WHERE id = ".$tribunalid."";
$consulta1 = BDConexion::getInstancia()->query($queryupdate);
  
  
  $queryasignatura="Select * from asignatura where nombre='$nombreasignatura'";
  $consulta = BDConexion::getInstancia()->query($queryasignatura);
  if ($consulta->num_rows != 0)
  {
      $filas='';
      while($row = $consulta->fetch_array())
      {
        $idasignatura=$row['codAsignatura'];
      }
  }
  $query = "INSERT INTO tribunalasignatura (idTribunal,idAsignatura) VALUES (".$tribunalid.",".$idasignatura.")";
  $consulta2 = BDConexion::getInstancia()->query($query);
}







if (!$consulta && !$consulta2) {
    BDConexion::getInstancia()->rollback();
    //arrojar una excepcion
    die(BDConexion::getInstancia()->errno);
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Crear Afectacion</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <p></p>
            <div class="card">
                <div class="card-header">
                    <h3>Crear Tribunal</h3>
                </div>
                <div class="card-body">
                    <?php if ($consulta && $consulta2) { ?>
                        <div class="alert alert-success" role="alert">
                            Operaci&oacute;n realizada con &eacute;xito.
                        </div>
                    <?php } ?>
                    <?php if (!$consulta && !$consulta2) { ?>
                        <div class="alert alert-danger" role="alert">
                            Ha ocurrido un error.
                        </div>
                    <?php } ?>
                    <hr />
                    <h5 class="card-text">Opciones</h5>
                    <a href="gestionarAfectacion.php">
                        <button type="button" class="btn btn-primary">
                            <span class="oi oi-account-logout"></span> Salir
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
