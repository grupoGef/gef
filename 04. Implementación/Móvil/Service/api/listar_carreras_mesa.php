<?php

header("Access-Control-Allow-Origin: *");

include_once '../config/inc_config.php';
include_once '../app/principal/modelo/Encriptador.php';
include_once './MySQL.php';

$instancia = MySQL::getInstancia();

$query = "SELECT DISTINCT car.codCarrera as 'codigo', car.nombre as 'nombreLargo' "
        . " FROM mesa_examen mex "
            . " INNER JOIN mesa_examen_carrera mexcar on mex.codMesa = mexcar.codMesa "
            . " INNER JOIN carrera car on mexcar.codCarrera = car.codCarrera "
            . " INNER JOIN asignatura asi on  mex.codAsignatura = asi.codAsignatura "
        . " ORDER BY asi.nombre";
$resultado = $instancia->query($query);
if ($resultado) {
    if ($resultado->num_rows > 0) {
        $carreras = $resultado->fetch_all(MYSQLI_ASSOC);
        $response = array('estado' => 'OK', 'carreras' => $carreras);
    } else {
        $response = array('estado' => 'BAD', 'datos' => 'No se encontraron carreras cargadas');
    }
} else {
    $response = array('estado' => 'BAD', 'datos' => 'Error al consultar carreras');
}

echo json_encode($response);




