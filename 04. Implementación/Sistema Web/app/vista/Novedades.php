<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARNOVEDADES);
include_once '../modelo/ColeccionDocentes.php';
include_once '../modelo/ColeccionLicencia.php';
include_once '../modelo/ColeccionTipoLicencia.php';
include_once '../modelo/ColeccionDepartamento.php';
date_default_timezone_set('America/Argentina/Buenos_Aires');
 ?>
<html>
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/novedades.js"></script>
    <style>
    </style>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Gestionar Novedades</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
<div class="container-fluid">
    <div class="container">
        <div class="row justify-content-around">
          <div class=" col-sm-3">
                <div class="card">
                    <div class="card-header alert-info">
                        B&uacute;squeda Avanzada
                    </div>
                    <div class="card-body">
                    <form id="buscarnovedad" action="novedadesbuscar.php" method="post">
                        <div class="form-group">
                            <div class="form-row">
                                <small>Filtrar resultados por:</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <label for="inputExpediente">Nombre del Docente</label>
                            </div>
                            <div class="form-row">
                                <input type="text" class="form-control" name="docenteb" id="inputDocente" oninput="validar('inputDocente')" name="inputDocente" value=""pattern="[A-Z]{4-23}">
                                <small id="inputAsignaturas" class="form-text text-muted"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <label for="inputDesdeIL">Fecha de Inicio</label>
                            </div>
                            <div class="form-row">
                                <input type="date" class="form-control " name="fechainiciob" id="inputFechainicio" oninput="validar('inputFechainicio')" name="inputFechainicio" value="" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" title="Ingrese formato de fecha dd/mm/yyyy">
                                <small id="inputDocentes" class="form-text text-muted"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <label for="inputDesdeIL">Fecha de Finalizacion</label>
                            </div>
                            <div class="form-row">
                                <input type="date" class="form-control " name="fechafinalb" id="inputfechafinalizacion"oninput="validar('inputfechafinalizacion')" name="inputFechafinalizacion" value="" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" title="Ingrese formato de fecha dd/mm/yyyy">
                                <small id="inputFecha" class="form-text text-muted"></small>
                            </div>
                        </div>

                        <br>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-lg" name="Buscar">Realizar B&uacute;squeda</button>
                        </div>
                        <p id = "mensajeError">
                        </p>
                    </form>
                    <p>
                        <a href="novedad.crear.php">
                        <button type="button" class="btn btn-success btn-block btn-lg">
                            <span class="oi oi-plus"></span> Agregar Novedad
                        </button>
                        </a>
                    </p>
                    </div>
                </div>
            </div>
            <div class="col-md-9 ml-md-auto">
                <div class="card">
                   <div class="card-header alert-success">
                   <span class="card-title">Listado de Novedades</span>
                    </div>
                    <div class="card-body">
                        <div id="resultadonovedades">
                        <table id="novedades" class ="table table-hover table-condensed table-bordered">
                         <thead>   
                        <tr scope="row">
                                <td>Nombre y Apellido</td>
                                <td>Departamento</td>
                                <td width="700">Dia de inicio</td>
                                <td width="700">Dia de fin</td>
                                <td>Régimen</td>
                                <td>Ver más...</td>
                                <td>Editar</td>
                                <td>Eliminar</td>
                            </tr>
                            </thead>
                           

                      <?php 


                    $SQL= "Select p.id,p.nombre,p.apellido,d.nombre as departamento,d.id as iddepartamento,l.id as idlicencia,l.fechaInicio,l.fechaFinal, tl.idLicencia as tipolicenciaid,tl.nombre as tipolicencianombre, tl.descripcion FROM
                    licencia l left join profesor p on l.idProfesor=p.id 
                    left JOIN
                    departamento d on p.idDepartamento=d.id
                    left join 
                   tipo_licencia tl 
                   ON l.id=tl.idLicencia";
                    $reportebuscar= BDConexion::getInstancia()->query($SQL);
                                            
                    if ($reportebuscar->num_rows != 0)
                        {
                            $filas='';
                            while($row = $reportebuscar->fetch_array())
                            {
                                $datetime = date_create(date("d-m-Y"));
                                $Color="";
                                $datetimecomparable= $row['fechaFinal'];
                               
                                $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                                $fechac=$datetime2->format('d-m-Y');
                                $datetime22=date_create($fechac);
                              
                                if ($datetime22 <= $datetime)
                                {
                                  $Color="#9c9c9c";
                                }
                                else
                                {
                                  $Color="#ffffff";
                                }
                                $fecha = new DateTime($datetimecomparable= $row['fechaInicio']);
                                $fecha_d_m_y1 = $fecha->format('d-m-Y');
                                $fecha = new DateTime($datetimecomparable= $row['fechaFinal']);
                                $fecha_d_m_y2 = $fecha->format('d-m-Y');

                                echo '
                                <tr>

                                <td style="background-color: '.$Color.'" >'.$row['nombre'].' '.$row['apellido'].'</td>
                                <td style="background-color:'.$Color.'" >'.$row['departamento'].'</td>
                                <td style="background-color:'.$Color.'" >'.$fecha_d_m_y1.'</td>
                                <td style="background-color:'.$Color.'"  width="700">'.$fecha_d_m_y2.'</td>
                                <td style="background-color:'.$Color.'" >'.$row['tipolicencianombre'].'</td>
                                <td style="background-color:'.$Color.'" >
                                    <a title="Ver detalle" href="novedad.ver.php?id='.$row['idlicencia'].'&id1='.$row['tipolicenciaid'].'&id2='.$row['id'].'">
                                        <button type="button" class="btn btn-outline-info">
                                            <span class="oi oi-zoom-in"></span>
                                        </button></a>
       
                                </td>
                                <td style="background-color:'.$Color.'" >
                                    <a title="Modificar" href="novedad.modificar.php?id='.$row['idlicencia'].'&id1='.$row['tipolicenciaid'].'&id2='.$row['id'].'">
                                        <button type="button" class="btn btn-outline-warning">
                                            <span class="oi oi-pencil"></span>
                                        </button></a>
                                </td>
                                <td style="background-color:'.$Color.'">
                                    <a title="Eliminar" href="novedades.eliminar.php?id='.$row['idlicencia'].'&id1='.$row['tipolicenciaid'].'&id2='.$row['id'].'">
                                      <button class="btn btn-outline-danger" class="btn btn-outline-info">
                                         <span class="oi oi-trash"></span>
                                      </button></a>
                                </td>
                                </tr>

                                ';
                            }
                        }
                        else
                        {
                          echo '<div class="alert alert-warning" role="alert">No se encontraron resultados</div>';
                        } 



                      ?>


                      
                      
             
                    </table>
                    </div>
                  </div>
              </div>

            </div>
        </div>
        <br>
    </div>
</div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
