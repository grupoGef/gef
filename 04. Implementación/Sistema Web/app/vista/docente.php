<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARDOCENTE);
include_once '../modelo/ColeccionDocentes.php';
include_once '../modelo/ColeccionCargo.php';
include_once '../modelo/ColeccionDepartamento.php';
$ColeccionDocente = new ColeccionDocentes();
$ColeccionCargo = new ColeccionCargo();
$ColeccionDepartamento = new ColeccionDepartamento(); ?>
<html>
    <head>
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2-bootstrap.min.css">
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/select2.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/i18n/es.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/jszip.min.js"></script>
    <script src="../../lib/dataTables/pdfmake.min.js"></script>
    <script src="../../lib/dataTables/vfs_fonts.js"></script>
    <script src="../../lib/dataTables/buttons.flash.min.js"></script>
    <script src="../../lib/dataTables/buttons.html5.min.js"></script>
    <script src="../../lib/dataTables/buttons.print.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/docente.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Gestionar Docentes</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
<div class="container-fluid">
    <div class="container">
        <div class="row justify-content-around">
          <div class=" col-sm-3">
                <div class="card">
                    <div class="card-header alert-info">
                        B&uacute;squeda Avanzada
                    </div>
                    <div class="card-body">
                    <form action="docente.buscar.php" name="buscardocente" id="buscardocente" method="post">
                        <div class="form-group">
                            <div class="form-row">
                                <small>Filtrar resultados por: </small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <label for="inputExpediente">Nombre del docente</label>
                            </div>
                            <div class="form-row">
                                <input type="text" class="form-control" id="inputDocenteb" name="buscardocente" value="" oninput="validar('inputDocenteb')" pattern="^[a-zA-Z]{4,}\s?([a-zA-Z]{4,})?">
                                <small id="inputAsignaturas" class="form-text text-muted"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <label for="inputDesdeIL">Número de DNI</label>
                            </div>
                            <div class="form-row">
                                <input type="text" class="form-control " id="inputDnib" name="buscardni" value="" oninput="validar('inputDnib')" pattern="[0-9]{8}">
                                <small id="inputDocentes" class="form-text text-muted"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <label for="inputDesdeIL">Cargo</label>
                            </div>
                            <div class="form-row">
                               <select  class="form-control " name="buscarcargo" >
                               <option>Ingresar Cargo </option>
                               <option value="Titular">Titular</option>
                               <option value="Asociado">Asociado</option>
                               <option value="Adjunto">Adjunto</option>
                               <option value="Adjunto">Asistente</option>
                               <option value="Ayudante">Ayudante</option>
                          </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-lg" name="Buscar">Realizar B&uacute;squeda</button>
                        </div>
                    </form>
                    <p>
                        <a href="docente.crear.php">
                        <button type="button" class="btn btn-success btn-block btn-lg">
                            <span class="oi oi-plus"></span> Nuevo Docente
                        </button>
                        </a>
                    </p>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                   <div class="card-header alert-success">
                   <span class="card-title">Listado de Docentes</span>
                    </div>
                    <div class="card-body " >
                        <div class="table-responsive " id= "resultado">
                            <table name="tabladocente" id="tabladocente" class ="table table-hover table-condensed table-bordered ">
                            <thead>
                            <tr scope="row">
                              <td>Nombre</td>
                              <td>Apellido</td>
                              <td>Categoría</td>
                              <td>Dedicación</td>
                              <td>Departamento</td>
                              <td>Ver más...</td>
                              <td>Editar</td>
                              <td>Eliminar</td>
                            </tr>
                            </thead>
                            <tbody>
                           
                                <?php 
                            
                             
                                
                            $reporte ="SELECT P.nombre,P.dni,P.categoria,P.categoria ,P.apellido,P.id,P.idDepartamento,D.nombre AS departamento,D.id AS idDepartamento,C.id AS idcargo,C.categoria,C.Dedicacion FROM profesor P INNER JOIN cargo C
                            ON P.id=C.idProfesor INNER JOIN departamento D
                            ON P.idDepartamento = D.id   ";
                            
                            
                                            $reportebuscar= BDConexion::getInstancia()->query($reporte);
                                            
                                        if ($reportebuscar->num_rows != 0)
                                            {
                                                while($row = $reportebuscar->fetch_array())
                                                {
                                                    echo '
                                                    <tr>
                                                    <td> '.$row['nombre']. '</td>
                                                    <td>'.$row['apellido']. '</td>
                                                    <td>'.$row['categoria']. '</td>
                                                    <td>'.$row['Dedicacion']. '</td>
                                                    <td>'.$row['departamento']. '</td>
                                                    <td>
                                                        <a title="Ver detalle" href="docente.ver.php?id='.$row['id'].'&id1='.$row['idcargo'].'&id2='.$row['idDepartamento'].'">
                                                            <button type="button" class="btn btn-outline-info">
                                                                <span class="oi oi-zoom-in"></span>
                                                            </button></a>
                
                                                    </td>
                                                    <td>
                                                        <a title="Modificar" href="docente.modificar.php?id='.$row['id'].'&id1='.$row['idcargo'].'">
                                                            <button type="button" class="btn btn-outline-warning">
                                                                <span class="oi oi-pencil"></span>
                                                            </button></a>
                                                    </td>
                                                    <td>
                                                        <a title="Eliminar" href="docente.eliminar.php?id='.$row['id'].'&id1='.$row['idcargo'].'">
                                                          <button class="btn btn-outline-danger" class="btn btn-outline-info">
                                                             <span class="oi oi-trash"></span>
                                                          </button></a>
                                                    </td>
                                                </tr>

                                                    ';

                                                }
                                                
                            
                                            }
                                         else
                                         {
                                           echo '<div class="alert alert-warning" role="alert">No se encontraron resultados</div>';
                                         }   
                                     ?>
                   
                    </tbody>
                    </table>

                    </div>

                  </div>
              </div>

            </div>
        </div>
        <br>
    </div>
</div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
