import {Component, OnInit} from '@angular/core';
import {ExamenesService} from "../../services/examenes.service";
import {Mesa} from "../../interfaces/interfaces";

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage{

  mesas: Mesa[] = [];

  constructor(private examenesService: ExamenesService) {}

  consultarMesasCarrera(){
    this.mesas = [];
    this.examenesService.consultarNotificacion()
      .subscribe(resp => {
          console.log(resp);
          resp['estado'] != "BAD" ? this.mesas.push( ...resp.mesas ) : '';
        }
      );
  }

}
