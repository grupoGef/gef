SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `bdusuarios` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bdusuarios`;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CargaPrimerllamado` (IN `dia1` VARCHAR(60) CHARSET utf8, IN `fecha1` VARCHAR(60), IN `dia2` VARCHAR(60) CHARSET utf8, IN `fecha2` VARCHAR(60), IN `dia3` VARCHAR(60) CHARSET utf8, IN `fecha3` VARCHAR(60), IN `dia4` VARCHAR(60) CHARSET utf8, IN `fecha4` VARCHAR(60), IN `dia5` VARCHAR(60) CHARSET utf8, IN `fecha5` VARCHAR(60), IN `turnact` VARCHAR(2), IN `tipomg` VARCHAR(60))  BEGIN
DECLARE dia1mesa nvarchar(30) DEFAULT 0;
SET dia1mesa = dia1;




DELETE FROM mesa_examen;
DELETE FROM turno_mesaexamen;

CREATE TEMPORARY TABLE dia1 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia1 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,p1.preferencias, fecha1 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where p1.preferencias = dia1mesa
;

CREATE TEMPORARY TABLE dia2 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia2 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,p1.preferencias, fecha2 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where p1.preferencias = dia2
;
CREATE TEMPORARY TABLE dia3 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia3 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,p1.preferencias, fecha3 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where p1.preferencias = dia3
;
CREATE TEMPORARY TABLE dia4 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia4 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,p1.preferencias, fecha4 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where p1.preferencias = dia4
;
CREATE TEMPORARY TABLE dia5 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia5 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,p1.preferencias, fecha5 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where p1.preferencias = dia5
;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia1;
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d1.fecha from dia1 d1 left JOIN
mesa_examen m on d1.codAsignatura=m.codAsignatura
where d1.codTribunal=m.idTribunal;


INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia2;
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d2.fecha from dia2 d2 left JOIN
mesa_examen m on d2.codAsignatura=m.codAsignatura
where d2.codTribunal=m.idTribunal;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia3;
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d3.fecha from dia3 d3 left JOIN
mesa_examen m on d3.codAsignatura=m.codAsignatura
where d3.codTribunal=m.idTribunal;


INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia4;
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d4.fecha from dia4 d4 left JOIN
mesa_examen m on d4.codAsignatura=m.codAsignatura
where d4.codTribunal=m.idTribunal;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia5;
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d5.fecha from dia5 d5 left JOIN
mesa_examen m on d5.codAsignatura=m.codAsignatura
where d5.codTribunal=m.idTribunal;

UPDATE turno_mesaexamen
Set idTurno = 1;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CargaSegundollamado` (IN `dia1` VARCHAR(60) CHARSET utf8, IN `fecha1` VARCHAR(60), IN `dia2` VARCHAR(60) CHARSET utf8, IN `fecha2` VARCHAR(60), IN `dia3` VARCHAR(60) CHARSET utf8, IN `fecha3` VARCHAR(60), IN `dia4` VARCHAR(60) CHARSET utf8, IN `fecha4` VARCHAR(60), IN `dia5` VARCHAR(60) CHARSET utf8, IN `fecha5` VARCHAR(60), IN `turnact` INT, IN `tipomg` VARCHAR(60) CHARSET utf8)  BEGIN 

DECLARE dia1mesa nvarchar(30) DEFAULT 0;
SET dia1mesa = dia1;
DROP TEMPORARY TABLE IF EXISTS dia1;
DROP TEMPORARY TABLE IF EXISTS dia2;
DROP TEMPORARY TABLE IF EXISTS dia3;
DROP TEMPORARY TABLE IF EXISTS dia4;
DROP TEMPORARY TABLE IF EXISTS dia5;


CREATE TEMPORARY TABLE dia1 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia1 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,p1.preferencias, fecha1 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where p1.preferencias = dia1mesa
;

CREATE TEMPORARY TABLE dia2 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia2 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,p1.preferencias, fecha2 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where p1.preferencias = dia2
;
CREATE TEMPORARY TABLE dia3 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia3 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,p1.preferencias, fecha3 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where p1.preferencias = dia3
;
CREATE TEMPORARY TABLE dia4 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia4 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,p1.preferencias, fecha4 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where p1.preferencias = dia4
;
CREATE TEMPORARY TABLE dia5 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia5 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,p1.preferencias, fecha5 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where p1.preferencias = dia5
;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia1
where codAsignatura not in (Select codAsignatura from mesa_examen);
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d1.fecha from dia1 d1 left JOIN
mesa_examen m on d1.codAsignatura=m.codAsignatura
where d1.codTribunal=m.idTribunal;


INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia2
where codAsignatura not in (Select codAsignatura from mesa_examen);
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d2.fecha from dia2 d2 left JOIN
mesa_examen m on d2.codAsignatura=m.codAsignatura
where d2.codTribunal=m.idTribunal;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia3
where codAsignatura not in (Select codAsignatura from mesa_examen);
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d3.fecha from dia3 d3 left JOIN
mesa_examen m on d3.codAsignatura=m.codAsignatura
where d3.codTribunal=m.idTribunal;


INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia4
where codAsignatura not in (Select codAsignatura from mesa_examen);
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d4.fecha from dia4 d4 left JOIN
mesa_examen m on d4.codAsignatura=m.codAsignatura
where d4.codTribunal=m.idTribunal;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia5
where codAsignatura not in (Select codAsignatura from mesa_examen);
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d5.fecha from dia5 d5 left JOIN
mesa_examen m on d5.codAsignatura=m.codAsignatura
where d5.codTribunal=m.idTribunal;

UPDATE turno_mesaexamen
Set idTurno = 1
where idTurno=0;

END$$

DELIMITER ;

CREATE TABLE `asignatura` (
  `codAsignatura` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `departamento` varchar(45) NOT NULL,
  `contenidosMinimos` text NOT NULL,
  `idCarrera` int(2) DEFAULT NULL,
  `idProfesor` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `asignatura` (`codAsignatura`, `nombre`, `departamento`, `contenidosMinimos`, `idCarrera`, `idProfesor`) VALUES
(2, 'Analisis Matematico I', 'EXACTAS Y NATURALES', '1', 16, 0),
(3, 'Matematica discreta', 'EXACTAS Y NATURALES', '1', 16, 0),
(4, 'Organizacion de las computadoras', 'EXACTAS Y NATURALES', '1', 16, 0),
(5, 'Analisis y Produccion del Discurso', 'EXACTAS Y NATURALES', '1', 16, 0),
(6, 'Base de datos', 'EXACTAS Y NATURALES', '2', 16, 0),
(7, 'Analisis y Diseno del Software', 'EXACTAS Y NATURALES', '2', 16, 0),
(8, 'Ciencia, Universidad y Sociedad', 'EXACTAS Y NATURALES', '2', 16, 0),
(9, 'Estructura de Datos', 'EXACTAS Y NATURALES', '2', 16, 0),
(10, 'Sistemas Operativos Distribuidos', 'EXACTAS Y NATURALES', '3', 16, 0),
(11, 'Gestion de Proyectos de Software', 'EXACTAS Y NATURALES', '3', 16, 0),
(12, 'Laboratorio de Desarrollo de Software', 'EXACTAS Y NATURALES', '3', 16, 0),
(13, 'Estadistica I', 'EXACTAS Y NATURALES', '3', 16, 0),
(14, 'Patrimonio Turistico: Circuito I', 'SOCIALES', '1', NULL, 0),
(15, 'Ingles I', 'SOCIALES', '1', 16, 0),
(16, 'Geografia', 'SOCIALES', '1', NULL, 0),
(17, 'Procesos Historicos', 'SOCIALES', '1', NULL, 0),
(18, 'Analisis y Produccion del Discurso', 'SOCIALES', '1', NULL, 0),
(19, 'Ingles II', 'SOCIALES', '2', NULL, 0),
(20, 'Metodologia de la investigacion', 'SOCIALES', '2', NULL, 0),
(21, 'Servicios Turisticos', 'SOCIALES', '2', NULL, 0),
(22, 'Ciencia, Universidad y Sociedad', 'SOCIALES', '2', NULL, 0),
(23, 'Introduccion a la Estadistica', 'SOCIALES', '2', NULL, 0),
(24, 'Legislacion Turistica Patrimonial y Ambiental', 'SOCIALES', '3', NULL, 0),
(25, 'Geografia Turistica Ev Impacto Amb ', 'SOCIALES', '3', NULL, 0),
(28, 'Practica Profesional I', 'SOCIALES', '3', NULL, 0),
(29, 'Conservacion de Sitios Culturales Arqueologicos y Paleontologicos', 'SOCIALES', '4', NULL, 0),
(30, 'Politica del Turismo', 'SOCIALES', '4', NULL, 0),
(33, 'Gestion y Administracion de Empresas Turisticas', 'SOCIALES', '4', NULL, 0),
(34, 'Practica II', 'SOCIALES', '4', NULL, 0),
(35, 'Proceso y Desarrollo de Software', 'EXACTAS Y NATURALES', '3', 16, 0),
(36, 'Analisis Matematico II', 'EXACTAS Y NATURALES', '5', 16, 0),
(37, 'Programacion Orientada a Objetos', 'EXACTAS Y NATURALES', '2', 16, 0),
(38, 'Arquitectura de las Computadoras', 'EXACTAS Y NATURALES', '5', 16, 0),
(39, 'Requerimientos de Software', 'EXACTAS Y NATURALES', '5', 16, 0),
(40, 'Sistemas Operativos', 'EXACTAS Y NATURALES', '5', 16, 0),
(41, 'Laboratorio de Programacion', 'EXACTAS Y NATURALES', '5', 16, 0),
(42, 'Redes y Telecomunicaciones', 'EXACTAS Y NATURALES', '13', 16, 0),
(43, 'Fundamentos de la ciencia y la computacion', 'EXACTAS Y NATURALES', '3', 16, 0),
(44, 'Enseñanza y curriculum', 'SOCIALES', '1', 60, 0),
(45, 'Didactica especial', 'SOCIALES', '1', 60, 0),
(46, 'Aprendizaje', 'SOCIALES', '1', 60, 0),
(47, 'Lengua y cultura latinas i', 'SOCIALES', '1', 60, 0),
(48, 'Literatura griega i', 'SOCIALES', '1', 60, 0),
(49, 'Historia de la lengua', 'SOCIALES', '1', 60, 0),
(50, 'Lingüistica i', 'SOCIALES', '1', 60, 0),
(51, 'Lingüistica ii', 'SOCIALES', '1', 60, 0),
(52, 'Seminario de lingüistica', 'SOCIALES', '1', 60, 0),
(53, 'Seminario de teoria literaria', 'SOCIALES', '1', 60, 0),
(54, 'Idioma moderno ingles', 'SOCIALES', '1', 60, 0),
(55, 'Literatura española ii', 'SOCIALES', '1', 60, 0),
(56, 'HERRAMIENTAS DE INFORMATICA', 'SOCIALES', '1', 918, 0),
(57, 'TEORIA DE LA COMUNICACION II', 'SOCIALES', '1', 918, 0),
(58, 'TALLER DE DISEÑO GRAFICO Y FOTOGRAFÍA ', 'SOCIALES', '1', 918, 0),
(59, 'METODOLOGIA DE LA INVESTIG. EN COMUNICACION', 'SOCIALES', '1', 918, 0),
(60, 'SEMINARIO DE INVESTIG PERIOD', 'SOCIALES', '1', 918, 0),
(61, 'INTRODUCCION A LOS MEDIOS MASIVOS', 'SOCIALES', '1', 918, 0);

CREATE TABLE `aula` (
  `codAula` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `sector` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `aula` (`codAula`, `nombre`, `sector`) VALUES
(1, 'A6', 'A'),
(2, 'A5', 'A'),
(3, '1', 'A'),
(4, '2', 'A'),
(5, '3', 'A'),
(6, 'Laboratorio 4', 'A'),
(9, '7', 'A'),
(10, '8', 'A'),
(11, '9', 'A'),
(12, '10', 'A'),
(13, '11', 'A'),
(14, '12', 'A'),
(15, '13', 'A'),
(16, '14', 'A'),
(17, 'Box 1', 'B'),
(18, 'Box 2', 'B'),
(19, 'Box 3', 'B'),
(20, 'Box 4', 'B'),
(21, 'Box 5', 'B'),
(22, 'Box 6', 'B'),
(23, 'Box 7', 'B'),
(24, 'Box 8', 'B'),
(25, 'Box 9', 'B'),
(26, 'Box 10', 'B'),
(27, 'Box 11', 'B'),
(28, 'Box 12', 'B'),
(29, 'Box 13', 'B'),
(30, 'Box 14', 'B');

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `dedicacion` varchar(45) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `idProfesor` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `cargo` (`id`, `dedicacion`, `categoria`, `idProfesor`) VALUES
(1, 'Exclusiva', 'Titular', 1),
(2, 'Parcial', 'Adjunto', 2),
(35, 'Exclusiva', 'Adjunto', 3),
(36, 'Parcial', 'Asociado', 4),
(37, 'Exclusiva', 'Titular', 5),
(38, 'Exclusiva', 'Adjunto', 6),
(39, 'Parcial', 'Ayudante', 7),
(40, 'Exclusiva', 'Titular', 8),
(41, 'Parcial', 'Asociado', 9),
(42, 'Simple', 'Adjunto', 10),
(43, 'Exclusiva', 'Titular', 11),
(44, 'Exclusiva', 'Adjunto', 12),
(45, 'Parcial', 'Ayudante', 13),
(46, 'Simple', 'Adjunto', 14),
(47, 'Exclusiva', 'Titular', 15),
(48, 'Exclusiva', 'Adjunto', 16),
(49, 'Exclusiva', 'Adjunto', 17),
(50, 'Simple', 'Ayudante', 18),
(51, 'Exclusiva', 'Adjunto', 19),
(52, 'Exclusiva', 'Titular', 20),
(53, 'Exclusiva', 'Adjunto', 21),
(54, 'Simple', 'Adjunto', 22),
(55, 'Exclusiva', 'Titular', 23),
(56, 'Exclusiva', 'Adjunto', 24),
(57, 'Exclusiva', 'Ayudante', 25),
(58, 'Exclusiva', 'Adjunto', 26),
(59, 'Exclusiva', 'Titular', 27),
(60, 'Exclusiva', 'Titular', 28),
(61, 'Exclusiva', 'Ayudante', 29),
(62, 'Simple', 'Asociado', 30),
(63, 'Exclusiva', 'Titular', 31),
(64, 'Exclusiva', 'Titular', 32);

CREATE TABLE `carrera` (
  `codCarrera` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `carrera` (`codCarrera`, `nombre`) VALUES
(3, 'Profesorado en Historia'),
(4, 'Profesorado en Geografía'),
(16, 'Analista de Sistemas'),
(23, 'Ingenieria en Recursos Naturales Renovables'),
(38, 'Licenciatura En Turismo'),
(45, 'Licenciatura en Psicopedagogia'),
(46, 'Enfermeria Universitaria'),
(47, 'Licenciatura en Enfermeria'),
(60, 'Licenciatura en Letras'),
(61, 'Profesorado en Letras'),
(64, 'Licenciatura en Geografia'),
(72, 'Licenciatura en Sistemas'),
(74, 'Licenciatura en Trabajo Social'),
(76, 'Tecnicatura Universitaria en Acompañamiento Terapeutico'),
(912, 'Tecnicatura Universitaria en Gestion de Organizaciones'),
(913, 'Licenciatura en Administracion'),
(914, 'Profesorado en Economia y Gestión de Organizaciones'),
(918, 'Licenciatura en Comunicación Social');

CREATE TABLE `correlativad` (
  `id` int(11) NOT NULL,
  `requisito` enum('Regular','Aprobada') DEFAULT NULL,
  `codAsignatura` int(10) UNSIGNED NOT NULL,
  `codAsignatura_Correlatividad` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `departamento` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `departamento` (`id`, `nombre`) VALUES
(1, 'EXACTAS Y NATURALES'),
(2, 'SOCIALES');

CREATE TABLE `fecha` (
  `dia` varchar(45) NOT NULL,
  `mes` varchar(45) NOT NULL,
  `año` varchar(45) NOT NULL,
  `idLlamado` int(10) UNSIGNED NOT NULL,
  `indice` int(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `licencia` (
  `id` int(10) UNSIGNED NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFinal` date NOT NULL,
  `idProfesor` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `llamado_mesa_examen` (
  `idLlamado` int(10) UNSIGNED NOT NULL,
  `codMesa` int(10) UNSIGNED NOT NULL,
  `hora` time(5) NOT NULL,
  `dia` int(2) UNSIGNED NOT NULL,
  `fechaUnica` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `llamado_mesa_examen` (`idLlamado`, `codMesa`, `hora`, `dia`, `fechaUnica`) VALUES
(1, 1, '00:00:12.00000', 1, '2021-11-01'),
(2, 1, '00:00:12.00000', 12, '2021-11-02'),
(3, 2, '00:00:12.00000', 1, '0000-00-00'),
(4, 2, '00:00:12.00000', 12, '0000-00-00');

CREATE TABLE `mesa_examen` (
  `codMesa` int(10) UNSIGNED NOT NULL,
  `tipo` enum('todoTiempo','general','extraordinaria') NOT NULL,
  `idTribunal` int(10) UNSIGNED NOT NULL,
  `codAsignatura` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `mesa_examen` (`codMesa`, `tipo`, `idTribunal`, `codAsignatura`) VALUES
(122, 'general', 29, 40),
(123, 'general', 30, 41),
(124, 'general', 35, 6),
(125, 'general', 38, 45),
(126, 'general', 47, 54),
(127, 'general', 48, 55),
(129, 'general', 23, 22),
(130, 'general', 24, 35),
(131, 'general', 28, 39),
(132, 'general', 32, 43),
(133, 'general', 33, 13),
(134, 'general', 34, 11),
(135, 'general', 36, 12),
(136, 'general', 39, 46),
(137, 'general', 40, 47),
(138, 'general', 43, 50),
(139, 'general', 44, 51),
(144, 'general', 31, 42),
(145, 'general', 42, 49),
(146, 'general', 45, 52),
(147, 'general', 46, 53),
(151, 'general', 25, 36),
(152, 'general', 26, 37),
(153, 'general', 27, 38),
(154, 'general', 37, 44),
(155, 'general', 41, 48);

CREATE TABLE `mesa_examen_carrera` (
  `codMesa` int(10) UNSIGNED NOT NULL,
  `codCarrera` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `mesa_examen_carrera` (`codMesa`, `codCarrera`) VALUES
(1, 16),
(2, 16),
(3, 16);

CREATE TABLE `permiso` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `permiso` (`id`, `nombre`) VALUES
(7, 'Usuarios'),
(8, 'Roles'),
(9, 'Permisos'),
(11, 'Salir'),
(12, 'Ingresar');

CREATE TABLE `profesor` (
  `id` int(10) NOT NULL,
  `dni` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `preferencias` varchar(60) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `dedicacion` varchar(45) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `idDepartamento` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `profesor` (`id`, `dni`, `nombre`, `apellido`, `preferencias`, `email`, `dedicacion`, `categoria`, `idDepartamento`) VALUES
(1, 47490271, 'Claudia', 'Mansilla', 'Jueves', 'Claudiamansilla@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(2, 78259508, 'Daniel', 'Jaremchuck', 'Miercoles', 'danieljaremchuk@unpa.edu.ar', 'Parcial', 'Adjunto', 2),
(3, 65340726, 'Yamile', 'Carcamo', 'Viernes', 'YCarcamo@unpa.edu.ar', 'Exclusiva', 'Adjunto', 2),
(4, 79041044, 'Carolina', 'Musci', 'Viernes', 'CarolinaMusci@unpa.edu.ar', 'Parcial', 'Asociado', 2),
(5, 89649464, 'Karim Omar', 'Hallar', 'Jueves', 'karimhallar@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(6, 98972597, 'Leonardo', 'Gonzales', 'Viernes', 'LGonzales@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(7, 78348430, 'Rodrigo', 'Farias', 'Jueves', 'fariasr@unpa.edu.ar', 'Parcial', 'Ayudante', 1),
(8, 30504125, 'Sandra', 'Vilca', 'Lunes', 'SVilca@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(9, 66133151, 'Valeria', 'Forchino', 'Jueves', 'Vforchino@unpa.edu.ar', 'Parcial', 'Asociado', 1),
(10, 68851196, 'Laura', 'Villanueva', 'Jueves', 'Lauravillanueva@unpa.edu.ar', 'Simple', 'Adjunto', 1),
(11, 11325222, 'Sandra', 'Casas', 'Martes', 'Scasas@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(12, 91669859, 'Claudio', 'Saldivia', 'Martes', 'Saldiviaclaudio@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(13, 88576924, 'Franco', 'Herrera', 'Miercoles', 'Fherrera@unpa.edu.ar', 'Parcial', 'Ayudante', 1),
(14, 98661302, 'Fernanda Daniela', 'Oyarzo', 'Viernes', 'Foyarzo@unpa.edu.ar', 'Simple', 'Adjunto', 1),
(15, 84817596, 'Carlos Alberto', 'Talay', 'Martes', 'Ctalay@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(16, 70757406, 'Mauro', 'Zhielke', 'Jueves', 'mzhielke@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(17, 17325880, 'Claudia', 'Gonzales', 'Viernes', 'Cgonzales@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(18, 25524238, 'Carlos Daniel', 'Amarilla', 'Jueves', 'camarilla@unpa.edu.ar', 'Simple', 'Ayudante', 1),
(19, 78042027, 'Esteban Guillermo', 'Gesto', 'Viernes', 'egesto@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(20, 23645268, 'Hector Manuel', 'Soto', 'Miercoles', 'hsoto@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(21, 24810100, 'Luis Mauricio', 'Sierpe Oyarzo', 'Jueves', 'Luissierpe@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(22, 36793324, 'Carlos Gustavo', 'Livacic', 'Viernes', 'Clivacic@unpa.edu.ar', 'Simple', 'Adjunto', 1),
(23, 34569183, 'Hector Hipolito', 'Reinaga', 'Miercoles', 'hreinaga@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(24, 39204542, 'Daniel Andres', 'Gonzales', 'Martes', 'danielgonzales@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(25, 66179834, 'Juan Gabriel', 'Enriquez', 'Lunes', 'jenriquez@unpa.edu.ar', 'Exclusiva', 'Ayudante', 1),
(26, 34634676, 'Diego Raul', 'Rodriguez Herling', 'Viernes', 'Rherling@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(27, 34504123, 'Paula Alejandra', 'Millado', 'Jueves', 'pmillado@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(28, 30446520, 'Albert Anibal Osiris', 'Sofia', 'Jueves', 'SofiaOsiris@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(29, 16111747, 'Jorge', 'Climis', 'Jueves', 'jclimis@unpa.edu.ar', 'Exclusiva', 'Ayudante', 1),
(30, 31978715, 'Jose Luis', 'Saenz', 'Jueves', 'Jsaenz@unpa.edu.ar', 'Simple', 'Asociado', 1),
(31, 28614026, 'Eder', 'Reboucas Dos Santos', 'Miercoles', 'EderDosSantos@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(32, 20450453, 'Walter Raul', 'Altamirano', 'Miercoles', 'waltamirano@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(33, 22509721, 'Federico jorge', 'Laje', 'Martes', 'flaje@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(34, 25004450, 'Pedro ignacio', 'Cornejo', 'Lunes', 'pcornejo@unpa.edu.ar', 'Parcial', 'Adjunto', 2),
(35, 25004451, 'Pedro', 'Dodman', 'Miercoles', 'pdodman@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(36, 29088186, 'Maria pilar', 'Melano', 'Viernes', 'mmelano@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(37, 22709221, 'Emilio vicente', 'Restivo', 'Jueves', 'erestivo@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(38, 28156167, 'Loreli', 'Stettler', 'Lunes', 'lstettler@unpa.edu.ar', 'Parcial', 'Adjunto', 2),
(39, 17734099, 'Patricia del carmen', 'Zapata', 'Jueves', 'pzapata@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(40, 27098934, 'Patricia beatriz', 'Vega', 'Martes', 'pvega@unpa.edu.ar', 'Parcial', 'Adjunto', 2),
(41, 23616141, 'Alejandra lorena', 'Costantini', 'Martes', 'acostantini@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(42, 20407136, 'Ana claudia', 'Tabares', 'Miercoles', 'atabares@unpa.edu.ar', 'Parcial', 'Asociado', 2),
(43, 24861220, 'Susana mabel', 'Bahamonde', 'Jueves', 'sbahamonde@unpa.edu.ar', 'Parcial', 'Asociado', 2),
(44, 16419128, 'Monica beatriz', 'Musci', 'Jueves', 'mmusci@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(45, 17801725, 'Marcela monica', 'Arpes', 'Viernes', 'marpes@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(46, 30968350, 'Nicolas', 'Albrieu', 'Miercoles', 'nalbrieu@unpa.edu.ar', 'Simple', 'Asociado', 2),
(47, 30968351, 'Claudia', 'Figueroa ', 'Miercoles', 'cfigueroa @unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(48, 30968352, 'Fernando', 'Montero', 'Jueves', 'fmontero@unpa.edu.ar', 'Simple', 'Asociado', 2),
(49, 23626041, 'Nestor horacio', 'Borquez', 'Miercoles', 'nborquez@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(50, 31440169, 'Hernan', 'Gimenez', 'Lunes', 'hgimenez@unpa.edu.ar', 'Parcial', 'Adjunto', 2),
(51, 20237624, 'Miriam liliana', 'Vazquez', 'Martes', 'mvazquez@unpa.edu.ar', 'Simple', 'Asociado', 2),
(52, 24861524, 'Sebastian ariel', 'Ruiz', 'Lunes', 'sruiz@unpa.edu.ar', 'Parcial', 'Asociado', 2),
(53, 24225561, 'MARIA ANGELICA', 'ZUÑIGA', 'Lunes', 'MZUÑIGA@UNPA.EDU.AR', 'Exclusiva', 'Titular', 2),
(54, 32122793, 'IVAN', 'PEZZINI ', 'Martes', 'IPEZZINI@UNPA.EDU.AR', 'Parcial', 'Adjunto', 2),
(55, 26351877, 'MARCELA ALEJANDRA', 'CONSTANZO', 'Jueves', 'MCONSTANZO@UNPA.EDU.AR', 'Simple', 'Asociado', 2),
(56, 29058990, 'ROBERTO ADRIAN', 'FARIAS', 'Viernes', 'RFARIAS@UNPA.EDU.AR', 'Parcial', 'Adjunto', 2),
(57, 30502856, 'ROMINA', 'BEHRENS', 'Miercoles', 'RBEHRENS@UNPA.EDU.AR', 'Exclusiva', 'Titular', 2),
(58, 24357679, 'CRISTIAN LEONARDO', 'BESSONE', 'Martes', 'CBESSONE@UNPA.EDU.AR', 'Simple', 'Asociado', 2),
(59, 28156155, 'ANDREA VIVIANA', 'ALVARADO', 'Jueves', 'AALVARADO@UNPA.EDU.AR', 'Parcial', 'Adjunto', 2),
(60, 28156156, 'Adrian', 'AGÜERO', 'Viernes', 'AAGÜERO@UNPA.EDU.AR', 'Simple', 'Asociado', 2),
(61, 26287837, 'VICTORIA LAURA', 'HAMMAR', 'Lunes', 'VHAMMAR@UNPA.EDU.AR', 'Exclusiva', 'Titular', 2),
(62, 26287838, 'Alejandro', 'VICTORIA', 'Martes', 'AVICTORIA@UNPA.EDU.AR', 'Parcial', 'Adjunto', 2),
(63, 28342661, 'RICARDO SANTIAGO', 'PUCA MOLINA', 'Jueves', 'RPUCA@UNPA.EDU.AR', 'Simple', 'Asociado', 2),
(64, 12995059, 'LUIS RAFAEL', 'DALLA COSTA ', 'Viernes', 'LDALLA@UNPA.EDU.AR', 'Parcial', 'Adjunto', 2),
(65, 35567708, 'ANDREA GISELLE', 'FERNANDEZ', 'Miercoles', 'AFERNANDEZ@UNPA.EDU.AR', 'Exclusiva', 'Titular', 2),
(66, 23130400, 'CHRISTIAN ALEJANDRO', 'BRITOS', 'Lunes', 'CBRITOS@UNPA.EDU.AR', 'Simple', 'Asociado', 2);

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `rol` (`id`, `nombre`) VALUES
(7, 'Usuario Comun'),
(8, 'Administrador');

CREATE TABLE `rol_permiso` (
  `id_rol` int(11) NOT NULL,
  `id_permiso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `rol_permiso` (`id_rol`, `id_permiso`) VALUES
(7, 7),
(7, 11),
(8, 7),
(8, 8),
(8, 9),
(8, 11);

CREATE TABLE `tipo_licencia` (
  `nombre` varchar(60) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `idLicencia` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tribunal` (
  `id` int(10) UNSIGNED NOT NULL,
  `presidente` int(10) NOT NULL,
  `vocal` int(10) NOT NULL,
  `vocal1` int(10) DEFAULT NULL,
  `suplente` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tribunal` (`id`, `presidente`, `vocal`, `vocal1`, `suplente`) VALUES
(23, 1, 2, 3, 4),
(24, 5, 6, 7, NULL),
(25, 8, 9, 10, NULL),
(26, 11, 12, 13, 14),
(27, 15, 16, 17, 18),
(28, 5, 19, 19, NULL),
(29, 20, 21, 22, NULL),
(30, 23, 24, 25, NULL),
(31, 22, 15, 26, NULL),
(32, 28, 27, 29, NULL),
(33, 9, 30, 10, NULL),
(34, 28, 29, 6, NULL),
(35, 31, 32, 28, NULL),
(36, 28, 19, 5, NULL),
(37, 33, 34, NULL, NULL),
(38, 35, 36, NULL, NULL),
(39, 37, 38, NULL, NULL),
(40, 39, 40, NULL, NULL),
(41, 41, 42, NULL, NULL),
(42, 36, 41, NULL, NULL),
(43, 44, 43, NULL, NULL),
(44, 44, 43, NULL, NULL),
(45, 36, 43, NULL, NULL),
(46, 45, 46, NULL, NULL),
(47, 47, 48, NULL, NULL),
(48, 49, 50, NULL, NULL),
(49, 44, 36, NULL, NULL),
(50, 53, 54, 55, 56),
(51, 57, 58, 59, 60),
(52, 61, 62, 63, NULL),
(53, 57, 64, 63, NULL),
(54, 65, 63, 66, NULL),
(55, 65, 63, 66, NULL);

CREATE TABLE `tribunalasignatura` (
  `id` int(10) UNSIGNED NOT NULL,
  `idTribunal` int(11) NOT NULL,
  `idAsignatura` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tribunalasignatura` (`id`, `idTribunal`, `idAsignatura`) VALUES
(12, 23, 22),
(13, 24, 35),
(14, 25, 36),
(15, 26, 37),
(16, 27, 38),
(17, 28, 39),
(18, 29, 40),
(19, 30, 41),
(20, 31, 42),
(21, 32, 43),
(22, 33, 13),
(23, 34, 11),
(24, 35, 6),
(25, 36, 12),
(26, 37, 44),
(27, 38, 45),
(28, 39, 46),
(29, 40, 47),
(30, 41, 48),
(31, 42, 49),
(32, 43, 50),
(33, 44, 51),
(34, 45, 52),
(35, 46, 53),
(36, 47, 54),
(37, 48, 55),
(38, 50, 56),
(39, 51, 57),
(40, 52, 58),
(41, 53, 59),
(42, 54, 60),
(43, 55, 61);

CREATE TABLE `turno` (
  `idTurno` int(11) NOT NULL,
  `fechainicio1` date NOT NULL,
  `fechafin1` date NOT NULL,
  `periodollamado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `turno` (`idTurno`, `fechainicio1`, `fechafin1`, `periodollamado`) VALUES
(1, '2021-02-22', '2021-02-26', 1),
(2, '2021-03-01', '2021-03-05', 2);

CREATE TABLE `turno_mesaexamen` (
  `id` int(11) NOT NULL,
  `idTurno` int(11) NOT NULL,
  `idMesa` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `codAula` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `turno_mesaexamen` (`id`, `idTurno`, `idMesa`, `fecha`, `hora`, `codAula`) VALUES
(188, 1, 122, '2021-12-01', '00:00:00', NULL),
(189, 1, 123, '2021-12-01', '00:00:00', NULL),
(190, 1, 124, '2021-12-01', '00:00:00', NULL),
(191, 1, 125, '2021-12-01', '00:00:00', NULL),
(192, 1, 126, '2021-12-01', '00:00:00', NULL),
(193, 1, 127, '2021-12-01', '00:00:00', NULL),
(195, 1, 129, '2021-12-02', '00:00:00', NULL),
(196, 1, 130, '2021-12-02', '00:00:00', NULL),
(197, 1, 131, '2021-12-02', '00:00:00', NULL),
(198, 1, 132, '2021-12-02', '00:00:00', NULL),
(199, 1, 133, '2021-12-02', '00:00:00', NULL),
(200, 1, 134, '2021-12-02', '00:00:00', NULL),
(201, 1, 135, '2021-12-02', '00:00:00', NULL),
(202, 1, 136, '2021-12-02', '00:00:00', NULL),
(203, 1, 137, '2021-12-02', '00:00:00', NULL),
(204, 1, 138, '2021-12-02', '00:00:00', NULL),
(205, 1, 139, '2021-12-02', '00:00:00', NULL),
(210, 1, 144, '2021-12-03', '00:00:00', NULL),
(211, 1, 145, '2021-12-03', '00:00:00', NULL),
(212, 1, 146, '2021-12-03', '00:00:00', NULL),
(213, 1, 147, '2021-12-03', '00:00:00', NULL),
(217, 1, 151, '2021-12-06', '00:00:00', NULL),
(218, 1, 152, '2021-12-07', '00:00:00', NULL),
(219, 1, 153, '2021-12-07', '00:00:00', NULL),
(220, 1, 154, '2021-12-07', '00:00:00', NULL),
(221, 1, 155, '2021-12-07', '00:00:00', NULL),
(225, 2, 151, '2021-12-13', '00:00:00', NULL),
(226, 2, 152, '2021-12-14', '00:00:00', NULL),
(227, 2, 153, '2021-12-14', '00:00:00', NULL),
(228, 2, 154, '2021-12-14', '00:00:00', NULL),
(229, 2, 155, '2021-12-14', '00:00:00', NULL),
(233, 2, 122, '2021-12-15', '00:00:00', NULL),
(234, 2, 123, '2021-12-15', '00:00:00', NULL),
(235, 2, 124, '2021-12-15', '00:00:00', NULL),
(236, 2, 125, '2021-12-15', '00:00:00', NULL),
(237, 2, 126, '2021-12-15', '00:00:00', NULL),
(238, 2, 127, '2021-12-15', '00:00:00', NULL),
(240, 2, 129, '2021-12-16', '00:00:00', NULL),
(241, 2, 130, '2021-12-16', '00:00:00', NULL),
(242, 2, 131, '2021-12-16', '00:00:00', NULL),
(243, 2, 132, '2021-12-16', '00:00:00', NULL),
(244, 2, 133, '2021-12-16', '00:00:00', NULL),
(245, 2, 134, '2021-12-16', '00:00:00', NULL),
(246, 2, 135, '2021-12-16', '00:00:00', NULL),
(247, 2, 136, '2021-12-16', '00:00:00', NULL),
(248, 2, 137, '2021-12-16', '00:00:00', NULL),
(249, 2, 138, '2021-12-16', '00:00:00', NULL),
(250, 2, 139, '2021-12-16', '00:00:00', NULL),
(255, 2, 144, '2021-12-17', '00:00:00', NULL),
(256, 2, 145, '2021-12-17', '00:00:00', NULL),
(257, 2, 146, '2021-12-17', '00:00:00', NULL),
(258, 2, 147, '2021-12-17', '00:00:00', NULL);

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `usuario` (`id`, `nombre`, `email`) VALUES
(24, 'emiliano Aguero', 'emilianoaguero2@gmail.com'),
(23, 'Eder dos Santos', 'esantos@uarg.unpa.edu.ar');

CREATE TABLE `usuario_rol` (
  `id_usuario` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `usuario_rol` (`id_usuario`, `id_rol`) VALUES
(23, 8),
(24, 8);


ALTER TABLE `asignatura`
  ADD PRIMARY KEY (`codAsignatura`),
  ADD KEY `fk_ASIGNATURA_PROFESOR_idx` (`idProfesor`);

ALTER TABLE `aula`
  ADD PRIMARY KEY (`codAula`),
  ADD UNIQUE KEY `idtable1_UNIQUE` (`codAula`);

ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `carrera`
  ADD PRIMARY KEY (`codCarrera`);

ALTER TABLE `correlativad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_CORRELATIVAD_ASIGNATURA1_idx` (`codAsignatura`),
  ADD KEY `fk_CORRELATIVAD_ASIGNATURA2_idx` (`codAsignatura_Correlatividad`);

ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`);

ALTER TABLE `fecha`
  ADD KEY `fk_FECHA_LLAMADO_idx` (`idLlamado`);

ALTER TABLE `licencia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_LICENCIA_PROFESOR_idx` (`idProfesor`);

ALTER TABLE `llamado_mesa_examen`
  ADD KEY `fk_LLAMADO_has_MESA_EXAMEN_MESA_EXAMEN1_idx` (`codMesa`),
  ADD KEY `fk_LLAMADO_has_MESA_EXAMEN_LLAMADO1_idx` (`idLlamado`);

ALTER TABLE `mesa_examen`
  ADD PRIMARY KEY (`codMesa`),
  ADD KEY `fk_MESA_EXAMEN_TRIBUNAL_idx` (`idTribunal`),
  ADD KEY `fk_MESA_EXAMEN_ASIGNATURA_idx` (`codAsignatura`);

ALTER TABLE `mesa_examen_carrera`
  ADD KEY `fk_MESA_EXAMEN_has_CARRERA_CARRERA1_idx` (`codCarrera`),
  ADD KEY `fk_MESA_EXAMEN_has_CARRERA_MESA_EXAMEN1_idx` (`codMesa`);

ALTER TABLE `permiso`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ID_PERMISO_IND` (`id`);

ALTER TABLE `profesor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni_UNIQUE` (`dni`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_PROFESOR_DEPARTAMENTO_idx` (`idDepartamento`);

ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ID_ROL_IND` (`id`);

ALTER TABLE `rol_permiso`
  ADD PRIMARY KEY (`id_rol`,`id_permiso`),
  ADD UNIQUE KEY `ID_ROL_PERMISO_IND` (`id_permiso`,`id_rol`),
  ADD KEY `FKASO_ROL_IND` (`id_rol`),
  ADD KEY `FKASO_PER_idx` (`id_permiso`);

ALTER TABLE `tribunal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_TRIBUNAL_PROFESOR1_idx` (`presidente`),
  ADD KEY `fk_TRIBUNAL_PROFESOR2_idx` (`vocal`),
  ADD KEY `fk_TRIBUNAL_PROFESOR3_idx` (`vocal1`),
  ADD KEY `fk_TRIBUNAL_PROFESOR4_idx` (`suplente`);

ALTER TABLE `tribunalasignatura`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `turno`
  ADD PRIMARY KEY (`idTurno`);

ALTER TABLE `turno_mesaexamen`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UN_USUARIO` (`email`,`nombre`),
  ADD UNIQUE KEY `ID_USUARIO_IND` (`id`);

ALTER TABLE `usuario_rol`
  ADD PRIMARY KEY (`id_usuario`,`id_rol`),
  ADD UNIQUE KEY `ID_USUARIO_ROL_IND` (`id_rol`,`id_usuario`),
  ADD KEY `FKVIN_USU_IND` (`id_usuario`),
  ADD KEY `FKVIN_ROL_idx` (`id_rol`);


ALTER TABLE `asignatura`
  MODIFY `codAsignatura` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

ALTER TABLE `aula`
  MODIFY `codAula` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

ALTER TABLE `correlativad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `licencia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `mesa_examen`
  MODIFY `codMesa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

ALTER TABLE `profesor`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

ALTER TABLE `tribunal`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

ALTER TABLE `tribunalasignatura`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

ALTER TABLE `turno`
  MODIFY `idTurno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `turno_mesaexamen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=262;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
