import {Component, OnInit, ViewChild} from '@angular/core';
import {IonSegment} from '@ionic/angular';
import {AsignaturaMesa, CarreraMesa, Mesa} from '../../interfaces/interfaces';
import {ExamenesService} from '../../services/examenes.service';
import {DataLocalService} from "../../services/data-local.service";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  sliderOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };

  @ViewChild(IonSegment, { static: true }) segment: IonSegment;
  mesas: Mesa[] = [];
  error: Error = null;
  header = 'Notificaciones';
  asignaturas: AsignaturaMesa[] = [];
  carreras: CarreraMesa[] = [];
  carrerass = ['buenisse'];

  constructor( private examenesService: ExamenesService,
               public datalocalService: DataLocalService) {}

  ngOnInit() {
    this.cargarCarreras();
    this.consultarMesasCarrera();
  }

  //Obtengo el listado de las mesas de examen por carrera
  consultarMesasCarrera(){
    this.mesas = [];
    // const body: string = "&idCarrera=" + carrera;
    this.examenesService.consultarNotificacion()
      .subscribe(resp => {
        console.log(resp);

        resp['estado'] != "BAD" ? this.mesas.push( ...resp.mesas ) : '';
        }
      );
  }

  //Obtengo el listado de las carreras de la univerisdad
  cargarCarreras(){
    this.examenesService.cargarCarrreras()
      .subscribe( resp => {
        this.carreras.push( ...resp.carreras );
      });
  }

}
