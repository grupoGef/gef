<?php

include_once 'BDObjetoGenerico.Class.php';
include_once 'Rol.Class.php';

class Cargo extends BDObjetoGenerico {

    protected $email;

    /**
     *
     * @var Rol[]
     */
    private $roles;

    function __construct($id = null) {
        parent::__construct($id, "cargo");
    }


    function getDedicacion() {
        return $this->dedicacion;
    }
    function getCargo() {
        return $this->cargo;
    }
    function getId() {
        return $this->id;
    }
    function getidProfesor() {
        return $this->idProfesor;
    }

    function setId($id) {
        $this->id = $id;
    }
    function setidProfesor($idProfesor) {
        $this->idProfesor = $idProfesor;
    }
    function setDedicacion($Dedicacion) {
        $this->dedicacion = $Dedicacion;
    }
    function setCargo($cargo) {
        $this->cargo = $cargo;
    }
  
    /**
     *
     * @param type $tablaVinculacion
     * @param type $tablaElementos
     * @param type $idObjetoContenedor
     * @param type $atributoFKElementoColeccion
     * @param type $claseElementoColeccion
     *
     */
    function setRoles($tablaVinculacion, $tablaElementos, $idObjetoContenedor, $atributoFKElementoColeccion, $claseElementoColeccion) {
        $this->setColeccionElementos($tablaVinculacion, $tablaElementos, $idObjetoContenedor, $atributoFKElementoColeccion, $claseElementoColeccion);
        $this->roles = $this->getColeccionElementos();
    }

    function getRoles() {
        return $this->roles;
    }

    /**
     *
     * @param int $id
     * @return boolean
     */
    function buscarRolPorId($id) {
        foreach ($this->getRoles() as $RolUsuario) {
            if ($id == $RolUsuario->getId()) {
                return true;
            }
        }
        return false;
    }

}
