import {Component, Input, OnInit} from '@angular/core';
import {Mesa} from '../../interfaces/interfaces';

@Component({
  selector: 'app-mesas',
  templateUrl: './mesas.component.html',
  styleUrls: ['./mesas.component.scss'],
})
export class MesasComponent implements OnInit {

  @Input() mesas: Mesa[] = [];
  @Input() enFavoritos = false;
  @Input() novedad = false;
  constructor() {}

  ngOnInit() {}

}
