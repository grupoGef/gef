<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARMESAEXAMEN);


$DatosFormulario = $_POST;

//var_dump($DatosFormulario);




// $query2 = "INSERT INTO mesa_examen VALUES (null,'general','{$DatosFormulario["idtribunal"]}','{$DatosFormulario["selectAsignatura"]}')";
// $consulta2 = BDConexion::getInstancia()->query($query2);



// $query3="SELECT codMesa FROM mesa_examen where 
// tipo='general' and idTribunal = '{$DatosFormulario["idtribunal"]}' and codAsignatura = '{$DatosFormulario["selectAsignatura"]}'
// "; 
// $consulta3 = BDConexion::getInstancia()->query($query3);
// while($row = $consulta3->fetch_array())
// {
//     $idmesa=$row['codMesa'];
// }

// $query4 = "INSERT INTO turno_mesaexamen VALUES (null,'general','{$DatosFormulario["selectllamado"]}',$idmesa,'{$DatosFormulario["hora"]}',null)";
// $consulta4 = BDConexion::getInstancia()->query($query4);
// echo $query4;








// $query = "INSERT INTO turno_mesaexamen VALUES (null,'{$DatosFormulario["selectllamado"]}','{$DatosFormulario["selectVocal"]}','{$DatosFormulario["selectVocal2"]}','{$DatosFormulario["selectSuplente"]}')";
// $consulta = BDConexion::getInstancia()->query($query);
// $ColeccionTribunal = new ColeccionTribunal();
// foreach ($ColeccionTribunal->getTribunales() as $Tribunal) {
//   if ($DatosFormulario ["selectPresidente"] == $Tribunal->getPresidente() && $DatosFormulario ["selectVocal"] == $Tribunal->getVocal() && $DatosFormulario ["selectVocal2"] == $Tribunal->getVocal1() && $DatosFormulario ["selectSuplente"] == $Tribunal->getSuplente() )
//   {
//     $id=$Tribunal->getId();
//   }
// }


// $query2 = "INSERT INTO mesa_examen VALUES (null,'$id','{$DatosFormulario["selectAsignatura"]}','{$DatosFormulario["orden"]}')";
// $consulta2 = BDConexion::getInstancia()->query($query2);


// $query3 = "INSERT INTO mesa_examen_carrera VALUES ('$idmesa','{$DatosFormulario["selectCarrera"]}')";
// $consulta3 = BDConexion::getInstancia()->query($query3);
// for ($i=2; $i <= 5 ; $i++) {
//   $query4 = "INSERT INTO llamado_mesa_examen VALUES ('$i','$idmesa','{$DatosFormulario["hora"]}',Null)";
//   $consulta4 = BDConexion::getInstancia()->query($query4);
// }


//if (!$consulta && !$consulta2) {
//    BDConexion::getInstancia()->rollback();
    //arrojar una excepcion
//    die(BDConexion::getInstancia()->errno);
//}
?>
<html>
    <head>
    <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2-bootstrap.min.css">
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/select2.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/i18n/es.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/jszip.min.js"></script>
    <script src="../../lib/dataTables/pdfmake.min.js"></script>
    <script src="../../lib/dataTables/vfs_fonts.js"></script>
    <script src="../../lib/dataTables/buttons.flash.min.js"></script>
    <script src="../../lib/dataTables/buttons.html5.min.js"></script>
    <script src="../../lib/dataTables/buttons.print.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Crear Examen</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <p></p>
            <div class="card">
                <div class="card-header">
                    <h3>Crear Examen</h3>
                </div>
                <div class="card-body">
                   
                        <div class="alert alert-success" role="alert">
                            Operaci&oacute;n realizada con &eacute;xito.
                        </div>
                    
                    <hr />
                    <h5 class="card-text">Opciones</h5>
                    <a href="gestionExamen.php">
                        <button type="button" class="btn btn-primary">
                            <span class="oi oi-account-logout"></span> Salir
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
