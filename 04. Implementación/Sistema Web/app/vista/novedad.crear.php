<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARNOVEDADES);
include_once '../modelo/ColeccionDocentes.php';
include_once '../modelo/ColeccionLicencia.php';
include_once '../modelo/ColeccionTipoLicencia.php';
$ColeccionDocente = new ColeccionDocentes();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
        <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
        <link rel="stylesheet" href="../../lib/select2/dist/css/select2.min.css" />
        <link rel="stylesheet" href="../../lib/select2/dist/css/select2-bootstrap.min.css">
        <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../../lib/select2/dist/js/select2.js"></script>
        <script type="text/javascript" src="../../lib/select2/dist/js/i18n/es.js"></script>
        <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Crear Licencia</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="novedad.crear.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                    <p><center><h3>Agregar nueva Licencia</h3></center></p>
                        <p>
                            Por favor, complete los campos a continuaci&oacute;n.
                            Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                            Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                        </p>
                    </div>
                    <div class="card-body">
                    <h4>Datos de la Licencia</h4>
                        <br>

                      
                        <label for="inputNombre">Nombre del Docente</label>
                        <div class="form-group">
                        <select class="js-example-basic-single form-control" name="docente">
                        </select>
                        </div>
                        <div class="form-group">
                            <label for="inputFechaInicio">Dia de Inicio</label>
                            <input type="date" name="fechainicio" class="form-control" id="inputFechaInicio"  placeholder="Ingrese la fecha de inicio" oninput="validar('inputFechaInicio')" required pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}"title="Ingrese formato de fecha dd/mm/yyyy" >
                        </div>
                        <div class="invalid-feedback">
                          Ingresar Dia de Inicio
                        </div>
                        <div class="form-group">
                            <label for="inputFechaFinal">Dia de Finalización</label>
                            <input type="date" name="fechafinal" class="form-control" id="inputFechaFinal"  placeholder="Ingrese la fecha de finalizacion" oninput="validate()" required pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" title="Ingrese formato de fecha dd/mm/yyyy">
                        </div>
                        <div class="invalid-feedback">
                          Ingresar Dia de Fin
                        </div>
                        <div class="form-group">
                            <label for="inputNombre">Tipo de Licencia</label>
                            <select class="tipolicencia form-control " name="tipolicencia">
                            <option value="Medica y Accidentes">Médica y Accidentes</option>
                            <option value="Personales">Personales</option>
                            <option value="Extraordinarias">Extraordinarias</option>
                            <option value="Especiales">Especiales</option>
                            <option value="Ejercicio transitorio de Mayor Jerarquia">Ejercicio transitorio de Mayor Jerarquía</option>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                          Tipo de licencia
                        </div>
                        <div class="form-group">
                            <label for="inputDescripcion">Descripcion</label>
                            <input type="text" name="descripcion" class="form-control" id="inputDescripcion" placeholder="Ingrese Descripcion de la Novedad" oninput="validar('inputDescripcion')" required="" pattern="[A-Za-z]{4-45}">
                        </div>
                        <div class="invalid-feedback">
                          ingresar Descripcion
                        </div>


                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <span class="oi oi-check"></span> Confirmar
                        </button>
                        <a href="Novedades.php">
                            <button type="button" class="btn btn-outline-danger">
                                <span class="oi oi-x"></span> Cancelar
                            </button>
                        </a>
                    </div>
                </div>
                
            </form>
</div>
          
           
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
<script>
    $(document).ready(function(){


        $.fn.select2.defaults.set( "theme", "bootstrap" );
        $.fn.select2.defaults.set('language', 'es');
        $('.tipolicencia').select2({
            placeholder: 'Ingrese el Profesor',
              language: {
noResults: function() {

  return "No hay resultados";        
},
searching: function() {

  return "Buscando..";
}
}

        });
        $('.js-example-basic-single').select2({
              placeholder: 'Ingrese el Profesor',
              language: {
noResults: function() {

  return "No hay resultados";        
},
searching: function() {

  return "Buscando..";
}
},
              ajax: {
                     url: 'buscarnombreprofesor.php',
                     dataType: 'json',
                     delay: 250,
                     data: function (data) {
                    return { 
                        query: data.term // search term
                    };
                },
                    processResults: function (data) {
                        
                        return {
                              results: data
                               };
                    },
                      cache: true
                    }
        });






  
        });
</script>
<script>  
function validate() {
    if(document.getElementById('inputFechaFinal').value<document.getElementById('inputFechaInicio').value) 
        document.getElementById('inputFechaFinal').setCustomValidity('Esta fecha debe ser mayor a la fecha inicial');
    else 
        document.getElementById('inputFechaFinal').setCustomValidity('');
}
</script>