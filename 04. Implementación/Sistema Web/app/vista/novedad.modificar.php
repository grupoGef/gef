<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARNOVEDADES);

$idlicencia=$_GET["id"];
$idtipolicencia=$_GET["id1"];
$iddocente=$_GET["id2"];

$query="Select p.id,p.nombre,p.apellido,d.nombre as departamento,d.id as iddepartamento,l.id as idlicencia,l.fechaInicio,l.fechaFinal, tl.idLicencia as tipolicenciaid,tl.nombre as tipolicencianombre, tl.descripcion FROM licencia l left join profesor p on l.idProfesor=p.id left JOIN departamento d on p.idDepartamento=d.id left join tipo_licencia tl ON l.id=tl.idLicencia WHERE 
p.id=$iddocente and l.id=$idlicencia and tl.idLicencia  = $idtipolicencia";


$reportebuscar= BDConexion::getInstancia()->query($query);



if ($reportebuscar->num_rows != 0)
{
   
    while($row = $reportebuscar->fetch_array())
    {
        $docente=$row['nombre'];
        $departamento=$row['departamento'];
        $nombrelicencia=$row['tipolicencianombre'];
        $fecha1 = new DateTime($datetimecomparable= $row['fechaInicio']);
        $fecha_d_m_y1 = $fecha1->format('Y-m-d');
        $fecha2 = new DateTime($datetimecomparable= $row['fechaFinal']);
        $fecha_d_m_y2 = $fecha2->format('Y-m-d'); 
        $licenciadesc=$row['descripcion'];
        


    }

}


?>
<html>
    <head>
      <meta charset="UTF-8">
      <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Actualizar Novedad</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="novedad.modificar.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                    <p><center><h3>Modificar Licencia</h3></center></p>
                       
                        <p>
                            Complete los campos que desea modificar.
                            Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                            Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                        </p>
                    </div>
                    <div class="card-body">
                       <h4 class="card-text">Nombre de Docente</h4>
                       <p> <?=$docente ?></p>
                       <hr />
                        <div class="form-group">
                            <label for="inputFechaInicio">Fecha Inicio</label>
                            <input type="date" name="fechainicio" class="form-control" id="inputFechaInicio" oninput="validar('inputFechaInicio')" value="<?=$fecha_d_m_y1?>" placeholder="Ingrese la fecha inicial de la licencia" required="" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}"title="Ingrese formato de fecha dd/mm/yyyy">

                        </div>
                        <div class="form-group">
                            <label for="inputFechaFinal">Fecha Final</label>
                            <input type="date" name="fechafinal" class="form-control" id="inputFechaFinal" oninput="validate()" value="<?=$fecha_d_m_y2?>" placeholder="Ingrese la fecha limite de licencia" required="" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" title="Ingrese formato de fecha dd/mm/yyyy">

                        </div>
                        <div class="form-group">
                            <label for="inputNombre">Tipo de Licencia</label>
                            <label for="inputNombre">Tipo de Licencia</label>
                            <select class="tipolicencia form-control " name="tipolicencia">
                            <option value="Medica y Accidentes">Medica y Accidentes</option>
                            <option value="Personales">Personales</option>
                            <option value="Extraordinarias">Extraordinarias</option>
                            <option value="Especiales">Especiales</option>
                            <option value="Ejercicio transitorio de Mayor Jerarquia">Ejercicio transitorio de Mayor Jerarquia</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputDescripcion">Descripcion</label>
                            <input type="text" name="descripcion" class="form-control" id="inputDescripcion" oninput="validar('inputDescripcion')" value="<?=$licenciadesc ?>" placeholder="Ingrese descripcion de la licencia" required=""pattern"[A-Za-z]{4-45}" >
                        </div>
                    <input type="hidden" name="id" class="form-control" id="id" value="<?= $idlicencia ?>" >
                    <input type="hidden" name="id1" class="form-control" id="id1" value="<?= $idtipolicencia ?>" >
                    <input type="hidden" name="id2" class="form-control" id="id2" value="<?= $iddocente ?>" >
                      </div>
                      <div class="class-footer">
                          <button type="submit" class="btn btn-outline-success">
                              <span class="oi oi-check"></span>
                              Confirmar
                          </button>
                          <a href="Novedades.php">
                              <button type="button" class="btn btn-outline-danger">
                                  <span class="oi oi-x"></span>
                                  Cancelar
                              </button>
                          </a>
                      </div>
                </div>
            </form>
        </div>
          <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
<script>  
function validate() {
    if(document.getElementById('inputFechaFinal').value<document.getElementById('inputFechaInicio').value) 
        document.getElementById('inputFechaFinal').setCustomValidity('Esta fecha debe ser mayor a la fecha inicial');
        //document.getElementById('inputFechaFinal').style.borderColor="red";
    else 
        document.getElementById('inputFechaFinal').setCustomValidity('');
        //document.getElementById('inputFechaFinal').style.borderColor="green";
}
</script>