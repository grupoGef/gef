<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARAFECTACION);
include_once '../modelo/Tribunal.Class.php';
include_once '../modelo/TribunalAsignatura.Class.php';
include_once '../modelo/Asignatura.Class.php';

$id = $_GET["id"];
$id1 = $_GET["id1"];
$id2 = $_GET["id2"];



$sql='SELECT a.nombre,a.codAsignatura,t.id as tribunalid,ta.id as tribunalasignaturaid,p.id as idpresidente, CONCAT(p.nombre, " ",p.apellido) as presidente,p1.id as idvocal,CONCAT(p1.nombre, " ",p1.apellido)as vocal ,p2.id as idvocal1, CONCAT(p2.nombre, " ",p2.apellido)as vocal1,p3.id as idsuplente,CONCAT(p3.nombre, " ",p3.apellido)as suplente from tribunal t left join tribunalasignatura ta ON t.id=ta.idTribunal left join asignatura a ON a.codAsignatura= ta.idAsignatura left join profesor p on t.presidente = p.id left join profesor p1 on t.vocal = p1.id left join profesor p2 on t.vocal1 = p2.id left join profesor p3 on t.suplente = p3.id
Where a.codAsignatura='.$id2.' and t.id='.$id.' and ta.id='.$id1;

$consulta2 = BDConexion::getInstancia()->query($sql);

if ($consulta2->num_rows != 0)
{
    $filas='';
    while($row = $consulta2->fetch_array())
    {
        $nombreasignatura=$row['nombre'];

    }

}


?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Eliminar Afectacion</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="afectacion.eliminar.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                        
                    <p><center><h3>Eliminar Afectacion</h3></center></p>
                    </div>
                    <div class="card-body">
                        <p class="alert alert-warning ">
                            <span class="oi oi-warning"></span> ATENCI&Oacute;N. Esta operaci&oacute;n no puede deshacerse.
                        </p>
                        <p>¿Est&aacute; seguro que desea eliminar el tribunal de la asignatura  <b><?= $nombreasignatura ?></b>?

                    </div>
                    <input type="hidden" name="id" class="form-control" id="id" value="<?= $id1 ?>">
                    <input type="hidden" name="id1" class="form-control" id="id1" value="<?= $id ?>">
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <span class="oi oi-check"></span> Sí, deseo eliminar
                        </button>
                        <a href="gestionarAfectacion.php">
                            <button type="button" class="btn btn-outline-danger">
                                <span class="oi oi-x"></span> NO (Salir de esta pantalla)
                            </button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
