import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage-angular';
import {Mesa} from '../interfaces/interfaces';
import {ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  mesas: Mesa[] = [];
  private _storage: Storage | null = null;

  constructor( private storage: Storage,
               public toastCtrl: ToastController) {
    this.init();
    this.cargarFavoritos();
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  async init(){
    const storage =  await this.storage.create();
    this._storage = storage;
  }

  guardarMesa( mesa: Mesa){
    const existe = this.mesas.find(mes => mes.idMesaExamen === mesa.idMesaExamen);
    if(!existe){
      this.mesas.unshift(mesa);
      this.storage.set( 'favoritos', this.mesas );
      this.presentToast('Guardado en favoritos');
    }
  }

  async cargarFavoritos(){
    const favoritos = await this.storage.get('favoritos');
      if(favoritos){
        this.mesas = favoritos;
      }
  }

  async actualizarFavoritos(mesa: Mesa[]){
    // console.log(mesa);
    // if(mesa) {
    //   this.mesas = this.mesas.filter(mes => mes.idMesaExamen !== mesa[0].idMesaExamen);
    //   console.log(this.mesas);
    // }
    // this.storage.set( 'favoritos', mesa);
  }

  borrarMesa(mesa: Mesa ){
    this.mesas = this.mesas.filter(mes => mes.idMesaExamen !== mesa.idMesaExamen);
    this.storage.set( 'favoritos', this.mesas );
    this.presentToast('Borrado de favoritos');
  }

}


