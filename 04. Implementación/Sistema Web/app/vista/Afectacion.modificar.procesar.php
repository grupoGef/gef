<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARAFECTACION);
include_once '../modelo/BDConexion.Class.php';
include_once '../modelo/ColeccionLicencia.php';
include_once '../modelo/ColeccionDocentes.php';
include_once '../modelo/ColeccionAsignaturas.php';
include_once '../modelo/ColeccionTribunal.php';
$DatosFormulario = $_POST;
$DatosFormulario = $_POST;
$idTribunal = $DatosFormulario["idt"];
$presidente=$DatosFormulario["presidente"];
$vocal=$DatosFormulario["vocal"];
$vocal1=$DatosFormulario["vocal1"];
$preferenciasprecidente = $DatosFormulario["presidentepreferencias"];
$preferenciasvocal = $DatosFormulario["vocalpreferencias"];
$preferenciasvocal1 = $DatosFormulario["vocal1preferencias"];
$preferenciassuplente = $DatosFormulario["suplentepreferencias"];




if (!empty($DatosFormulario["suplente"]))
{
 
$suplente=$DatosFormulario["suplente"]; 






$queryupdate ="UPDATE tribunal SET preferenciapresidente = '".$preferenciasprecidente."' WHERE id = ".$idTribunal."";
$consulta1 = BDConexion::getInstancia()->query($queryupdate);
$queryupdate ="UPDATE tribunal SET preferenciavocal = '".$preferenciasvocal."' WHERE id = ".$idTribunal."";
$consulta1 = BDConexion::getInstancia()->query($queryupdate);
$queryupdate ="UPDATE tribunal SET preferenciavocal1 = '".$preferenciasvocal1."' WHERE id = ".$idTribunal."";
$consulta1 = BDConexion::getInstancia()->query($queryupdate);
$queryupdate ="UPDATE tribunal SET preferenciasuplente = '".$preferenciassuplente."' WHERE id = ".$idTribunal."";
$consulta1 = BDConexion::getInstancia()->query($queryupdate);



$query = "UPDATE TRIBUNAL 
SET presidente=".$presidente.",
vocal=".$vocal.",
vocal1=".$vocal1.",
suplente=".$suplente."
WHERE id=".$idTribunal."";
$consulta = BDConexion::getInstancia()->query($query);
$ColeccionTribunal = new ColeccionTribunal();

 
}
else
{
  
  

    $queryupdate ="UPDATE tribunal SET preferenciapresidente = '".$preferenciasprecidente."' WHERE id = ".$idTribunal."";
    $consulta1 = BDConexion::getInstancia()->query($queryupdate);
    $queryupdate ="UPDATE tribunal SET preferenciavocal = '".$preferenciasvocal."' WHERE id = ".$idTribunal."";
    $consulta1 = BDConexion::getInstancia()->query($queryupdate);
    $queryupdate ="UPDATE tribunal SET preferenciavocal1 = '".$preferenciasvocal1."' WHERE id = ".$idTribunal."";
    $consulta1 = BDConexion::getInstancia()->query($queryupdate);

  
  
   
  $query = "UPDATE TRIBUNAL 
  SET presidente=".$presidente.",
  vocal=".$vocal.",
  vocal1=".$vocal1."
  WHERE id=".$idTribunal."";
  $consulta = BDConexion::getInstancia()->query($query);
  $ColeccionTribunal = new ColeccionTribunal();
      
    

}



if (!$consulta )
{
    BDConexion::getInstancia()->rollback();
    //arrojar una excepcion
    die(BDConexion::getInstancia()->errno);
}






?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2-bootstrap.min.css">
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/select2.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/i18n/es.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Actualizar Afectacion</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>

        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h3>Actualizar Afectacion</h3>
                </div>
                <div class="card-body">
                    <?php if ($consulta ) { ?>
                        <div class="alert alert-success" role="alert">
                            Operaci&oacute;n realizada con &eacute;xito.
                        </div>
                    <?php } ?>
                    <?php if (!$consulta) { ?>
                        <div class="alert alert-danger" role="alert">
                            Ha ocurrido un error.
                        </div>
                    <?php } ?>
                    <hr />
                    <h5 class="card-text">Opciones</h5>
                    <a href="gestionarAfectacion.php">
                        <button type="button" class="btn btn-primary">
                            <span class="oi oi-account-logout"></span> Salir
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
