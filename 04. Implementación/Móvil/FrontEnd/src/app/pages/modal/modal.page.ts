import { Component, OnInit } from '@angular/core';
import {AsignaturaMesa, CarreraMesa, Mesa} from '../../interfaces/interfaces';
import {ExamenesService} from '../../services/examenes.service';
import {DataLocalService} from '../../services/data-local.service';
import {ModalController} from "@ionic/angular";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  mesas: Mesa[] = [];
  error: Error = null;
  header = 'Notificaciones';
  asignaturas: AsignaturaMesa[] = [];
  carreras: CarreraMesa[] = [];
  carrerass = ['buenisse'];

  constructor(private examenesService: ExamenesService,
              public datalocalService: DataLocalService,
              private modalCtrl: ModalController) { }

  ngOnInit() {
    this.cargarCarreras();
    this.consultarMesasCarrera();
  }

  salir(){
    this.modalCtrl.dismiss();
  }

  //Obtengo el listado de las mesas de examen por carrera
  consultarMesasCarrera(){
    this.mesas = [];
    // const body: string = "&idCarrera=" + carrera;
    this.examenesService.consultarNotificacion()
      .subscribe(resp => {
          resp['estado'] != "BAD" ? this.mesas.push( ...resp.mesas ) : '';
        }
      );
  }

  //Obtengo el listado de las carreras de la univerisdad
  cargarCarreras(){
    this.examenesService.cargarCarrreras()
      .subscribe( resp => {
        this.carreras.push( ...resp.carreras );
      });
  }

}
