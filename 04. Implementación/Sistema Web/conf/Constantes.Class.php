<?php

setlocale(LC_TIME, 'es_AR.utf8');

/**
 *
 * Clase para mantener las directivas de sistema.
 * Deben coincidir con las configuraciones del proyecto.
 *
 * @author Eder dos Santos <esantos@uarg.unpa.edu.ar>
 *
 */
/**
 *  /var/www/html/uargflow//
 */
class Constantes {


    const NOMBRE_SISTEMA = "Gestor de Examenes Finales";

    const WEBROOT = "C:/xampp/htdocs/PGEF";
    const APPDIR = "PGEF";

    const SERVER = "http://localhost:80";
    const APPURL = "http://localhost:80/PGEF/app/vista/index.php";
    const HOMEURL = "http://localhost:80/PGEF/app/vista/index.php";
    const HOMEAUTH = "http://localhost:80/PGEF/app/vista/home.php";

    const BD_SCHEMA = "bdusuarios";
    const BD_USERS = "bdusuarios";

}
