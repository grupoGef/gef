export interface RespuestaTopHeadlines {
  status: string;
  totalResults: number;
  mesas: Mesa[];
  mesa: Mesa;
  error: string;
  asignaturas: AsignaturaMesa[];
  carreras: CarreraMesa[];
  articles: Articule[];
}

export interface Articule{
  source: Source;
  author?: string;
  title: string;
  description: string;
  url: string;
  urlToImage: string;
  publishedAt: string;
  content?: string;
}

export interface Source{
  id?: string;
  name: string;
}

export interface Mesa {
  idMesaExamen: string;
  codigoCarrera: string;
  nombreLargoCarrera: string;
  aulaPrimerLlamado: string;
  aulaSegundoLlamado: string;
  favorito: number;
  fechaPrimerLlamado: string;
  fechaSegundoLlamado: string;
  horaPrimerLlamado: string;
  horaSegundoLlamado: string;
  fechaModificadaPrimerLlamado: string;
  horaModificadaPrimerLlamado: string;
  fechaModificadaSegundoLlamado: string;
  horaModificadaSegundoLlamado: string;
  nombreLargoAsignatura: string;
  nombrePresidente: string;
  nombreSuplente: string;
  nombreVocalPrimero: string;
  nombreVocalSegundo: string;
}

export interface AsignaturaMesa{
  codigo: number;
  nombreCorto: string;
  nombreLargo: string;
}

export interface CarreraMesa{
  codigo: number;
  nombreCorto: string;
  nombreLargo: string;
}
