import {Component, Input, OnInit} from '@angular/core';
import {Mesa} from '../../interfaces/interfaces';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.component.html',
  styleUrls: ['./notificaciones.component.scss'],
})
export class NotificacionesComponent implements OnInit {

  @Input() mesas: Mesa[] = [];
  @Input() enFavoritos = false;
  @Input() novedad = false;
  constructor() { }

  ngOnInit() {
  }

}
