<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARMESAEXAMEN);

if(isset($_GET["id"]))
{
    $tribunalmesaid=$_GET["id"];
}
if(isset($_GET["id1"]))
{
    $mesaid=$_GET["id1"];
}
if(isset($_GET["id2"]))
{
    $codasignaturaid=$_GET["id2"];
}
if(isset($_GET["id3"]))
{
    $turnoid=$_GET["id3"];
}

date_default_timezone_set('America/Argentina/Buenos_Aires');
$sql1='Select fechainicio1, fechafin1 from turno where periodollamado='.$turnoid.'';

$consulta1 = BDConexion::getInstancia()->query($sql1);

if ($consulta1->num_rows != 0)
{
    $filas='';
    while($row = $consulta1->fetch_array())
    {
        $fecha1=new DateTime($row["fechainicio1"]);
        $fecha_d_m_y1 = $fecha1->format('Y/m/d');
        $fecha2=new DateTime($row["fechafin1"]);
        $fecha_d_m_y2 = $fecha2->format('Y/m/d');
      
    }

}
$fecha2->modify('+1 day');
$period1 = new DatePeriod( new datetime($fecha_d_m_y1)
, new DateInterval('P1D'),
$fecha2
);


$fechasprimerllamado=array();




    $dias = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
    $fechasprimerllamado=array();
    foreach ($period1 as $key => $value) { 
        $value->format('Y-m-d');
        $fecha_d_m_y = $value->format('d-m-Y');
        $dia = $dias[(date('N', strtotime($fecha_d_m_y))) - 1];
      
        if (strcmp($dia,'Sabado')!= 0 && strcmp($dia,'Domingo')!= 0)
        {
          
        array_push($fechasprimerllamado,$fecha_d_m_y);
        }
     }




     

 





                    
$sql='Select a.nombre,c.nombre,c.codCarrera,p.preferencias as presidentepreferencias,p1.preferencias as vocalpreferencias,p2.preferencias as vocal1preferencias,p3.preferencias as suplentepreferencias ,au.nombre as nombreaula,au.sector as sectornombre,a.nombre as nombreasignatura,t.periodollamado,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaInicio as presidenteinicio,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal, l1.fechaInicio as vocalinicio,l2.id as vocal1licencia,l2.fechainicio as vocal1inicio,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente ,l3.fechaInicio as suplenteinico,l3.fechaFinal as suplentefinal from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on mec.codCarrera=c.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
licencia l3 on p3.id=l3.idProfesor left join aula au on tm.codAula=au.codAula
where tm.id='.$tribunalmesaid.' and m.codMesa='.$mesaid.' and a.codAsignatura='.$codasignaturaid;

$consulta = BDConexion::getInstancia()->query($sql);







if ($consulta->num_rows != 0)
{
    $filas='';
    while($row = $consulta->fetch_array())
    {
        $fecha = new DateTime($row["fecha"]);
        $fecha_d_m_y1 = $fecha->format('d-m-Y');
    



?>
<html>
    <head>
      <meta charset="UTF-8">
      <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2-bootstrap.min.css">
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/select2.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/i18n/es.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/jszip.min.js"></script>
    <script src="../../lib/dataTables/pdfmake.min.js"></script>
    <script src="../../lib/dataTables/vfs_fonts.js"></script>
    <script src="../../lib/dataTables/buttons.flash.min.js"></script>
    <script src="../../lib/dataTables/buttons.html5.min.js"></script>
    <script src="../../lib/dataTables/buttons.print.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/gestionarmesa.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Actualizar Examen</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="examen.modificar.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                        <p><center><h3>Modificar Examen</h3></center></p>
                        <p>
                            Por favor, complete los campos que desea modificar.
                            Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                            Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                        </p>
                    </div>

                    <div class="card-body">
                    <h4 class="card-text">Asignatura</h4>
                    <p> <?=utf8_encode($row['nombreasignatura'])?> </p>
                    <hr />
                        <div class="form-group">
                            <label for="fechamesa">Fecha</label>
                            <!-- <input type="date" name="fecha" class="form-control" id="inputFecha" value="" placeholder="Ingrese el presidente a modificar" required=""> -->
                            <select  class="form-control " name="fechamesa" >
                            <option selected=""value="<?=$fecha_d_m_y1?>"><?=$fecha_d_m_y1?></option>
                            <?php
                    $longitud = count($fechasprimerllamado);
                    for($i=1; $i<=$longitud; $i++)
                             {
                    	    echo ' 
                            <option value="'.$fechasprimerllamado[$i].'">'.$fechasprimerllamado[$i].'</option>
                          
                         ';
                            
                            }
                    ?>
                          </select>  
                        </div>
                        <div class="form-group">
                            <label for="inputFechaFinal">Hora</label>
                            <input type="time" name="hora" class="form-control" id="inputHora" value="<?= $row['Hora'] ?>" placeholder="Ingrese el vocal 1 a modificar"  min="12:00" max="22:00" required="">
                        </div>
                        <div class="form-group">
                            <label for="inputNombre">Aula</label>
                            <select required='' class="js-example-basic-single form-control" name="aula">
                                
                         </select>
                        </div>
                  
                    <input type="hidden" name="idtribunalmesa" class="form-control" id="idtribunalmesa" value="<?= $row['tribunalmesaid']?>" >
                    <input type="hidden" name="idMesa" class="form-control" id="idMesa" value="<?= $row['mesaid'] ?>" >
                    <input type="hidden" name="idasignatura" class="form-control" id="idasignatura" value="<?= $row['codAsignatura'] ?>" >
                    <input type="hidden" name="idturno" class="form-control" id="idturno" value="<?= $turnoid ?>" >
                    <input type="hidden" name="fecha" class="form-control" id="inputFecha" value="<?= $fecha_d_m_y1 ?>" placeholder="Ingrese el presidente a modificar" required="">
                    <input type="hidden" name="horadefault" class="form-control" id="horadefault" value="<?= $row['Hora'] ?>" placeholder="Ingrese el vocal 1 a modificar"  min="12:00" max="22:00" required="">
                      </div>
                      <div class="class-footer">
                          <button type="submit" class="btn btn-outline-success">
                              <span class="oi oi-check"></span>
                              Confirmar
                          </button>
                          <a href="gestionExamen.php">
                              <button type="button" class="btn btn-outline-danger">
                                  <span class="oi oi-x"></span>
                                  Cancelar
                              </button>
                          </a>
                      </div>
                </div>
            </form>
            <?php     }
}     ?>
        </div>
          <?php include_once '../gui/footer.php'; ?>
    </body>
    <script>
  


       $('.js-example-basic-single').select2({
             placeholder: 'Ingrese el Aula',
             language: {
noResults: function() {

 return "No hay resultados";        
},
searching: function() {

 return "Buscando..";
}
},
             ajax: {
                    url: 'buscaraula.php',
                    dataType: 'json',
                    delay: 250,
                    data: function (data) {
                   return { 
                       query: data.term // search term
                   };
               },
                   processResults: function (data) {
                   
                       return {
                             results: data
                              };
                   },
                     cache: true
                   }
       });
      




  


</script>
</html>
