<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARDOCENTE);
include_once '../modelo/ColeccionRoles.php';
include_once '../modelo/ColeccionDepartamento.php';
$ColeccionDepartamento = new ColeccionDepartamento();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
        <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
        <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
         
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Agregar Docente</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="docente.crear.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                    <p><center><h3>Agregar Docente</h3></center></p>
                        <p>
                            Por favor, complete los campos a continuaci&oacute;n.
                            Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                            Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                        </p>
                    </div>
                    <div class="card-body">
                        <h4>Información del Docente</h4>
                        <br>
                        <div class="form-group">
                            <label for="inputNombree">Nombre</label>
                            <input type="text" name="nombre" class="form-control" id="inputNombre" placeholder="Ingrese el nombre del Docente" oninput="validar('inputNombre')" required="" pattern="^[a-zA-Z]{4,}\s?([a-zA-Z]{3,})?\s?([a-zA-Z]{3,})?">
                        </div>
                        <div class="form-group">
                            <label for="inputApellidoo">Apellido</label>
                            <input type="text" name="apellido" class="form-control" id="inputApellido"  placeholder="Ingrese el apellido del Docente" oninput="validar('inputApellido')" required="" pattern="^^[a-zA-Z]{4,}\s?([a-zA-Z]{3,})?\s?([a-zA-Z]{3,})?">
                        </div>
                        <div class="form-group">
                            <label for="inputDni">DNI</label>
                            <input type="text" name="dni" class="form-control" id="inputDni"  placeholder="Ingrese el dni del Docente" oninput="validar('inputDni')" required="" pattern="[0-9]{8}">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">E-mail</label>
                            <input type="email" name="email" class="form-control" id="inputEmail"  placeholder="Ingrese el email del Docente" required="" oninput="validar('inputEmail')" >
                        </div>
                        <div class "form-group">
                            <label for="categoria">Categoría</label>
                          <select required='' class="form-control " name="categoria" >
                            <option value="Titular">Titular</option>
                            <option value="Asociado">Asociado</option>
                            <option value="Adjunto">Adjunto</option>
                            <option value="Adjunto">Asistente</option>
                            <option value="Ayudante">Ayudante</option>
                          </select>
                        </div>
                        <br>
                        <div class "form-group">
                          <label for"Dedicacion">Dedicación</label>
                          <select  required=''  class="form-control" name="dedicacion" >
                            <option value="Exclusiva">Exclusiva</option>
                            <option value="Parcial">Parcial</option>
                            <option value="Simple">Simple</option>
                          </select>
                        </div>
                        <br>
                        <div class="form-group">
                             <label for="selectTipoBusqueda">Seleccione el departamento</label>
                             <select  required='' class="form-control" name="selectDepartamento">
                             <?php foreach ($ColeccionDepartamento->getDepartamentos() as $Departamento) {
                                echo '<option value="'.$Departamento->getId().'">'.$Departamento->getNombre().'</option>';
                            }
                            ?>
                            </select>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <span class="oi oi-check"></span> Confirmar
                        </button>
                        <a href="docente.php">
                            <button type="button" class="btn btn-outline-danger">
                                <span class="oi oi-x"></span> Cancelar
                            </button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
