<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARMESAEXAMEN);
include_once '../modelo/Licencia.Class.php';
include_once '../modelo/TipoLicencia.Class.php';
include_once '../modelo/Docente.Class.php';



if(isset($_GET["id"]))
{
    $tribunalmesaid=$_GET["id"];
}
if(isset($_GET["id1"]))
{
    $mesaid=$_GET["id1"];
}

if(isset($_GET["id2"]))
{
    $codasignaturaid=$_GET["id2"];
}



?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Eliminar Mesa de Examen</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="examen.eliminar.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                    <?php 
                      $sql='Select a.nombre,c.nombre,c.codCarrera,p.preferencias as presidentepreferencias,p1.preferencias as vocalpreferencias,p2.preferencias as vocal1preferencias,p3.preferencias as suplentepreferencias ,au.nombre as nombreaula,au.sector as sectornombre,a.nombre as nombreasignatura,t.periodollamado,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaInicio as presidenteinicio,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal, l1.fechaInicio as vocalinicio,l2.id as vocal1licencia,l2.fechainicio as vocal1inicio,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente ,l3.fechaInicio as suplenteinico,l3.fechaFinal as suplentefinal from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on mec.codCarrera=c.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
                      licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
                      licencia l3 on p3.id=l3.idProfesor left join aula au on tm.codAula=au.codAula
                      where tm.id='.$tribunalmesaid.' and m.codMesa='.$mesaid.' and a.codAsignatura='.$codasignaturaid;
                
                      $consulta2 = BDConexion::getInstancia()->query($sql);                       
                      
                      
                      while($row = $consulta2->fetch_array())
                      {
                    
                    
                    ?>
                    <p><center><h3>Eliminar Mesa de Examen</h3></center></p>
                       
                    </div>
                    <div class="card-body">
                        <p class="alert alert-warning ">
                            <span class="oi oi-warning"></span> ATENCI&Oacute;N. Esta operaci&oacute;n no puede deshacerse.
                        </p>
                        <p>¿Est&aacute; seguro que desea eliminar la mesa de  <b><?= $row['nombreasignatura'] ?></b> ?

                    </div>
                    <input type="hidden" name="id" class="form-control" id="id" value="<?= $tribunalmesaid ?>">
                    <input type="hidden" name="id1" class="form-control" id="id1" value="<?= $mesaid ?>">
                    <input type="hidden" name="id2" class="form-control" id="id2" value="<?= $codasignaturaid ?>">
                    <input type="hidden" name="id3" class="form-control" id="id3" value="<?= $row['idTurno']?>">
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <span class="oi oi-check"></span> Sí, deseo eliminar
                        </button>
                        <a href="gestionExamen.php">
                            <button type="button" class="btn btn-outline-danger">
                                <span class="oi oi-x"></span> NO (Salir de esta pantalla)
                            </button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
        <?php
        } include_once '../gui/footer.php'; ?>
    </body>
</html>
