import { Component, OnInit } from '@angular/core';
import { ExamenesService } from '../../services/examenes.service';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {Articule, AsignaturaMesa, CarreraMesa, Mesa} from '../../interfaces/interfaces';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

   form: FormGroup;
   mesas: Mesa[] = [];
   header = 'Exámenes Finales';
   asignaturas: AsignaturaMesa[] = [];
   carreras: CarreraMesa[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private examenesService: ExamenesService) {
    this.form = this.formBuilder.group({
      carrera: ['', Validators.required],
      asignatura: ['', Validators.required],
      docente: ['', Validators.required]
    });
  }

  cargarCarreras(){
    this.examenesService.cargarCarrreras()
      .subscribe( resp => {
        console.log(resp);
        this.carreras.push( ...resp.carreras );
      });
  }

  consultarMesas(){
    this.mesas = [];
    const carrera: number = this.form.controls["carrera"].value;
    const asignatura: number = this.form.controls["asignatura"].value;
    const docente: number = this.form.controls["docente"].value;
    const body: string = "&idCarrera=" + carrera + "&idAsignatura=" + asignatura + "&docente=" + docente;
    this.examenesService.consultarMesas(body)
      .subscribe( resp => {
        console.log(resp);
          this.mesas.push( ...resp.mesas );
          console.log(this.header);
        },
      );
  }

  cargarAsignaturas(){
    this.asignaturas = [];
    const carrera: number = this.form.controls["carrera"].value;
    this.examenesService.cargarAsignaturas(carrera)
      .subscribe( resp => {
        this.asignaturas.push( ...resp.asignaturas );
      });
  }

  ngOnInit() {
   this.cargarCarreras();
  }
}
