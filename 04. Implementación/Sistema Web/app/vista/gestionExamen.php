<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARMESAEXAMEN);
// include_once '../modelo/ColeccionCarrera.php';
// $ColeccionCarrera = new ColeccionCarrera();






?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../lib/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2-bootstrap.min.css">
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/select2.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/i18n/es.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/jszip.min.js"></script>
    <script src="../../lib/dataTables/pdfmake.min.js"></script>
    <script src="../../lib/dataTables/vfs_fonts.js"></script>
    <script src="../../lib/dataTables/buttons.flash.min.js"></script>
    <script src="../../lib/dataTables/buttons.html5.min.js"></script>
    <script src="../../lib/dataTables/buttons.print.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/gestionarmesa.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Gestionar de Mesa de Examen</title>
    </head>
    <body>

        <?php include_once '../gui/navbar.php'; ?>
<div class="container-fluid ">
    <div class="container-fluid">
        <div class="row justify-content-around">
          <div class=" col-sm-3">
                <div class="card">
                    <div class="card-header alert-info">
                        B&uacute;squeda Avanzada
                    </div>
                    <div class="card-body">
                    <form  name="buscarmesa" id="buscarmesa" method="post">
                        <div class="form-group">
                            <div class="form-row">
                                <small>Filtrar resultados por:</small>
                                <br><br>
                            </div>
                            <div class="form-row">
                                <label for="selectTipoBusqueda">Carrera</label>
                                <select  class="carrera form-control" name="carrera">
                        </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <label for="inputExpediente">Asignatura</label>
                            </div>
                            <div class="form-row">
                            <select  class="asignatura form-control" name="asignatura">
                        </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <label for="inputDesdeIL">Aula</label>
                            </div>
                            <div class="form-row">
                            <select  class="js-example-basic-single form-control" name="docente">
                            </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-lg" name="Buscar">Realizar B&uacute;squeda</button>
                        </div>
                    </form>
                    <p>
                        <a href="examen.crear.php">
                        <button type="button" class="btn btn-success btn-block btn-lg">
                            <span class="oi oi-plus"></span> Nueva Mesa de Examen
                        </button>
                        </a>
                    </p>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                   <div class="card-header alert-success">
                   <span class="card-title">Listado de Exámenes</span>
                    </div>

                    <div class="card-body">
                    <div class="table-responsive" id= "resultado">
                    <table id="gestionmesaexamen" class ="table table-hover table-condensed table-bordered">
                            <thead>
                            <tr scope="row">
                           
                                <td>Carrera</td>
                                <td>Asignatura</td>
                                <td>Presidente</td>
                                <td>Vocal 1</td>
                                <td>Vocal 2</td>
                                <td>Suplente</td>
                                <td>Fecha</td>
                                <td>Hora</td>
                                <td>Ver más...</td>
                                <td>Modificar</td>
                                <td>Eliminar</td>
                            </tr>
                            </thread>
                            <tbody>    
                            <?php 
                    
                    
                    $sql='Select a.nombre,c.nombre,c.codCarrera,a.nombre as nombreasignatura,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal,l2.id as vocal1licencia,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on a.idCarrera=c.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
                    licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
                    licencia l3 on p3.id=l3.idProfesor
                    where tm.id is not null
                    order by c.nombre desc';
                    $consulta = BDConexion::getInstancia()->query($sql);

                    if ($consulta->num_rows != 0)
                        {
                        $filas='';
                        while($row = $consulta->fetch_array())
                            {

                               
                                $filas.='
                                <tr>
                              
                                <td>'.$row['nombre'].'</td>
                                <td>'.$row['nombreasignatura'].'</td>
                                ';
                                $Colorpresidente='';
                                $Colorvocal='';
                                $Colorvocal1='';
                                $Colorsuplente='';
                           
                                if ($row['presidentelicencia']!=null )
                                {   

                                $fechafinal=$row['fecha'];
                                $datetime1=date_create_from_format('Y-m-d', $fechafinal);
                                $datetimecomparable= $row['presidentefinal'];
                                $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                                
                                if ($datetime1 < $datetime2)
                                {
                                   
                                    $Colorpresidente="#FF0000";

                                }
                                 

                                }
                                if ($row['vocallicencia']!=null )
                                {   

                                $fechafinal=$row['fecha'];
                                $datetime1=date_create_from_format('Y-m-d', $fechafinal);
                                $datetimecomparable= $row['vocalfinal'];
                                $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                                
                                if ($datetime1 < $datetime2)
                                {
                                    $Colorvocal="#FF0000";

                                }
                                 

                                }
                                if ($row['vocal1licencia']!=null )
                                {   

                                $fechafinal=$row['fecha'];
                                $datetime1=date_create_from_format('Y-m-d', $fechafinal);
                                $datetimecomparable= $row['vocal1final'];
                                $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                                
                                if ($datetime1 < $datetime2)
                                {
                                    $Colorvocal1="#FF0000";

                                }
                                 

                                }
                                if ($row['suplentelicencia']!=null )
                                {   

                                $fechafinal=$row['fecha'];
                                $datetime1=date_create_from_format('Y-m-d', $fechafinal);
                                $datetimecomparable= $row['suplentefinal'];
                                $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                                
                                if ($datetime1 < $datetime2)
                                {
                                    $Colorsuplente="#FF0000";

                                }
                                 

                                }
                                $filas.='
                                <td style="background-color: '.$Colorpresidente.'">'.$row['presidente'].'</td>
                                <td style="background-color: '.$Colorvocal.'">'.$row['vocal'].'</td>
                                <td style="background-color: '.$Colorvocal1.'">'.$row['vocal1'].'</td>
                                ';

                                if($row['suplente'] == null)
                                {

                                    $filas.='
                                    <td> </td>
                                    <td>'.$row['fecha'].'</td>
                                    <td>'.$row['Hora'].'</td>
                                    <td>
                                        <a title="Ver detalle" href="examen.ver.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                                            <button type="button" class="btn btn-outline-info">
                                                <span class="oi oi-zoom-in"></span>
                                            </button></a>
    
                                    </td>
                                    <td>
                                        <a title="Modificar" href="examen.modificar.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                                            <button type="button" class="btn btn-outline-warning">
                                                <span class="oi oi-pencil"></span>
                                            </button></a>
                                    </td>
                                    <td>
                                        <a title="Eliminar" href="examen.eliminar.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                                          <button class="btn btn-outline-danger" class="btn btn-outline-info">
                                             <span class="oi oi-trash"></span>
                                          </button></a>
                                    </td>
                                </tr>
                                    
                                    ';
                                }
                                else
                                {
                                    $filas.='
                                    <td style="background-color: '.$Colorsuplente.'">'.$row['suplente'].' </td>
                                    <td>'.$row['fecha'].'</td>
                                    <td>'.$row['Hora'].'</td>
                                    <td>
                                        <a title="Ver detalle" href="examen.ver.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                                            <button type="button" class="btn btn-outline-info">
                                                <span class="oi oi-zoom-in"></span>
                                            </button></a>
    
                                    </td>
                                    <td>
                                        <a title="Modificar" href="examen.modificar.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                                            <button type="button" class="btn btn-outline-warning">
                                                <span class="oi oi-pencil"></span>
                                            </button></a>
                                    </td>
                                    <td>
                                        <a title="Eliminar" href="examen.eliminar.php?id='.$row['tribunalmesaid'].'&id1='.$row['mesaid'].'&id2='.$row['codAsignatura'].'&id3='.$row['idTurno'].'">
                                          <button class="btn btn-outline-danger" class="btn btn-outline-info">
                                             <span class="oi oi-trash"></span>
                                          </button></a>
                                    </td>
                                </tr>
                                    
                                    ';
                                }


                               
                               
                            }
                            echo $filas;
                        }
                    else 
                    {
                        $filas='<div class="alert alert-warning" role="alert">No se encontraron resultados</div>';
                    }
                    
                    ?>
                  


                  </tbody>
                 
                    </table>
                </div>
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
