<?php

include_once 'BDObjetoGenerico.Class.php';
include_once 'Rol.Class.php';


class Asignatura extends BDObjetoGenerico {

    function __construct($id = null) {
        parent::__construct($id, "Asignatura");
    }

    function codAsignatura() {
        return $this->codAsignatura;
    }
    function getNombre() {
        return $this->nombre;
    }
    function getDepartamento(){
        return $this->departamento;
    }
    function getcontenidosMinimos(){
        return $this->contenidosMinimos;
    }
    function getidProfesor(){
        return $this->idProfesor;
    }

    function setId($codAsignatura) {
        $this->codAsignatura = $codAsignatura;
    }
    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    function setDepartamento($departamento){
        $this->departamento=$departamento;
    }
    function setcontenidosMinimos($contenidosMinimos){
        $this->contenidosMinimos=$contenidosMinimos;
    }

    function setidProfesor($idProfesor){
        $this->idProfesor= $idProfesor;
    }
}
