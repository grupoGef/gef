<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARMESAEXAMEN);


if(isset($_GET["id"]))
{
    $tribunalmesaid=$_GET["id"];
}
if(isset($_GET["id1"]))
{
    $mesaid=$_GET["id1"];
}

if(isset($_GET["id2"]))
{
    $codasignaturaid=$_GET["id2"];
}

$sql='Select a.nombre,c.nombre,c.codCarrera,a.nombre as nombreasignatura,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal,l2.id as vocal1licencia,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on mec.codCarrera=c.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
licencia l3 on p3.id=l3.idProfesor';
$consulta = BDConexion::getInstancia()->query($sql);



?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Acerca del Examen</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>

        <?php
        
                    
        $sql='Select a.nombre,ca.nombre as nombrecarrera,c.codCarrera,p.preferencias as presidentepreferencias,p1.preferencias as vocalpreferencias,p2.preferencias as vocal1preferencias,p3.preferencias as suplentepreferencias ,au.nombre as nombreaula,au.sector as sectornombre,a.nombre as nombreasignatura,t.periodollamado,tm.fecha as fecha,tm.id tribunalmesaid,m.codMesa as mesaid, tm.hora as Hora,t.idTurno,tm.idMesa,m.codAsignatura,m.idTribunal ,tr.id ,p.id as presidenteid, CONCAT(p.nombre, " ",p.apellido) as presidente,l.id presidentelicencia ,l.fechaInicio as presidenteinicio,l.fechaFinal as presidentefinal, p1.id as vocalid, CONCAT(p1.nombre, " ",p1.apellido) as vocal,l1.id as vocallicencia,l1.fechaFinal as vocalfinal, l1.fechaInicio as vocalinicio,l2.id as vocal1licencia,l2.fechainicio as vocal1inicio,l2.fechaFinal as vocal1final,l3.id as suplentelicencia,p2.id as vocal1id , CONCAT(p2.nombre, " ",p2.apellido) as vocal1,p3.id as suplenteid,CONCAT(p3.nombre, " ",p3.apellido) as suplente ,l3.fechaInicio as suplenteinicio,l3.fechaFinal as suplentefinal from turno t left JOIN turno_mesaexamen tm on t.idTurno = tm.idTurno left join mesa_examen m on tm.idMesa= m.codMesa left join tribunal tr on m.idTribunal= tr.id left join profesor p on p.id= tr.presidente left join profesor p1 on p1.id= tr.vocal left join profesor p2 on p2.id= tr.vocal1 left join profesor p3 on p3.id= tr.suplente left join asignatura a on a.codAsignatura= m.codAsignatura left join mesa_examen_carrera mec on mec.codMesa=m.codMesa left join carrera c on mec.codCarrera=c.codCarrera left join carrera ca on a.idCarrera=ca.codCarrera left JOIN licencia l on p.id= l.idProfesor left join 
        licencia l1 on p1.id=l1.idProfesor left join licencia l2 on p2.id = l2.idProfesor left JOIN
        licencia l3 on p3.id=l3.idProfesor left join aula au on tm.codAula=au.codAula
        where tm.id='.$tribunalmesaid.' and m.codMesa='.$mesaid.' and a.codAsignatura='.$codasignaturaid;
  
        $consulta2 = BDConexion::getInstancia()->query($sql);                       
        
        
        while($row = $consulta2->fetch_array())
        {
          
          if ($row['suplenteid']== NULL && $row['suplente']== NULL )
          {
            $datos='
            <div class="container">
            <p></p>
            <div class="card">
                <div class="card-header">
                <p><center><h3>Información de la Mesa de Examen</h3></center></p>
                </div>
                <div class="card-body">
                      <h4 class="card-text">Asignatura</h4>
                    <p>'.$row['nombreasignatura'].'  </p>
                    <hr />
                    <h4 class="card-text">Carrera</h4>
                    <p>'.$row['nombrecarrera'].'  </p>
                    <hr />
                    <h4 class="card-text">Llamado</h4>
                    <p>';
                    if($row['periodollamado'] ==1)
                    {
                        $llamado='Primer Llamado';
                    }
                    else
                    {
                        $llamado='Segundo Llamado';
                    }
                    
                    $datos.=''.
                   $llamado. 
                    '  </p>
                    <hr />
                    ';
                    
                    $datos.='
                    <h4 class="card-text">Fecha</h4>
                    <p>'.$row['fecha'].'</p>
                    <hr />
                    ';
                    
                    if($row['Hora'] != '00:00:00')
                    {                   
                    $datos.='
                    <h4 class="card-text">Hora</h4>
                    <p>'.$row['Hora'].'</p>
                    <hr />
                    ';
                    }
                    if($row['nombreaula'] && $row['sectornombre'])
                    {
                    $datos.='<h4 class="card-text">Aula/Sector</h4>
                    <p>'.$row['nombreaula'].' / '.$row['sectornombre'].'</p>
                    <hr />';
                    }
                    $datos.='
                      <h4 class="card-text">Presidente</h4>
                    <p>'.$row['presidente'].'</p>
                   

                    <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo1">Detalle</button>
                    <div id="demo1" class="collapse">
                    <br>
                    <div class="row">
                    <div class="col-sm">
                    Preferencias:   
                    '.$row['presidentepreferencias'].'
                  </div>';
                  $fechafinal=$row['fecha'];
                  $datetime1=date_create_from_format('Y-m-d', $fechafinal);
                  $datetimecomparable= $row['presidentefinal'];
                  $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                  
                  if ($datetime1 < $datetime2)
                  {$datos.='
                  <div class="col-sm">
                    Inicio Licencia:'.$row['presidenteinicio'].'
                  </div>
                  <div class="col-sm">
                    Final de Licencia:'.$row['presidentefinal'].'
                  </div>';
                  }
                 $datos.='
                </div>
                </div>
                <hr />
                  <h4 class="card-text">Primer Vocal</h4>
                  <p>'.$row['vocal'].'</p>
               

                  <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo2">Detalle</button>
                  <div id="demo2" class="collapse">
                  <br>
                  <div class="row">
                  <div class="col-sm">
                  Preferencias:   
                  '.$row['vocalpreferencias'].'
                </div>';
                $fechafinal=$row['fecha'];
                $datetime1=date_create_from_format('Y-m-d', $fechafinal);
                $datetimecomparable= $row['vocalfinal'];
                $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                
                if ($datetime1 < $datetime2)
                {$datos.='
                  <div class="col-sm">
                  Inicio Licencia:'.$row['vocalinicio'].'
                </div>
                <div class="col-sm">
                  Final de Licencia:'.$row['vocalfinal'].'
                </div>';
                }
               $datos.=' 
              </div>
              </div>
              <hr />
                <h4 class="card-text">Segundo Vocal</h4>
                <p>'.$row['vocal1'].'</p>
               

                <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo3">Detalle</button>
                <div id="demo3" class="collapse">
                <br>
                <div class="row">
                <div class="col-sm">
                Preferencias:   
                '.$row['vocal1preferencias'].'
              </div>';
              $fechafinal=$row['fecha'];
              $datetime1=date_create_from_format('Y-m-d', $fechafinal);
              $datetimecomparable= $row['vocal1final'];
              $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
              
              if ($datetime1 < $datetime2)
              {$datos.='
                <div class="col-sm">
                Inicio Licencia:'.$row['vocal1inicio'].'
              </div>
              <div class="col-sm">
                Final de Licencia:'.$row['vocal1final'].'
              </div>';
              };
              $datos.='
            </div>
            </div>
            <hr />



                    ';
                }
            else
            {
                $datos='
                <div class="container">
                <p></p>
                <div class="card">
                    <div class="card-header">
                    <p><center><h3>Información de la Mesa de Examen</h3></center></p>
                    </div>
                    <div class="card-body">
                          <h4 class="card-text">Asignatura</h4>
                        <p>'.$row['nombreasignatura'].'  </p>
                        <hr />
                        <h4 class="card-text">Carrera</h4>
                        <p>'.$row['nombrecarrera'].'  </p>
                        <hr />
                        <h4 class="card-text">Llamado</h4>
                        <p>';
                        if($row['periodollamado'] ==1)
                        {
                            $llamado='Primer Llamado';
                        }
                        else
                        {
                            $llamado='Segundo Llamado';
                        }
                        
                        $datos.=''.
                       $llamado. 
                        '  </p>
                        <hr />
                        <h4 class="card-text">Fecha</h4>
                        <p>'.$row['fecha'].'</p>
                        <hr />';
                        
                        if($row['Hora'] != '00:00:00')
                        {                   
                        $datos.='
                        <h4 class="card-text">Hora</h4>
                        <p>'.$row['Hora'].'</p>
                        <hr />
                        ';
                        }
                        if($row['nombreaula'] && $row['sectornombre'])
                        {
                        $datos.='<h4 class="card-text">Aula/Sector</h4>
                        <p>'.$row['nombreaula'].' / '.$row['sectornombre'].'</p>
                        <hr />';
                        }
                        $datos.='
                          <h4 class="card-text">Presidente</h4>
                        <p>'.$row['presidente'].'</p>
                        
    
                        <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo1">Detalle</button>
                        <div id="demo1" class="collapse">
                        <br>
                        <div class="row">
                        <div class="col-sm">
                        Preferencias:   
                          '.$row['presidentepreferencias'].'
                        </div>';
                        $fechafinal=$row['fecha'];
                        $datetime1=date_create_from_format('Y-m-d', $fechafinal);
                        $datetimecomparable= $row['presidentefinal'];
                        $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                        
                        if ($datetime1 < $datetime2)
                        {$datos.='
                        <div class="col-sm">
                          Inicio Licencia:'.$row['presidenteinicio'].'
                        </div>
                        <div class="col-sm">
                          Final de Licencia:'.$row['presidentefinal'].'
                        </div>';
                        }
                       $datos.='
                      </div>
                      </div>
                      <hr />
                      <h4 class="card-text">Primer Vocal</h4>
                      <p>'.$row['vocal'].'</p>
                     
    
                      <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo2">Detalle</button>
                      <div id="demo2" class="collapse">
                      <br>
                      <div class="row">
                      <div class="col-sm">
                      Preferencias:   
                        '.$row['vocalpreferencias'].'
                      </div>';
                      $fechafinal=$row['fecha'];
                      $datetime1=date_create_from_format('Y-m-d', $fechafinal);
                      $datetimecomparable= $row['vocalfinal'];
                      $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                      
                      if ($datetime1 < $datetime2)
                      {$datos.='
                        <div class="col-sm">
                        Inicio Licencia:'.$row['vocalinicio'].'
                      </div>
                      <div class="col-sm">
                        Final de Licencia:'.$row['vocalfinal'].'
                      </div>';
                      }
                     $datos.=' 
                    </div>
                    </div>
                    <hr />
                    <h4 class="card-text">Segundo Vocal</h4>
                    <p>'.$row['vocal1'].'</p>
                   
    
                    <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo3">Detalle</button>
                    <div id="demo3" class="collapse">
                    <br>
                    <div class="row">
                    <div class="col-sm">
                    Preferencias:   
                      '.$row['vocal1preferencias'].'
                    </div>';
                    $fechafinal=$row['fecha'];
                    $datetime1=date_create_from_format('Y-m-d', $fechafinal);
                    $datetimecomparable= $row['vocal1final'];
                    $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                    
                    if ($datetime1 < $datetime2)
                    {$datos.='
                      <div class="col-sm">
                      Inicio Licencia:'.$row['vocal1inicio'].'
                    </div>
                    <div class="col-sm">
                      Final de Licencia:'.$row['vocal1final'].'
                    </div>';
                    };
                    $datos.='
                  </div>
                  </div>
                  <hr />
                                    <h4 class="card-text">Suplente</h4>
                      <p>'.$row['suplente'].'</p>
                      
    
                      <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo4">Detalle</button>
                      <div id="demo4" class="collapse">
                      <br>
                      <div class="row">
                      <div class="col-sm">
                      Preferencias:   
                        '.$row['suplentepreferencias'].'
                      </div>';
                      $fechafinal=$row['fecha'];
                      $datetime1=date_create_from_format('Y-m-d', $fechafinal);
                      $datetimecomparable= $row['suplentefinal'];
                      $datetime2=date_create_from_format('Y-m-d', $datetimecomparable);
                      if ($datetime1 < $datetime2)
                      {$datos.='
                        <div class="col-sm">
                        Inicio Licencia:'.$row['suplenteinicio'].'
                      </div>
                      <div class="col-sm">
                        Final de Licencia:'.$row['suplentefinal'].'
                      </div>';
                      };
                      $datos.='
                      </div>
                    </div>
                    </div>
                    <hr />
    
    
    
                        ';
                    }
                    

        }
        $datos.='
        <br>
        <br>
        <br>
        <br>
        <h5 class="card-text">Opciones</h5>
                    <a href="gestionExamen.php">
                        <button type="button" class="btn btn-primary">
                            <span class="oi oi-account-logout"></span> Salir
                        </button>
                    </a>
                </div>
            </div>
        </div>
        ';

        echo $datos;

            



          

        
        ?>
        
    
    </body>
</html>