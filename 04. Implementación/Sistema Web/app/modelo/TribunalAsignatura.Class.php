<?php

include_once 'BDObjetoGenerico.Class.php';
include_once 'Rol.Class.php';

class TribunalAsignatura extends BDObjetoGenerico {

    protected $email;

    /**
     *
     * @var Rol[]
     */
    private $roles;

    function __construct($id = null) {
        parent::__construct($id,"tribunalasignaturas");
    }

    function getId() {
        return $this->id;
    }
    function getTribunalid() {
        return $this->TRIBUNAL_id;
    }
    function getAsignaturaid() {
        return $this->ASIGNATURA_id;
    }

    function setId($idTribunalAsignatura) {
        $this->idTribunalAsignatura = $idTribunalAsignatura;
    }
    function setTribunalid($TRIBUNAL_id) {
        $this->TRIBUNAL_id = $TRIBUNAL_id;
    }
    function setAsignaturaid($ASIGNATURA_id) {
        $this->ASIGNATURA_id = $ASIGNATURA_id;
    }
   



}
