-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-02-2022 a las 11:03:56
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdusuarios`
--

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `CargaPrimerllamado`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CargaPrimerllamado` (IN `dia1` VARCHAR(60) CHARSET utf8, IN `fecha1` VARCHAR(60), IN `dia2` VARCHAR(60) CHARSET utf8, IN `fecha2` VARCHAR(60), IN `dia3` VARCHAR(60) CHARSET utf8, IN `fecha3` VARCHAR(60), IN `dia4` VARCHAR(60) CHARSET utf8, IN `fecha4` VARCHAR(60), IN `dia5` VARCHAR(60) CHARSET utf8, IN `fecha5` VARCHAR(60), IN `turnact` VARCHAR(2), IN `tipomg` VARCHAR(60))  BEGIN
DELETE FROM mesa_examen;
DELETE FROM turno_mesaexamen;

CREATE TEMPORARY TABLE dia1 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia1 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,t.preferenciapresidente, fecha1 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where t.preferenciapresidente = dia1
;

CREATE TEMPORARY TABLE dia2 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia2 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,t.preferenciapresidente, fecha2 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where t.preferenciapresidente = dia2
;
CREATE TEMPORARY TABLE dia3 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia3 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,t.preferenciapresidente, fecha3 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where t.preferenciapresidente = dia3
;
CREATE TEMPORARY TABLE dia4 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia4 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,t.preferenciapresidente, fecha4 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where t.preferenciapresidente = dia4
;
CREATE TEMPORARY TABLE dia5 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia5 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,t.preferenciapresidente, fecha5 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where t.preferenciapresidente = dia5
;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia1;
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d1.fecha from dia1 d1 left JOIN
mesa_examen m on d1.codAsignatura=m.codAsignatura
where d1.codTribunal=m.idTribunal;


INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia2;
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d2.fecha from dia2 d2 left JOIN
mesa_examen m on d2.codAsignatura=m.codAsignatura
where d2.codTribunal=m.idTribunal;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia3;
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d3.fecha from dia3 d3 left JOIN
mesa_examen m on d3.codAsignatura=m.codAsignatura
where d3.codTribunal=m.idTribunal;


INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia4;
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d4.fecha from dia4 d4 left JOIN
mesa_examen m on d4.codAsignatura=m.codAsignatura
where d4.codTribunal=m.idTribunal;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia5;
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d5.fecha from dia5 d5 left JOIN
mesa_examen m on d5.codAsignatura=m.codAsignatura
where d5.codTribunal=m.idTribunal;

UPDATE turno_mesaexamen
Set idTurno = 1;

END$$

DROP PROCEDURE IF EXISTS `CargaSegundollamado`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CargaSegundollamado` (IN `dia1` VARCHAR(60) CHARSET utf8, IN `fecha1` VARCHAR(60), IN `dia2` VARCHAR(60) CHARSET utf8, IN `fecha2` VARCHAR(60), IN `dia3` VARCHAR(60) CHARSET utf8, IN `fecha3` VARCHAR(60), IN `dia4` VARCHAR(60) CHARSET utf8, IN `fecha4` VARCHAR(60), IN `dia5` VARCHAR(60) CHARSET utf8, IN `fecha5` VARCHAR(60), IN `turnact` INT, IN `tipomg` VARCHAR(60) CHARSET utf8)  BEGIN 

DECLARE dia1mesa nvarchar(30) DEFAULT 0;
SET dia1mesa = dia1;
DROP TEMPORARY TABLE IF EXISTS dia1;
DROP TEMPORARY TABLE IF EXISTS dia2;
DROP TEMPORARY TABLE IF EXISTS dia3;
DROP TEMPORARY TABLE IF EXISTS dia4;
DROP TEMPORARY TABLE IF EXISTS dia5;


CREATE TEMPORARY TABLE dia1 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia1 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,t.preferenciapresidente, fecha1 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where t.preferenciapresidente= dia1mesa
;

CREATE TEMPORARY TABLE dia2 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia2 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,t.preferenciapresidente, fecha2 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where t.preferenciapresidente= dia2
;
CREATE TEMPORARY TABLE dia3 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia3 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,t.preferenciapresidente, fecha3 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where t.preferenciapresidente= dia3
;
CREATE TEMPORARY TABLE dia4 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia4 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,t.preferenciapresidente, fecha4 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where t.preferenciapresidente= dia4
;
CREATE TEMPORARY TABLE dia5 (
 codAsignatura int(12) NOT NULL 
, codTribunal int(12) not null
, fecha date  NULL
, horario varchar(30) NULL
, aula varchar(30) NULL                                   
, dia VARCHAR(30) NULL
);
INSERT INTO dia5 (codAsignatura,codTribunal,dia,fecha)
SELECT a.codAsignatura as codAsignatura,t.id as codTribunal,t.preferenciapresidente, fecha5 as fecha FROM tribunalasignatura ta left JOIN tribunal t on ta.idTribunal= t.id left JOIN profesor p1 on p1.id=t.presidente left join asignatura a on ta.idAsignatura= a.codAsignatura where t.preferenciapresidente= dia5
;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia1
where codAsignatura not in (Select codAsignatura from mesa_examen);
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d1.fecha from dia1 d1 left JOIN
mesa_examen m on d1.codAsignatura=m.codAsignatura
where d1.codTribunal=m.idTribunal;


INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia2
where codAsignatura not in (Select codAsignatura from mesa_examen);
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d2.fecha from dia2 d2 left JOIN
mesa_examen m on d2.codAsignatura=m.codAsignatura
where d2.codTribunal=m.idTribunal;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia3
where codAsignatura not in (Select codAsignatura from mesa_examen);
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d3.fecha from dia3 d3 left JOIN
mesa_examen m on d3.codAsignatura=m.codAsignatura
where d3.codTribunal=m.idTribunal;


INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia4
where codAsignatura not in (Select codAsignatura from mesa_examen);
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d4.fecha from dia4 d4 left JOIN
mesa_examen m on d4.codAsignatura=m.codAsignatura
where d4.codTribunal=m.idTribunal;

INSERT into mesa_examen (tipo,idTribunal,codAsignatura)
Select tipomg as tipo,codTribunal,codAsignatura  from dia5
where codAsignatura not in (Select codAsignatura from mesa_examen);
Insert into turno_mesaexamen (idTurno,idmesa,fecha)
Select turnact as idTurno,m.codMesa,d5.fecha from dia5 d5 left JOIN
mesa_examen m on d5.codAsignatura=m.codAsignatura
where d5.codTribunal=m.idTribunal;

UPDATE turno_mesaexamen
Set idTurno = 2
where idTurno=0 or idTurno is NULL;


END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignatura`
--

DROP TABLE IF EXISTS `asignatura`;
CREATE TABLE `asignatura` (
  `codAsignatura` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `departamento` varchar(45) NOT NULL,
  `contenidosMinimos` text NOT NULL,
  `idCarrera` int(2) DEFAULT NULL,
  `idProfesor` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `asignatura`
--

INSERT INTO `asignatura` (`codAsignatura`, `nombre`, `departamento`, `contenidosMinimos`, `idCarrera`, `idProfesor`) VALUES
(2, 'Analisis Matematico I', 'EXACTAS Y NATURALES', '1', 16, 0),
(3, 'Matematica discreta', 'EXACTAS Y NATURALES', '1', 16, 0),
(4, 'Organizacion de las computadoras', 'EXACTAS Y NATURALES', '1', 16, 0),
(5, 'Analisis y Produccion del Discurso', 'EXACTAS Y NATURALES', '1', 16, 0),
(6, 'Base de datos', 'EXACTAS Y NATURALES', '2', 16, 0),
(7, 'Analisis y Diseno del Software', 'EXACTAS Y NATURALES', '2', 16, 0),
(8, 'Ciencia, Universidad y Sociedad', 'EXACTAS Y NATURALES', '2', 16, 0),
(9, 'Estructura de Datos', 'EXACTAS Y NATURALES', '2', 16, 0),
(10, 'Sistemas Operativos Distribuidos', 'EXACTAS Y NATURALES', '3', 16, 0),
(11, 'Gestion de Proyectos de Software', 'EXACTAS Y NATURALES', '3', 16, 0),
(12, 'Laboratorio de Desarrollo de Software', 'EXACTAS Y NATURALES', '3', 16, 0),
(13, 'Estadistica I', 'EXACTAS Y NATURALES', '3', 16, 0),
(14, 'Patrimonio Turistico: Circuito I', 'SOCIALES', '1', NULL, 0),
(15, 'Ingles I', 'SOCIALES', '1', 16, 0),
(16, 'Geografia', 'SOCIALES', '1', NULL, 0),
(17, 'Procesos Historicos', 'SOCIALES', '1', NULL, 0),
(18, 'Analisis y Produccion del Discurso', 'SOCIALES', '1', NULL, 0),
(19, 'Ingles II', 'SOCIALES', '2', NULL, 0),
(20, 'Metodologia de la investigacion', 'SOCIALES', '2', NULL, 0),
(21, 'Servicios Turisticos', 'SOCIALES', '2', NULL, 0),
(23, 'Introduccion a la Estadistica', 'SOCIALES', '2', NULL, 0),
(24, 'Legislacion Turistica Patrimonial y Ambiental', 'SOCIALES', '3', NULL, 0),
(25, 'Geografia Turistica Ev Impacto Amb ', 'SOCIALES', '3', NULL, 0),
(28, 'Practica Profesional I', 'SOCIALES', '3', NULL, 0),
(29, 'Conservacion de Sitios Culturales Arqueologicos y Paleontologicos', 'SOCIALES', '4', NULL, 0),
(30, 'Politica del Turismo', 'SOCIALES', '4', NULL, 0),
(33, 'Gestion y Administracion de Empresas Turisticas', 'SOCIALES', '4', NULL, 0),
(34, 'Practica II', 'SOCIALES', '4', NULL, 0),
(35, 'Proceso y Desarrollo de Software', 'EXACTAS Y NATURALES', '3', 16, 0),
(36, 'Analisis Matematico II', 'EXACTAS Y NATURALES', '5', 16, 0),
(37, 'Programacion Orientada a Objetos', 'EXACTAS Y NATURALES', '2', 16, 0),
(38, 'Arquitectura de las Computadoras', 'EXACTAS Y NATURALES', '5', 16, 0),
(39, 'Requerimientos de Software', 'EXACTAS Y NATURALES', '5', 16, 0),
(40, 'Sistemas Operativos', 'EXACTAS Y NATURALES', '5', 16, 0),
(41, 'Laboratorio de Programacion', 'EXACTAS Y NATURALES', '5', 16, 0),
(42, 'Redes y Telecomunicaciones', 'EXACTAS Y NATURALES', '13', 16, 0),
(43, 'Fundamentos de la ciencia y la computacion', 'EXACTAS Y NATURALES', '3', 16, 0),
(44, 'Enseñanza y curriculum', 'SOCIALES', '1', 60, 0),
(45, 'Didactica especial', 'SOCIALES', '1', 60, 0),
(46, 'Aprendizaje', 'SOCIALES', '1', 60, 0),
(47, 'Lengua y cultura latinas i', 'SOCIALES', '1', 60, 0),
(48, 'Literatura griega i', 'SOCIALES', '1', 60, 0),
(49, 'Historia de la lengua', 'SOCIALES', '1', 60, 0),
(50, 'Lingüistica i', 'SOCIALES', '1', 60, 0),
(51, 'Lingüistica ii', 'SOCIALES', '1', 60, 0),
(52, 'Seminario de lingüistica', 'SOCIALES', '1', 60, 0),
(53, 'Seminario de teoria literaria', 'SOCIALES', '1', 60, 0),
(54, 'Idioma moderno ingles', 'SOCIALES', '1', 60, 0),
(55, 'Literatura española ii', 'SOCIALES', '1', 60, 0),
(56, 'Herramientas De Informatica', 'SOCIALES', '1', 918, 0),
(57, 'Teoria De La Comunicacion II', 'SOCIALES', '1', 918, 0),
(58, 'Taller De Diseño Grafico Y Fotografía ', 'SOCIALES', '1', 918, 0),
(59, 'Metodología De La Investigación En Comunicación', 'SOCIALES', '1', 918, 0),
(60, 'Seminario De Investigación Periodística', 'SOCIALES', '1', 918, 0),
(61, 'Introducción a los Medios Masivos', 'SOCIALES', '1', 918, 0),
(63, 'Validación y Verificación de Software', 'EXACTAS Y NATURALES', '1', 16, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aula`
--

DROP TABLE IF EXISTS `aula`;
CREATE TABLE `aula` (
  `codAula` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `sector` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aula`
--

INSERT INTO `aula` (`codAula`, `nombre`, `sector`) VALUES
(1, 'A6', 'A'),
(2, 'A5', 'A'),
(3, '1', 'A'),
(4, '2', 'A'),
(5, '3', 'A'),
(6, 'Laboratorio 4', 'A'),
(9, '7', 'A'),
(10, '8', 'A'),
(11, '9', 'A'),
(12, '10', 'A'),
(13, '11', 'A'),
(14, '12', 'A'),
(15, '13', 'A'),
(16, '14', 'A'),
(17, 'Box 1', 'B'),
(18, 'Box 2', 'B'),
(19, 'Box 3', 'B'),
(20, 'Box 4', 'B'),
(21, 'Box 5', 'B'),
(22, 'Box 6', 'B'),
(23, 'Box 7', 'B'),
(24, 'Box 8', 'B'),
(25, 'Box 9', 'B'),
(26, 'Box 10', 'B'),
(27, 'Box 11', 'B'),
(28, 'Box 12', 'B'),
(29, 'Box 13', 'B'),
(30, 'Box 14', 'B');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

DROP TABLE IF EXISTS `cargo`;
CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `dedicacion` varchar(45) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `idProfesor` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id`, `dedicacion`, `categoria`, `idProfesor`) VALUES
(1, 'Exclusiva', 'Titular', 1),
(2, 'Parcial', 'Adjunto', 2),
(35, 'Exclusiva', 'Adjunto', 3),
(36, 'Parcial', 'Asociado', 4),
(37, 'Exclusiva', 'Titular', 5),
(38, 'Exclusiva', 'Adjunto', 6),
(39, 'Parcial', 'Ayudante', 7),
(40, 'Exclusiva', 'Titular', 8),
(41, 'Parcial', 'Asociado', 9),
(42, 'Simple', 'Adjunto', 10),
(43, 'Exclusiva', 'Titular', 11),
(44, 'Exclusiva', 'Adjunto', 12),
(45, 'Parcial', 'Ayudante', 13),
(46, 'Simple', 'Adjunto', 14),
(47, 'Exclusiva', 'Titular', 15),
(48, 'Exclusiva', 'Adjunto', 16),
(49, 'Exclusiva', 'Adjunto', 17),
(50, 'Simple', 'Ayudante', 18),
(51, 'Exclusiva', 'Adjunto', 19),
(52, 'Exclusiva', 'Titular', 20),
(53, 'Exclusiva', 'Adjunto', 21),
(54, 'Simple', 'Adjunto', 22),
(55, 'Exclusiva', 'Titular', 23),
(56, 'Exclusiva', 'Adjunto', 24),
(57, 'Exclusiva', 'Ayudante', 25),
(58, 'Exclusiva', 'Adjunto', 26),
(59, 'Exclusiva', 'Titular', 27),
(60, 'Exclusiva', 'Titular', 28),
(61, 'Exclusiva', 'Ayudante', 29),
(62, 'Simple', 'Asociado', 30),
(63, 'Exclusiva', 'Titular', 31),
(64, 'Exclusiva', 'Titular', 32),
(65, 'Simple', 'Ayudante', 67);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera`
--

DROP TABLE IF EXISTS `carrera`;
CREATE TABLE `carrera` (
  `codCarrera` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carrera`
--

INSERT INTO `carrera` (`codCarrera`, `nombre`) VALUES
(3, 'Profesorado en Historia'),
(4, 'Profesorado en Geografía'),
(16, 'Analista de Sistemas'),
(23, 'Ingenieria en Recursos Naturales Renovables'),
(38, 'Licenciatura En Turismo'),
(45, 'Licenciatura en Psicopedagogia'),
(46, 'Enfermeria Universitaria'),
(47, 'Licenciatura en Enfermeria'),
(60, 'Licenciatura en Letras'),
(61, 'Profesorado en Letras'),
(64, 'Licenciatura en Geografia'),
(72, 'Licenciatura en Sistemas'),
(74, 'Licenciatura en Trabajo Social'),
(76, 'Tecnicatura Universitaria en Acompañamiento Terapeutico'),
(912, 'Tecnicatura Universitaria en Gestion de Organizaciones'),
(913, 'Licenciatura en Administracion'),
(914, 'Profesorado en Economia y Gestión de Organizaciones'),
(918, 'Licenciatura en Comunicación Social');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correlativad`
--

DROP TABLE IF EXISTS `correlativad`;
CREATE TABLE `correlativad` (
  `id` int(11) NOT NULL,
  `requisito` enum('Regular','Aprobada') DEFAULT NULL,
  `codAsignatura` int(10) UNSIGNED NOT NULL,
  `codAsignatura_Correlatividad` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

DROP TABLE IF EXISTS `departamento`;
CREATE TABLE `departamento` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id`, `nombre`) VALUES
(1, 'EXACTAS Y NATURALES'),
(2, 'SOCIALES');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fecha`
--

DROP TABLE IF EXISTS `fecha`;
CREATE TABLE `fecha` (
  `dia` varchar(45) NOT NULL,
  `mes` varchar(45) NOT NULL,
  `año` varchar(45) NOT NULL,
  `idLlamado` int(10) UNSIGNED NOT NULL,
  `indice` int(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `licencia`
--

DROP TABLE IF EXISTS `licencia`;
CREATE TABLE `licencia` (
  `id` int(10) UNSIGNED NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFinal` date NOT NULL,
  `idProfesor` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `licencia`
--

INSERT INTO `licencia` (`id`, `fechaInicio`, `fechaFinal`, `idProfesor`) VALUES
(1, '2021-11-01', '2021-12-31', 15),
(2, '2021-11-01', '2022-03-01', 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `llamado_mesa_examen`
--

DROP TABLE IF EXISTS `llamado_mesa_examen`;
CREATE TABLE `llamado_mesa_examen` (
  `idLlamado` int(10) UNSIGNED NOT NULL,
  `codMesa` int(10) UNSIGNED NOT NULL,
  `hora` time(5) NOT NULL,
  `dia` int(2) UNSIGNED NOT NULL,
  `fechaUnica` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `llamado_mesa_examen`
--

INSERT INTO `llamado_mesa_examen` (`idLlamado`, `codMesa`, `hora`, `dia`, `fechaUnica`) VALUES
(1, 1, '00:00:12.00000', 1, '2021-11-01'),
(2, 1, '00:00:12.00000', 12, '2021-11-02'),
(3, 2, '00:00:12.00000', 1, '0000-00-00'),
(4, 2, '00:00:12.00000', 12, '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa_examen`
--

DROP TABLE IF EXISTS `mesa_examen`;
CREATE TABLE `mesa_examen` (
  `codMesa` int(10) UNSIGNED NOT NULL,
  `tipo` enum('todoTiempo','general','extraordinaria') NOT NULL,
  `idTribunal` int(10) UNSIGNED NOT NULL,
  `codAsignatura` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mesa_examen`
--

INSERT INTO `mesa_examen` (`codMesa`, `tipo`, `idTribunal`, `codAsignatura`) VALUES
(1247, 'general', 50, 56),
(1248, 'general', 52, 58),
(1250, 'general', 26, 37),
(1251, 'general', 27, 38),
(1252, 'general', 37, 44),
(1253, 'general', 41, 48),
(1257, 'general', 29, 40),
(1258, 'general', 30, 41),
(1259, 'general', 35, 6),
(1260, 'general', 38, 45),
(1261, 'general', 47, 54),
(1262, 'general', 48, 55),
(1263, 'general', 51, 57),
(1264, 'general', 53, 59),
(1265, 'general', 54, 60),
(1266, 'general', 55, 61),
(1272, 'general', 24, 35),
(1273, 'general', 28, 39),
(1274, 'general', 32, 43),
(1275, 'general', 33, 13),
(1276, 'general', 34, 11),
(1277, 'general', 36, 12),
(1278, 'general', 39, 46),
(1279, 'general', 40, 47),
(1280, 'general', 43, 50),
(1281, 'general', 44, 51),
(1287, 'general', 31, 42),
(1288, 'general', 42, 49),
(1289, 'general', 45, 52),
(1290, 'general', 46, 53);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa_examen_carrera`
--

DROP TABLE IF EXISTS `mesa_examen_carrera`;
CREATE TABLE `mesa_examen_carrera` (
  `codMesa` int(10) UNSIGNED NOT NULL,
  `codCarrera` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mesa_examen_carrera`
--

INSERT INTO `mesa_examen_carrera` (`codMesa`, `codCarrera`) VALUES
(1, 16),
(2, 16),
(3, 16);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `new_view`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `new_view`;
CREATE TABLE `new_view` (
`idMesaExamen` int(10) unsigned
,`codigoCarrera` int(10) unsigned
,`nombreLargoCarrera` varchar(200)
,`idAsignatura` int(10) unsigned
,`nombreLargoAsignatura` varchar(100)
,`sectorAula` varchar(45)
,`nombreAula` varchar(45)
,`fecha` date
,`hora` time
,`fechaEdicion` date
,`horaEdicion` time
,`turnoEdicion` int(11)
,`turno` int(11)
,`nombrePresidente` varchar(96)
,`nombreVocalPrimero` varchar(96)
,`nombreVocalSegundo` varchar(96)
,`nombreSuplente` varchar(96)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedad`
--

DROP TABLE IF EXISTS `novedad`;
CREATE TABLE `novedad` (
  `id` int(11) NOT NULL,
  `idTurno` int(11) DEFAULT NULL,
  `fechaVieja` date DEFAULT NULL,
  `fechaNueva` date DEFAULT NULL,
  `horaVieja` time DEFAULT NULL,
  `horaNueva` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

DROP TABLE IF EXISTS `permiso`;
CREATE TABLE `permiso` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`id`, `nombre`) VALUES
(1, 'Gestionar Examen'),
(2, 'Inicio'),
(3, 'Gestionar Novedades'),
(4, 'Gestionar Afectacion'),
(5, 'Gestionar Docente'),
(7, 'Usuarios'),
(8, 'Roles'),
(9, 'Permisos'),
(11, 'Salir'),
(12, 'Ingresar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

DROP TABLE IF EXISTS `profesor`;
CREATE TABLE `profesor` (
  `id` int(10) NOT NULL,
  `dni` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `preferencias` varchar(60) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `dedicacion` varchar(45) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `idDepartamento` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`id`, `dni`, `nombre`, `apellido`, `preferencias`, `email`, `dedicacion`, `categoria`, `idDepartamento`) VALUES
(1, 47490271, 'Claudia', 'MANSILLA', 'Jueves', 'Claudiamansilla@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(2, 78259508, 'Daniel', 'JAREMCHUCK', 'Miercoles', 'danieljaremchuk@unpa.edu.ar', 'Parcial', 'Adjunto', 2),
(3, 65340726, 'Yamile', 'CARCAMO', 'Viernes', 'YCarcamo@unpa.edu.ar', 'Exclusiva', 'Adjunto', 2),
(4, 79041044, 'Carolina', 'MUSCI', 'Viernes', 'CarolinaMusci@unpa.edu.ar', 'Parcial', 'Asociado', 2),
(5, 89649464, 'Karim Omar', 'HALLAR', 'Jueves', 'karimhallar@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(6, 98972597, 'Leonardo', 'GONZALES', 'Viernes', 'LGonzales@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(7, 78348430, 'Rodrigo', 'FARIAS', 'Jueves', 'fariasr@unpa.edu.ar', 'Parcial', 'Ayudante', 1),
(8, 30504125, 'Sandra', 'VILCA', 'Lunes', 'SVilca@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(9, 66133151, 'Valeria', 'FORCHINO', 'Jueves', 'Vforchino@unpa.edu.ar', 'Parcial', 'Asociado', 1),
(10, 68851196, 'Laura', 'VILLANUEVA', 'Jueves', 'Lauravillanueva@unpa.edu.ar', 'Simple', 'Adjunto', 1),
(11, 11325222, 'Sandra', 'CASAS', 'Martes', 'Scasas@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(12, 91669859, 'Claudio', 'SALDIVIA', 'Martes', 'Saldiviaclaudio@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(13, 88576924, 'Franco', 'HERRERA', 'Miercoles', 'Fherrera@unpa.edu.ar', 'Parcial', 'Ayudante', 1),
(14, 98661302, 'Fernanda Daniela', 'OYARZO', 'Viernes', 'Foyarzo@unpa.edu.ar', 'Simple', 'Adjunto', 1),
(15, 84817596, 'Carlos Alberto', 'TALAY', 'Martes', 'Ctalay@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(16, 70757406, 'Mauro', 'ZHIELKE', 'Jueves', 'mzhielke@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(17, 17325880, 'Claudia', 'GONZALES', 'Viernes', 'Cgonzales@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(18, 25524238, 'Carlos Daniel', 'AMARILLA', 'Jueves', 'camarilla@unpa.edu.ar', 'Simple', 'Ayudante', 1),
(19, 78042027, 'Esteban Guillermo', 'GESTO', 'Viernes', 'egesto@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(20, 23645268, 'Hector Manuel', 'SOTO', 'Miercoles', 'hsoto@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(21, 24810100, 'Luis Mauricio', 'SIERPE OYARZO', 'Jueves', 'Luissierpe@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(22, 36793324, 'Carlos Gustavo', 'LIVACIC', 'Viernes', 'Clivacic@unpa.edu.ar', 'Simple', 'Adjunto', 1),
(23, 34569183, 'Hector Hipolito', 'REINAGA', 'Miercoles', 'hreinaga@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(24, 39204542, 'Daniel Andres', 'GONZALES', 'Martes', 'danielgonzales@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(25, 66179834, 'Juan Gabriel', 'ENRIQUEZ', 'Lunes', 'jenriquez@unpa.edu.ar', 'Exclusiva', 'Ayudante', 1),
(26, 34634676, 'Diego Raul', 'RODRIGUEZ HERLING', 'Viernes', 'Rherling@unpa.edu.ar', 'Exclusiva', 'Adjunto', 1),
(27, 34504123, 'Paula Alejandra', 'MILLADO', 'Jueves', 'pmillado@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(28, 30446520, 'Albert Anibal Osiris', 'SOFIA', 'Jueves', 'SofiaOsiris@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(29, 16111747, 'Jorge', 'CLIMIS', 'Jueves', 'jclimis@unpa.edu.ar', 'Exclusiva', 'Ayudante', 1),
(30, 31978715, 'Jose Luis', 'SAENZ', 'Jueves', 'Jsaenz@unpa.edu.ar', 'Simple', 'Asociado', 1),
(31, 28614026, 'Eder', 'REBOUCAS DOS SANTOS', 'Miercoles', 'EderDosSantos@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(32, 20450453, 'Walter Raul', 'ALTAMIRANO', 'Miercoles', 'waltamirano@unpa.edu.ar', 'Exclusiva', 'Titular', 1),
(33, 22509721, 'Federico jorge', 'LAJE', 'Martes', 'flaje@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(34, 25004450, 'Pedro ignacio', 'CORNEJO', 'Lunes', 'pcornejo@unpa.edu.ar', 'Parcial', 'Adjunto', 2),
(35, 25004451, 'Pedro', 'DODMAN', 'Miercoles', 'pdodman@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(36, 29088186, 'Maria pilar', 'MELANO', 'Viernes', 'mmelano@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(37, 22709221, 'Emilio vicente', 'RESTIVO', 'Jueves', 'erestivo@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(38, 28156167, 'Loreli', 'STETTLER', 'Lunes', 'lstettler@unpa.edu.ar', 'Parcial', 'Adjunto', 2),
(39, 17734099, 'Patricia del carmen', 'ZAPATA', 'Jueves', 'pzapata@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(40, 27098934, 'Patricia beatriz', 'VEGA', 'Martes', 'pvega@unpa.edu.ar', 'Parcial', 'Adjunto', 2),
(41, 23616141, 'Alejandra lorena', 'COSTANTINI', 'Martes', 'acostantini@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(42, 20407136, 'Ana claudia', 'TABARES', 'Miercoles', 'atabares@unpa.edu.ar', 'Parcial', 'Asociado', 2),
(43, 24861220, 'Susana mabel', 'BAHAMONDE', 'Jueves', 'sbahamonde@unpa.edu.ar', 'Parcial', 'Asociado', 2),
(44, 16419128, 'Monica beatriz', 'MUSCI', 'Jueves', 'mmusci@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(45, 17801725, 'Marcela monica', 'ARPES', 'Viernes', 'marpes@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(46, 30968350, 'Nicolas', 'ALBRIEU', 'Miercoles', 'nalbrieu@unpa.edu.ar', 'Simple', 'Asociado', 2),
(47, 30968351, 'Claudia', 'FIGUEROA ', 'Miercoles', 'cfigueroa @unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(48, 30968352, 'Fernando', 'MONTERO', 'Jueves', 'fmontero@unpa.edu.ar', 'Simple', 'Asociado', 2),
(49, 23626041, 'Nestor horacio', 'BORQUEZ', 'Miercoles', 'nborquez@unpa.edu.ar', 'Exclusiva', 'Titular', 2),
(50, 31440169, 'Hernan', 'GIMENEZ', 'Lunes', 'hgimenez@unpa.edu.ar', 'Parcial', 'Adjunto', 2),
(51, 20237624, 'Miriam liliana', 'VAZQUEZ', 'Martes', 'mvazquez@unpa.edu.ar', 'Simple', 'Asociado', 2),
(52, 24861524, 'Sebastian ariel', 'RUIZ', 'Lunes', 'sruiz@unpa.edu.ar', 'Parcial', 'Asociado', 2),
(53, 24225561, 'Maria Angelica', 'ZUÑIGA', 'Lunes', 'MZUÑIGA@UNPA.EDU.AR', 'Exclusiva', 'Titular', 2),
(54, 32122793, 'Ivan', 'PEZZINI ', 'Martes', 'IPEZZINI@UNPA.EDU.AR', 'Parcial', 'Adjunto', 2),
(55, 26351877, 'Marcela Alejandra', 'CONSTANZO', 'Jueves', 'MCONSTANZO@UNPA.EDU.AR', 'Simple', 'Asociado', 2),
(56, 29058990, 'Roberto Adrian', 'FARIAS', 'Viernes', 'RFARIAS@UNPA.EDU.AR', 'Parcial', 'Adjunto', 2),
(57, 30502856, 'Romina', 'BEHRENS', 'Miercoles', 'RBEHRENS@UNPA.EDU.AR', 'Exclusiva', 'Titular', 2),
(58, 24357679, 'Cristian Leonardo', 'BESSONE', 'Martes', 'CBESSONE@UNPA.EDU.AR', 'Simple', 'Asociado', 2),
(59, 28156155, 'Andrea Viviana', 'ALVARADO', 'Jueves', 'AALVARADO@UNPA.EDU.AR', 'Parcial', 'Adjunto', 2),
(60, 28156156, 'Adrian', 'AGÜERO', 'Viernes', 'AAGÜERO@UNPA.EDU.AR', 'Simple', 'Asociado', 2),
(61, 26287837, 'Victoria Laura', 'HAMMAR', 'Lunes', 'VHAMMAR@UNPA.EDU.AR', 'Exclusiva', 'Titular', 2),
(62, 26287838, 'Alejandro', 'VICTORIA', 'Martes', 'AVICTORIA@UNPA.EDU.AR', 'Parcial', 'Adjunto', 2),
(63, 28342661, 'Ricardo Santiago', 'PUCA MOLINA', 'Jueves', 'RPUCA@UNPA.EDU.AR', 'Simple', 'Asociado', 2),
(64, 12995059, 'Luis Rafael', 'DALLA COSTA ', 'Viernes', 'LDALLA@UNPA.EDU.AR', 'Parcial', 'Adjunto', 2),
(65, 35567708, 'Andrea Giselle', 'FERNANDEZ', 'Miercoles', 'AFERNANDEZ@UNPA.EDU.AR', 'Exclusiva', 'Titular', 2),
(66, 23130400, 'Christian Alejandro', 'BRITOS', 'Lunes', 'CBRITOS@UNPA.EDU.AR', 'Simple', 'Asociado', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`) VALUES
(7, 'Usuario Comun'),
(8, 'Administrador'),
(9, 'Secretaria Academica'),
(10, 'Personal Academico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_permiso`
--

DROP TABLE IF EXISTS `rol_permiso`;
CREATE TABLE `rol_permiso` (
  `id_rol` int(11) NOT NULL,
  `id_permiso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol_permiso`
--

INSERT INTO `rol_permiso` (`id_rol`, `id_permiso`) VALUES
(7, 7),
(7, 11),
(8, 1),
(8, 2),
(8, 3),
(8, 4),
(8, 5),
(8, 7),
(8, 8),
(8, 9),
(8, 11),
(9, 1),
(9, 2),
(9, 4),
(9, 11),
(9, 12),
(10, 3),
(10, 5),
(10, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_licencia`
--

DROP TABLE IF EXISTS `tipo_licencia`;
CREATE TABLE `tipo_licencia` (
  `nombre` varchar(60) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `idLicencia` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_licencia`
--

INSERT INTO `tipo_licencia` (`nombre`, `descripcion`, `idLicencia`) VALUES
('Medica y Accidentes', 'Covid-19', 1),
('Medica y Accidentes', 'Gripe', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tribunal`
--

DROP TABLE IF EXISTS `tribunal`;
CREATE TABLE `tribunal` (
  `id` int(10) UNSIGNED NOT NULL,
  `presidente` int(10) NOT NULL,
  `vocal` int(10) NOT NULL,
  `vocal1` int(10) DEFAULT NULL,
  `suplente` int(10) DEFAULT NULL,
  `preferenciapresidente` varchar(60) DEFAULT NULL,
  `preferenciavocal` varchar(60) DEFAULT NULL,
  `preferenciavocal1` varchar(60) DEFAULT NULL,
  `preferenciasuplente` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tribunal`
--

INSERT INTO `tribunal` (`id`, `presidente`, `vocal`, `vocal1`, `suplente`, `preferenciapresidente`, `preferenciavocal`, `preferenciavocal1`, `preferenciasuplente`) VALUES
(24, 5, 6, 7, NULL, 'Jueves', 'Viernes', 'Jueves', NULL),
(26, 11, 12, 13, 14, 'Martes', 'Martes', 'Miercoles', 'Viernes'),
(27, 15, 16, 17, 18, 'Martes', 'Jueves', 'Viernes', 'Jueves'),
(28, 5, 19, 19, NULL, 'Jueves', 'Viernes', 'Viernes', NULL),
(29, 20, 21, 22, NULL, 'Miercoles', 'Jueves', 'Viernes', NULL),
(30, 23, 24, 25, NULL, 'Miercoles', 'Martes', 'Lunes', NULL),
(31, 22, 15, 26, NULL, 'Viernes', 'Martes', 'Viernes', NULL),
(32, 28, 27, 29, NULL, 'Jueves', 'Jueves', 'Jueves', NULL),
(33, 9, 30, 10, NULL, 'Jueves', 'Jueves', 'Jueves', NULL),
(34, 28, 29, 6, NULL, 'Jueves', 'Jueves', 'Viernes', NULL),
(35, 31, 32, 28, NULL, 'Miercoles', 'Miercoles', 'Jueves', NULL),
(36, 28, 19, 5, NULL, 'Jueves', 'Viernes', 'Jueves', NULL),
(37, 33, 34, NULL, NULL, 'Martes', 'Lunes', NULL, NULL),
(38, 35, 36, NULL, NULL, 'Miercoles', 'Viernes', NULL, NULL),
(39, 37, 38, NULL, NULL, 'Jueves', 'Lunes', NULL, NULL),
(40, 39, 40, NULL, NULL, 'Jueves', 'Martes', NULL, NULL),
(41, 41, 42, NULL, NULL, 'Martes', 'Miercoles', NULL, NULL),
(42, 36, 41, NULL, NULL, 'Viernes', 'Martes', NULL, NULL),
(43, 44, 43, NULL, NULL, 'Jueves', 'Jueves', NULL, NULL),
(44, 44, 43, NULL, NULL, 'Jueves', 'Jueves', NULL, NULL),
(45, 36, 43, NULL, NULL, 'Viernes', 'Jueves', NULL, NULL),
(46, 45, 46, NULL, NULL, 'Viernes', 'Miercoles', NULL, NULL),
(47, 47, 48, NULL, NULL, 'Miercoles', 'Jueves', NULL, NULL),
(48, 49, 50, NULL, NULL, 'Miercoles', 'Lunes', NULL, NULL),
(50, 53, 54, 55, 56, 'Lunes', 'Martes', 'Jueves', 'Viernes'),
(51, 57, 58, 59, 60, 'Miercoles', 'Martes', 'Jueves', 'Viernes'),
(52, 61, 62, 63, NULL, 'Lunes', 'Martes', 'Jueves', NULL),
(53, 57, 64, 63, NULL, 'Miercoles', 'Viernes', 'Jueves', NULL),
(54, 65, 63, 66, NULL, 'Miercoles', 'Jueves', 'Lunes', NULL),
(55, 65, 63, 66, NULL, 'Miercoles', 'Jueves', 'Lunes', NULL),
(57, 5, 27, 28, NULL, 'Jueves', 'Viernes', 'Jueves', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tribunalasignatura`
--

DROP TABLE IF EXISTS `tribunalasignatura`;
CREATE TABLE `tribunalasignatura` (
  `id` int(10) UNSIGNED NOT NULL,
  `idTribunal` int(11) NOT NULL,
  `idAsignatura` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tribunalasignatura`
--

INSERT INTO `tribunalasignatura` (`id`, `idTribunal`, `idAsignatura`) VALUES
(13, 24, 35),
(15, 26, 37),
(16, 27, 38),
(17, 28, 39),
(18, 29, 40),
(19, 30, 41),
(20, 31, 42),
(21, 32, 43),
(22, 33, 13),
(23, 34, 11),
(24, 35, 6),
(25, 36, 12),
(26, 37, 44),
(27, 38, 45),
(28, 39, 46),
(29, 40, 47),
(30, 41, 48),
(31, 42, 49),
(32, 43, 50),
(33, 44, 51),
(34, 45, 52),
(35, 46, 53),
(36, 47, 54),
(37, 48, 55),
(38, 50, 56),
(39, 51, 57),
(40, 52, 58),
(41, 53, 59),
(42, 54, 60),
(43, 55, 61),
(45, 57, 63);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

DROP TABLE IF EXISTS `turno`;
CREATE TABLE `turno` (
  `idTurno` int(11) NOT NULL,
  `fechainicio1` date NOT NULL,
  `fechafin1` date NOT NULL,
  `periodollamado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `turno`
--

INSERT INTO `turno` (`idTurno`, `fechainicio1`, `fechafin1`, `periodollamado`) VALUES
(1, '2022-02-14', '2022-02-18', 1),
(2, '2022-03-02', '2022-03-08', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno_mesaexamen`
--

DROP TABLE IF EXISTS `turno_mesaexamen`;
CREATE TABLE `turno_mesaexamen` (
  `id` int(11) NOT NULL,
  `idTurno` int(11) NOT NULL,
  `idMesa` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `codAula` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `turno_mesaexamen`
--

INSERT INTO `turno_mesaexamen` (`id`, `idTurno`, `idMesa`, `fecha`, `hora`, `codAula`) VALUES
(2145, 1, 1247, '2022-02-14', '00:00:00', NULL),
(2146, 1, 1248, '2022-02-14', '00:00:00', NULL),
(2148, 1, 1250, '2022-02-15', '00:00:00', NULL),
(2149, 1, 1251, '2022-02-15', '00:00:00', NULL),
(2150, 1, 1252, '2022-02-15', '00:00:00', NULL),
(2151, 1, 1253, '2022-02-15', '00:00:00', NULL),
(2155, 1, 1257, '2022-02-16', '00:00:00', NULL),
(2156, 1, 1258, '2022-02-16', '00:00:00', NULL),
(2157, 1, 1259, '2022-02-16', '00:00:00', NULL),
(2158, 1, 1260, '2022-02-16', '00:00:00', NULL),
(2159, 1, 1261, '2022-02-16', '00:00:00', NULL),
(2160, 1, 1262, '2022-02-16', '00:00:00', NULL),
(2161, 1, 1263, '2022-02-16', '00:00:00', NULL),
(2162, 1, 1264, '2022-02-16', '00:00:00', NULL),
(2163, 1, 1265, '2022-02-16', '00:00:00', NULL),
(2164, 1, 1266, '2022-02-16', '00:00:00', NULL),
(2170, 1, 1272, '2022-02-17', '00:00:00', NULL),
(2171, 1, 1273, '2022-02-17', '00:00:00', NULL),
(2172, 1, 1274, '2022-02-17', '00:00:00', NULL),
(2173, 1, 1275, '2022-02-17', '00:00:00', NULL),
(2174, 1, 1276, '2022-02-17', '00:00:00', NULL),
(2175, 1, 1277, '2022-02-17', '00:00:00', NULL),
(2176, 1, 1278, '2022-02-17', '00:00:00', NULL),
(2177, 1, 1279, '2022-02-17', '00:00:00', NULL),
(2178, 1, 1280, '2022-02-17', '00:00:00', NULL),
(2179, 1, 1281, '2022-02-17', '00:00:00', NULL),
(2185, 1, 1287, '2022-02-18', '00:00:00', NULL),
(2186, 1, 1288, '2022-02-18', '00:00:00', NULL),
(2187, 1, 1289, '2022-02-18', '00:00:00', NULL),
(2188, 1, 1290, '2022-02-18', '00:00:00', NULL),
(2192, 2, 1257, '2022-03-02', '00:00:00', NULL),
(2193, 2, 1258, '2022-03-02', '00:00:00', NULL),
(2194, 2, 1259, '2022-03-02', '00:00:00', NULL),
(2195, 2, 1260, '2022-03-02', '00:00:00', NULL),
(2196, 2, 1261, '2022-03-02', '00:00:00', NULL),
(2197, 2, 1262, '2022-03-02', '00:00:00', NULL),
(2198, 2, 1263, '2022-03-02', '00:00:00', NULL),
(2199, 2, 1264, '2022-03-02', '00:00:00', NULL),
(2200, 2, 1265, '2022-03-02', '00:00:00', NULL),
(2201, 2, 1266, '2022-03-02', '00:00:00', NULL),
(2207, 2, 1272, '2022-03-03', '00:00:00', NULL),
(2208, 2, 1273, '2022-03-03', '00:00:00', NULL),
(2209, 2, 1274, '2022-03-03', '00:00:00', NULL),
(2210, 2, 1275, '2022-03-03', '00:00:00', NULL),
(2211, 2, 1276, '2022-03-03', '00:00:00', NULL),
(2212, 2, 1277, '2022-03-03', '00:00:00', NULL),
(2213, 2, 1278, '2022-03-03', '00:00:00', NULL),
(2214, 2, 1279, '2022-03-03', '00:00:00', NULL),
(2215, 2, 1280, '2022-03-03', '00:00:00', NULL),
(2216, 2, 1281, '2022-03-03', '00:00:00', NULL),
(2222, 2, 1287, '2022-03-04', '00:00:00', NULL),
(2223, 2, 1288, '2022-03-04', '00:00:00', NULL),
(2224, 2, 1289, '2022-03-04', '00:00:00', NULL),
(2225, 2, 1290, '2022-03-04', '00:00:00', NULL),
(2229, 2, 1247, '2022-03-07', '00:00:00', NULL),
(2230, 2, 1248, '2022-03-07', '00:00:00', NULL),
(2232, 2, 1250, '2022-03-08', '00:00:00', NULL),
(2233, 2, 1251, '2022-03-08', '00:00:00', NULL),
(2234, 2, 1252, '2022-03-08', '00:00:00', NULL),
(2235, 2, 1253, '2022-03-08', '00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `email`) VALUES
(24, 'emiliano Aguero', 'emilianoaguero2@gmail.com'),
(23, 'Eder dos Santos', 'esantos@uarg.unpa.edu.ar'),
(26, 'Personal Academico', 'personalacademicogef@gmail.com'),
(25, 'Secretaria Academica', 'secretariaacademicagef@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_rol`
--

DROP TABLE IF EXISTS `usuario_rol`;
CREATE TABLE `usuario_rol` (
  `id_usuario` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_rol`
--

INSERT INTO `usuario_rol` (`id_usuario`, `id_rol`) VALUES
(23, 8),
(24, 8),
(25, 9),
(26, 10);

-- --------------------------------------------------------

--
-- Estructura para la vista `new_view`
--
DROP TABLE IF EXISTS `new_view`;

DROP VIEW IF EXISTS `new_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `new_view`  AS  select `m`.`codMesa` AS `idMesaExamen`,`c`.`codCarrera` AS `codigoCarrera`,`c`.`nombre` AS `nombreLargoCarrera`,`a`.`codAsignatura` AS `idAsignatura`,`a`.`nombre` AS `nombreLargoAsignatura`,`au`.`sector` AS `sectorAula`,`au`.`nombre` AS `nombreAula`,`tm`.`fecha` AS `fecha`,`tm`.`hora` AS `hora`,`n`.`fechaNueva` AS `fechaEdicion`,`n`.`horaNueva` AS `horaEdicion`,`n`.`idTurno` AS `turnoEdicion`,`tm`.`idTurno` AS `turno`,concat_ws(' ',`presidente`.`nombre`,`presidente`.`apellido`) AS `nombrePresidente`,concat_ws(' ',`vocal`.`nombre`,`vocal`.`apellido`) AS `nombreVocalPrimero`,concat_ws(' ',`vocal1`.`nombre`,`vocal1`.`apellido`) AS `nombreVocalSegundo`,concat_ws(' ',`suplente`.`nombre`,`suplente`.`apellido`) AS `nombreSuplente` from ((((((((((`turno_mesaexamen` `tm` join `mesa_examen` `m` on(`tm`.`idMesa` = `m`.`codMesa`)) join `asignatura` `a` on(`a`.`codAsignatura` = `m`.`codAsignatura`)) join `carrera` `c` on(`c`.`codCarrera` = `a`.`idCarrera`)) left join `aula` `au` on(`au`.`codAula` = `tm`.`codAula`)) left join `novedad` `n` on(`n`.`idTurno` = `tm`.`id`)) join `tribunal` `t` on(`t`.`id` = `m`.`idTribunal`)) left join `profesor` `presidente` on(`presidente`.`id` = `t`.`presidente`)) left join `profesor` `vocal` on(`vocal`.`id` = `t`.`vocal`)) left join `profesor` `vocal1` on(`vocal1`.`id` = `t`.`vocal1`)) left join `profesor` `suplente` on(`suplente`.`id` = `t`.`suplente`)) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asignatura`
--
ALTER TABLE `asignatura`
  ADD PRIMARY KEY (`codAsignatura`),
  ADD KEY `fk_ASIGNATURA_PROFESOR_idx` (`idProfesor`);

--
-- Indices de la tabla `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`codAula`),
  ADD UNIQUE KEY `idtable1_UNIQUE` (`codAula`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carrera`
--
ALTER TABLE `carrera`
  ADD PRIMARY KEY (`codCarrera`);

--
-- Indices de la tabla `correlativad`
--
ALTER TABLE `correlativad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_CORRELATIVAD_ASIGNATURA1_idx` (`codAsignatura`),
  ADD KEY `fk_CORRELATIVAD_ASIGNATURA2_idx` (`codAsignatura_Correlatividad`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`);

--
-- Indices de la tabla `fecha`
--
ALTER TABLE `fecha`
  ADD KEY `fk_FECHA_LLAMADO_idx` (`idLlamado`);

--
-- Indices de la tabla `licencia`
--
ALTER TABLE `licencia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_LICENCIA_PROFESOR_idx` (`idProfesor`);

--
-- Indices de la tabla `llamado_mesa_examen`
--
ALTER TABLE `llamado_mesa_examen`
  ADD KEY `fk_LLAMADO_has_MESA_EXAMEN_MESA_EXAMEN1_idx` (`codMesa`),
  ADD KEY `fk_LLAMADO_has_MESA_EXAMEN_LLAMADO1_idx` (`idLlamado`);

--
-- Indices de la tabla `mesa_examen`
--
ALTER TABLE `mesa_examen`
  ADD PRIMARY KEY (`codMesa`),
  ADD KEY `fk_MESA_EXAMEN_TRIBUNAL_idx` (`idTribunal`),
  ADD KEY `fk_MESA_EXAMEN_ASIGNATURA_idx` (`codAsignatura`);

--
-- Indices de la tabla `mesa_examen_carrera`
--
ALTER TABLE `mesa_examen_carrera`
  ADD KEY `fk_MESA_EXAMEN_has_CARRERA_CARRERA1_idx` (`codCarrera`),
  ADD KEY `fk_MESA_EXAMEN_has_CARRERA_MESA_EXAMEN1_idx` (`codMesa`);

--
-- Indices de la tabla `novedad`
--
ALTER TABLE `novedad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_idTurno_id` (`idTurno`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ID_PERMISO_IND` (`id`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni_UNIQUE` (`dni`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_PROFESOR_DEPARTAMENTO_idx` (`idDepartamento`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ID_ROL_IND` (`id`);

--
-- Indices de la tabla `rol_permiso`
--
ALTER TABLE `rol_permiso`
  ADD PRIMARY KEY (`id_rol`,`id_permiso`),
  ADD UNIQUE KEY `ID_ROL_PERMISO_IND` (`id_permiso`,`id_rol`),
  ADD KEY `FKASO_ROL_IND` (`id_rol`),
  ADD KEY `FKASO_PER_idx` (`id_permiso`);

--
-- Indices de la tabla `tribunal`
--
ALTER TABLE `tribunal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_TRIBUNAL_PROFESOR1_idx` (`presidente`),
  ADD KEY `fk_TRIBUNAL_PROFESOR2_idx` (`vocal`),
  ADD KEY `fk_TRIBUNAL_PROFESOR3_idx` (`vocal1`),
  ADD KEY `fk_TRIBUNAL_PROFESOR4_idx` (`suplente`);

--
-- Indices de la tabla `tribunalasignatura`
--
ALTER TABLE `tribunalasignatura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `turno`
--
ALTER TABLE `turno`
  ADD PRIMARY KEY (`idTurno`);

--
-- Indices de la tabla `turno_mesaexamen`
--
ALTER TABLE `turno_mesaexamen`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UN_USUARIO` (`email`,`nombre`),
  ADD UNIQUE KEY `ID_USUARIO_IND` (`id`);

--
-- Indices de la tabla `usuario_rol`
--
ALTER TABLE `usuario_rol`
  ADD PRIMARY KEY (`id_usuario`,`id_rol`),
  ADD UNIQUE KEY `ID_USUARIO_ROL_IND` (`id_rol`,`id_usuario`),
  ADD KEY `FKVIN_USU_IND` (`id_usuario`),
  ADD KEY `FKVIN_ROL_idx` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asignatura`
--
ALTER TABLE `asignatura`
  MODIFY `codAsignatura` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de la tabla `aula`
--
ALTER TABLE `aula`
  MODIFY `codAula` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `correlativad`
--
ALTER TABLE `correlativad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `licencia`
--
ALTER TABLE `licencia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `mesa_examen`
--
ALTER TABLE `mesa_examen`
  MODIFY `codMesa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1294;

--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT de la tabla `tribunal`
--
ALTER TABLE `tribunal`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `tribunalasignatura`
--
ALTER TABLE `tribunalasignatura`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `turno`
--
ALTER TABLE `turno`
  MODIFY `idTurno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `turno_mesaexamen`
--
ALTER TABLE `turno_mesaexamen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2239;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `novedad`
--
ALTER TABLE `novedad`
  ADD CONSTRAINT `FK_idTurno_id` FOREIGN KEY (`idTurno`) REFERENCES `turno_mesaexamen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
