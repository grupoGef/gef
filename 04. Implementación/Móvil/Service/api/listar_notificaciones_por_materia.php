<?php

header("Access-Control-Allow-Origin: *");

include_once '../config/inc_config.php';
include_once '../app/principal/modelo/Encriptador.php';
include_once './MySQL.php';

$idCarrera = strip_tags($_REQUEST['idCarrera']);
//$idAsignatura = strip_tags($_REQUEST['idAsignatura']);
//$docente = strip_tags($_REQUEST['docente']);


// $idCarrera = 16;
// $idAsignatura = 1;//2
// $docente = "Emiliano";

if ($idCarrera) {
    $instancia = MySQL::getInstancia();

    $query = "SELECT "
        . " idMesaExamen,"
        . " codigoCarrera,"
        . " nombreLargoCarrera,"
        . " idAsignatura,"
        . " nombreLargoAsignatura,"
        . " nombrePresidente,"
        . " nombreVocalPrimero,"
        . " nombreVocalSegundo,"
        . " nombreSuplente,"
        . " DATE_FORMAT(fechaPrimerLlamado, '%d/%m/%Y') fechaPrimerLlamado,"
        . " TIME_FORMAT(horaPrimerLlamado, '%H:%i') horaPrimerLlamado,"
        . " (CASE WHEN sectorAulaPrimerLlamado IS NULL THEN 'Sin asignar' ELSE CONCAT(sectorAulaPrimerLlamado,' ',nombreAulaPrimerLlamado) END) aulaPrimerLlamado,"
        . " DATE_FORMAT(fechaSegundoLlamado, '%d/%m/%Y') fechaSegundoLlamado,"
        . " TIME_FORMAT(horaSegundoLlamado, '%H:%i') horaSegundoLlamado,"
        . " (CASE WHEN sectorAulaSegundoLlamado IS NULL THEN 'Sin asignar' ELSE CONCAT(sectorAulaSegundoLlamado,' ',nombreAulaSegundoLlamado) END) aulaSegundoLlamado,"
        . " false favorito"
        . " FROM vw_mesa_examen "
        . " WHERE codigoCarrera = {$idCarrera}  ";
    $query .= " ORDER BY nombreLargoAsignatura ";

    //$query = "SELECT * FROM asignatura";

    $resultado = $instancia->query($query);
    if ($resultado) {
        if ($resultado->num_rows > 0) {
            $carreras = $resultado->fetch_all(MYSQLI_ASSOC);
            $response = array('estado' => 'OK', 'mesas' => $carreras);
        } else {
            $response = array('estado' => 'BAD', 'datos' => 'No se encontraron mesas de examen cargadas');
        }
    } else {
        $response = array('estado' => 'BAD', 'datos' => $idAsignatura);
    }
} else {
    $response = array('estado' => 'BAD', 'datos' => 'No se indicó alguno de los datos obligatorios');
}

echo json_encode($response);



