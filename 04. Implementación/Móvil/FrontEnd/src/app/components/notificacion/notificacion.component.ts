import {Component, Input, OnInit} from '@angular/core';
import {Mesa} from '../../interfaces/interfaces';
import {ActionSheetController} from '@ionic/angular';
import {DataLocalService} from '../../services/data-local.service';
import {ExamenesService} from '../../services/examenes.service';

@Component({
  selector: 'app-notificacion',
  templateUrl: './notificacion.component.html',
  styleUrls: ['./notificacion.component.scss'],
})
export class NotificacionComponent implements OnInit {

  @Input() mesa: Mesa;
  @Input() enFavoritos;

  mesa2: Mesa;
  mesas: Mesa[] = [];

  constructor(private actionSheetCtrl: ActionSheetController,
              private datalocalService: DataLocalService,
              private examenesService: ExamenesService) {
  }

  ngOnInit() {
    this.actualizarFavoritos();
  }

 async  actualizarFavoritos(){
     const body: string = "&idMesaExamen=" + this.mesa.idMesaExamen;
      this.examenesService.actualizarFavorito(body)
       .subscribe( resp => {
         this.datalocalService.actualizarFavoritos(resp.mesas);
         },
       );
  }
}
