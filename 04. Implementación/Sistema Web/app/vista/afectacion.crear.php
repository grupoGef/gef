<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARAFECTACION);
include_once '../modelo/ColeccionDocentes.php';
include_once '../modelo/ColeccionLicencia.php';
include_once '../modelo/ColeccionTipoLicencia.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2-bootstrap.min.css">
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/select2.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/i18n/es.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Crear Afectacion</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="afectacion.crear.procesar.php" method="post">
               
                <div class="card">
                <div class="card-header">
                    <p><center><h3>Agregar nueva Afectación</h3></center></p>
                        <p>
                            Por favor, complete los campos a continuaci&oacute;n.
                            Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                            Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                        </p>
                    </div>
                         
                    <div class="card-body">
                    <h4>Datos de la Afectación</h4>
                        <br>
                        <label for="inputNombre">Asignaturas</label>
                        <div class="form-group">
                        <select required='' class="asignatura form-control" name="asignatura">
                        </select>
                        </div>

                        <label for="inputPresidente">Presidente</label>
                        <div class="form-group">
                        <select required='' class="js-example-basic-single form-control" name="presidente">
                         </select>
                        </div>

                        <label for="inputPresidentepref">Preferencias de Presidente</label>
                        <div class="form-group">
                        <select  class="form-control " name="preferenciapresidente" >
                            <option value="">Día de preferencia...</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miércoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>
                          </select>
                        </div>

                       <label for="inputVocal">Vocal</label>
                        <div class="form-group">
                        <select required='' class="js-example-basic-single form-control" name="vocal" onselect=validatevocal()>
                         </select>
                        </div>
                       
                        <label for="inputPresidente">Preferencias de Vocal</label>
                        <div class="form-group">
                        <select  class="form-control " name="preferenciavocal" >
                            <option value="">Dia de preferencia...</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miércoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>
                          </select>
                        </div>



                        <label for="inputSegundoVocal">Segundo Vocal</label>
                        <div class="form-group">
                        <select required='' class="js-example-basic-single form-control" name="vocal1">
                         </select>
                        </div>
                      
                         <label for="inputPresidente">Preferencias de Segundo Vocal</label>
                        <div class="form-group">
                        <select  class="form-control " name="preferenciavocal1" >
                            <option value="">Día de preferencia...</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miércoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>
                          </select>
                          </div>


                       <label for="inputSuplente">Suplente</label>
                        <div class="form-group">
                        <select class="js-example-basic-single form-control" name="suplente">
                         </select>
                        </div>
                      
                         <label for="inputPresidente">Preferencias del Suplente</label>
                        <div class="form-group">
                        <select  class="form-control " name="preferenciasuplente" >
                            <option value="">Día de preferencia...</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miércoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>
                          </select>  
                        </div>



                    </div>
                   
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <span class="oi oi-check"></span> Confirmar
                        </button>
                        <a href="gestionarAfectacion.php">
                            <button type="button" class="btn btn-outline-danger">
                                <span class="oi oi-x"></span> Cancelar
                            </button>
                        </a>
                    </div>
                </div>
            </form>

</div>
   
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
<script>
     $.fn.select2.defaults.set( "theme", "bootstrap" );
       $.fn.select2.defaults.set('language', 'es');
       $('.asignatura').select2({
           placeholder: 'Ingrese la Asignatura',
             language: {
noResults: function() {

 return "No hay resultados";        
},
searching: function() {

 return "Buscando..";
}
},
ajax: {
                    url: 'buscarasignatura.php',
                    dataType: 'json',
                    delay: 250,
                    data: function (data) {
                       
                   return { 
                       query1: data.term // search term
                   };
               },
                   processResults: function (data) {
                       console.log(data)
                       return {
                             results: data
                              };
                   },
                     cache: true
                   }


       });


       $('.js-example-basic-single').select2({
             placeholder: 'Ingrese el Profesor',
             language: {
noResults: function() {

 return "No hay resultados";        
},
searching: function() {

 return "Buscando..";
}
},
             ajax: {
                    url: 'buscarnombreprofesor.php',
                    dataType: 'json',
                    delay: 250,
                    data: function (data) {
                   return { 
                       query: data.term // search term
                   };
               },
                   processResults: function (data) {
                  
                       return {
                             results: data
                              };
                   },
                     cache: true
                   }
       });
      




    $(document.getElementById('#buscarapresidente')).ready(function(){
        $('#buscarpresidente').keyup(function(){
            var query=$(this).val();
            if(query !='')
            {
                console.log(query);
                $.ajax({
                    url:"buscarnombreprofesortribunal.php",
                    method:"POST",
                    data:{query:query},
                    success:function(data)
                    {
                       
                        console.log(data);
                        $('#listapresidente').fadeIn();
                        $('#listapresidente').html(data);
                    }

                });
            }
            });
         $('#listapresidente').on('click', 'li', function(){  
           $('#buscarpresidente').val($(this).text());  
           $('#listapresidente').fadeOut();  
           }); 
        });
        


</script>
<script>
    $(document.getElementById('#buscarvocal')).ready(function(){
        $('#buscarvocal').keyup(function(){
            var query=$(this).val();
            if(query !='')
            {
                $.ajax({
                    url:"buscarnombreprofesor.php",
                    method:"POST",
                    data:{query:query},
                    success:function(data)
                    {

                        $('#listavocal').fadeIn();
                        $('#listavocal').html(data);
                    }

                });
            }
            });
         $('#listavocal').on('click', 'li', function(){  
           $('#buscarvocal').val($(this).text());  
           $('#listavocal').fadeOut();  
           }); 
        });
        


</script>





<script>  
function validatevocal() {
    if(document.getElementById('buscarvocal').value == document.getElementById('buscarpresidente').value || document.getElementById('buscarvocal').value == document.getElementById( 'buscarvocal1') || document.getElementById('buscarvocal').value == document.getElementById('buscarsuplente').value ) 
        document.getElementById('buscarvocal').setCustomValidity('El nombre del vocal coincide con otro miembro ');
    else 
        document.getElementById('buscarvocal').style.borderColor="green";
        document.getElementById('buscarvocal').setCustomValidity('');
}
</script>
<script>  
function validatevocal1() {
    if(document.getElementById('buscarvocal1').value == document.getElementById('buscarpresidente').value || document.getElementById('buscarvocal1').value == document.getElementById( 'buscarvocal') || document.getElementById('buscarvocal1').value == document.getElementById('buscarsuplente').value ) 
        document.getElementById('buscarvocal1').setCustomValidity('El nombre del vocal coincide con otro miembro ');
    else 
        document.getElementById('buscarvocal1').setCustomValidity('');
}
</script>
<script>  
function validatesuplente() {
    if(document.getElementById('buscarsuplente').value == document.getElementById('buscarpresidente').value || document.getElementById('buscarsuplente').value == document.getElementById( 'buscarvocal') || document.getElementById('buscarsuplente').value == document.getElementById('buscarvocal1').value ) 
        document.getElementById('buscarsuplente').setCustomValidity('El nombre del suplente coincide con otro miembro ');
    else 
        document.getElementById('buscarsuplente').setCustomValidity('');
}
</script>