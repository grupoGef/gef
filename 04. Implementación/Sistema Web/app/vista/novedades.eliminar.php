<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARNOVEDADES);


$idlicencia=$_GET["id"];
$idtipolicencia=$_GET["id1"];
$iddocente=$_GET["id2"];

$query="Select p.id,p.nombre,p.apellido,d.nombre as departamento,d.id as iddepartamento,l.id as idlicencia,l.fechaInicio,l.fechaFinal, tl.idLicencia as tipolicenciaid,tl.nombre as tipolicencianombre, tl.descripcion FROM licencia l left join profesor p on l.idProfesor=p.id left JOIN departamento d on p.idDepartamento=d.id left join tipo_licencia tl ON l.id=tl.idLicencia WHERE 
p.id=$iddocente and l.id=$idlicencia and tl.idLicencia  = $idtipolicencia";


$reportebuscar= BDConexion::getInstancia()->query($query);



if ($reportebuscar->num_rows != 0)
{
   
    while($row = $reportebuscar->fetch_array())
    {
        $docente=$row['nombre'];
        $docentea=$row['apellido'];
        $departamento=$row['departamento'];
        $nombrelicencia=$row['tipolicencianombre'];
        $fecha1 = new DateTime($datetimecomparable= $row['fechaInicio']);
        $fecha_d_m_y1 = $fecha1->format('Y-m-d');
        $fecha2 = new DateTime($datetimecomparable= $row['fechaFinal']);
        $fecha_d_m_y2 = $fecha2->format('Y-m-d'); 
        $licenciadesc=$row['descripcion'];
        


    }

}

?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Eliminar Novedad</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="novedad.eliminar.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                        <h3>Eliminar Licencia</h3>
                    </div>
                    <div class="card-body">
                        <p class="alert alert-warning ">
                            <span class="oi oi-warning"></span> ATENCI&Oacute;N. Esta operaci&oacute;n no puede deshacerse.
                        </p>
                        <p>¿Est&aacute; seguro que desea eliminar la licencia de  <b><?= $docente ?></b> <b><?= $docentea ?></b>?

                    </div>
                    <input type="hidden" name="id" class="form-control" id="id" value="<?= $idlicencia ?>">
                    <input type="hidden" name="id1" class="form-control" id="id1" value="<?= $idtipolicencia ?>">
                    <input type="hidden" name="id2" class="form-control" id="id2" value="<?=$iddocente ?>">
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <span class="oi oi-check"></span> Sí, deseo eliminar
                        </button>
                        <a href="Novedades.php">
                            <button type="button" class="btn btn-outline-danger">
                                <span class="oi oi-x"></span> NO (Salir de esta pantalla)
                            </button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
