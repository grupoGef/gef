<?php  
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARAFECTACION);
include_once '../modelo/ColeccionDocentes.php';
include_once  '../modelo/ColeccionMesaExamen.php';
include_once  '../modelo/ColeccionTribunal.php';
include_once  '../modelo/ColeccionAsignaturas.php';
include_once  '../modelo/ColeccionTribunalAsignatura.php';


//Se reciben los datos del post

if (isset($_POST["docente"]))
{
    $iddocente = $_POST["docente"];
}
if (isset($_POST["asignatura"]))
{
    $nombreasignatura = $_POST["asignatura"];
}







$espacio = ' ';
$sql='';
$cuerpodiv;
if(empty($iddocente) && empty($nombreasignatura))
{

    $cuerpodiv='<div id="param" class="alert alert-warning" role="alert">No se encontraron resultados con los filtros ingresados. <strong>Por favor, intente nuevamente </strong></div>';

}
else
{
    if(empty($iddocente) && !empty($nombreasignatura))
{

    $sql='SELECT a.nombre,a.codAsignatura,t.id as tribunalid,ta.id as tribunalasignaturaid,p.id as idpresidente, CONCAT(p.nombre, " ",p.apellido) as presidente,p1.id as idvocal,CONCAT(p1.nombre, " ",p1.apellido)as vocal ,p2.id as idvocal1, CONCAT(p2.nombre, " ",p2.apellido)as vocal1,p3.id as idsuplente,CONCAT(p3.nombre, " ",p3.apellido)as suplente from tribunal t left join tribunalasignatura ta ON t.id=ta.idTribunal left join asignatura a ON a.codAsignatura= ta.idAsignatura left join profesor p on t.presidente = p.id left join profesor p1 on t.vocal = p1.id left join profesor p2 on t.vocal1 = p2.id left join profesor p3 on t.suplente = p3.id
    where a.nombre='."'".$nombreasignatura."'";
$consulta2 = BDConexion::getInstancia()->query($sql);


}

elseif(!empty($iddocente) && !empty($nombreasignatura))
{
  
    
    $sqlnombredocente="SELECT CONCAT(nombre,' ',apellido) as nombre from profesor where id = $iddocente";
    $consultan = BDConexion::getInstancia()->query($sqlnombredocente);
    while($row = $consultan->fetch_array())
    {
     $nombredocente=$row['nombre'];
    }





    $sql='SELECT a.nombre,a.codAsignatura,t.id as tribunalid,ta.id as tribunalasignaturaid,p.id as idpresidente, CONCAT(p.nombre, " ",p.apellido) as presidente,p1.id as idvocal,CONCAT(p1.nombre, " ",p1.apellido)as vocal ,p2.id as idvocal1, CONCAT(p2.nombre, " ",p2.apellido)as vocal1,p3.id as idsuplente,CONCAT(p3.nombre, " ",p3.apellido)as suplente from tribunal t left join tribunalasignatura ta ON t.id=ta.idTribunal left join asignatura a ON a.codAsignatura= ta.idAsignatura left join profesor p on t.presidente = p.id left join profesor p1 on t.vocal = p1.id left join profesor p2 on t.vocal1 = p2.id left join profesor p3 on t.suplente = p3.id
    where (CONCAT(p.nombre,'."'".$espacio."'".',p.apellido)='."'".$nombredocente."'".' OR CONCAT(p1.nombre,'."'".$espacio."'".',p1.apellido)='."'".$nombredocente."'".' OR CONCAT(p2.nombre,'."'".$espacio."'".',p2.apellido)= '."'".$nombredocente."'".' OR CONCAT(p3.nombre,'."'".$espacio."'".',p3.apellido)= '."'".$nombredocente."'".') AND a.nombre='."'".$nombreasignatura."'";
    

    
$consulta2 = BDConexion::getInstancia()->query($sql);


} 
elseif(!empty($iddocente) && empty($nombreasignatura))
{
    $sqlnombredocente="SELECT CONCAT(nombre,' ',apellido) as nombredocente from profesor where id = $iddocente";
    $consultan = BDConexion::getInstancia()->query($sqlnombredocente);
    if ($consultan->num_rows != 0)
    {
    while($row = $consultan->fetch_array())
    {
     $nombredocente=$row['nombredocente'];
    }
    }



    $sql='SELECT a.nombre,a.codAsignatura,t.id as tribunalid,ta.id as tribunalasignaturaid,p.id as idpresidente, CONCAT(p.nombre, " ",p.apellido) as presidente,p1.id as idvocal,CONCAT(p1.nombre, " ",p1.apellido)as vocal ,p2.id as idvocal1, CONCAT(p2.nombre, " ",p2.apellido)as vocal1,p3.id as idsuplente,CONCAT(p3.nombre, " ",p3.apellido)as suplente from tribunal t left join tribunalasignatura ta ON t.id=ta.idTribunal left join asignatura a ON a.codAsignatura= ta.idAsignatura left join profesor p on t.presidente = p.id left join profesor p1 on t.vocal = p1.id left join profesor p2 on t.vocal1 = p2.id left join profesor p3 on t.suplente = p3.id
    where (CONCAT(p.nombre,'."'".$espacio."'".',p.apellido)='."'".$nombredocente."'".' OR CONCAT(p1.nombre,'."'".$espacio."'".',p1.apellido)='."'".$nombredocente."'".' OR CONCAT(p2.nombre,'."'".$espacio."'".',p2.apellido)='."'".$nombredocente."'".' OR CONCAT(p3.nombre,'."'".$espacio."'".',p3.apellido)= '."'".$nombredocente."'".') ';
    
    
}


$consulta2 = BDConexion::getInstancia()->query($sql);
if ($consulta2 !=false)
{
if ($consulta2->num_rows != 0)
{
    $filas='';
    while($row = $consulta2->fetch_array())
    {
      
      if ($row['idsuplente']== NULL && $row['suplente']== NULL )
      {
        $filas.='
        <tr>
        <td>'.$row['nombre'].'</td>
        <td>'.$row['presidente'].'</td>
        <td>'.$row['vocal'].'</td>
        <td>'.$row['vocal1'].'</td>
        <td> </td>
        <td>
            <a title="Ver detalle" href="afectacion.ver.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
                <button type="button" class="btn btn-outline-info">
                    <span class="oi oi-zoom-in"></span>
                </button></a>

        </td>
        <td>
            <a title="Modificar" href="afectacion.modificar.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
                <button type="button" class="btn btn-outline-warning">
                    <span class="oi oi-pencil"></span>
                </button></a>
        </td>
        <td>
            <a title="Eliminar" href="afectacion.eliminar.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
              <button class="btn btn-outline-danger" class="btn btn-outline-info">
                 <span class="oi oi-trash"></span>
              </button></a>
        </td>
    </tr>
        
        
        ';



      }
      else
      {
          $filas.='
          <tr>
          <td>'.$row['nombre'].'</td>
          <td>'.$row['presidente'].'</td>
          <td>'.$row['vocal'].'</td>
          <td>'.$row['vocal1'].'</td>
          <td>'.$row['suplente'].'</td>
         
          <td>
              <a title="Ver detalle" href="afectacion.ver.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
                  <button type="button" class="btn btn-outline-info">
                      <span class="oi oi-zoom-in"></span>
                  </button></a>

          </td>
          <td>
              <a title="Modificar" href="afectacion.modificar.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
                  <button type="button" class="btn btn-outline-warning">
                      <span class="oi oi-pencil"></span>
                  </button></a>
          </td>
          <td>
              <a title="Eliminar" href="afectacion.eliminar.php?id='.$row['tribunalid'].'&id1='.$row['tribunalasignaturaid'].'&id2='.$row['codAsignatura'].'">
                <button class="btn btn-outline-danger" class="btn btn-outline-info">
                   <span class="oi oi-trash"></span>
                </button></a>
          </td>
      </tr>
          


          ';





      }












    }

    $cuerpodiv='
    
    
    <table id="afectaciones" class ="table table-hover table-condensed table-bordered">
    <thead>
    <tr scope="row">
      <td>Asignatura</td>
      <td>Presidente</td>
      <td>Vocal</td>
      <td>Vocal 2</td>
      <td>Suplente</td>
      <td>Ver mas</td>
      <td>Editar</td>
      <td>Eliminar</td>
    </tr>
    </thead>
    '.$filas.'
    
    </table>
    
    
    ';
}
else
{
    $cuerpodiv='<div id="vacio" class="alert alert-warning" role="alert">No se encontraron resultados con los filtros ingresados. <strong>Por favor, intente nuevamente </strong></div>';

}
}
else
{
    $cuerpodiv='<div id="vacio" class="alert alert-warning" role="alert">Error al realizar al consulta. <strong>Por favor, intente nuevamente </strong></div>';   
}







}
echo $cuerpodiv;


?>
