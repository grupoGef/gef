import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ModalPage} from '../../pages/modal/modal.page';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() header;

  constructor( private modalCtrl: ModalController) { }

  ngOnInit() {}

 async mostrarModal(){
    const modal = await this.modalCtrl.create({
      component: ModalPage,
    });
     await modal.present();
  }
}
