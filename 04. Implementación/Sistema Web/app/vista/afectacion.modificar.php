<?php
include_once '../../conf/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_GESTIONARAFECTACION);

include_once '../modelo/ColeccionLicencia.php';
include_once '../modelo/ColeccionTipoLicencia.php';
include_once '../modelo/Tribunal.Class.php';
include_once '../modelo/TribunalAsignatura.Class.php';
include_once '../modelo/Asignatura.Class.php';
include_once '../modelo/Docente.Class.php';
if(isset($_GET["id"]))
{
    $idtribunal=$_GET["id"];

}
if(isset($_GET["id1"]))
{
    $idtribunalasig=$_GET["id1"];
}

if(isset($_GET["id2"]))
{
    $idasignatura=$_GET["id2"];

}
$sql='SELECT a.nombre,a.codAsignatura,t.id as tribunalid,ta.id as tribunalasignaturaid,p.id as idpresidente,p.preferencias as presidentepreferencias ,p.nombre as presidentenombre ,CONCAT(p.nombre, " ",p.apellido) as presidente,p1.id as idvocal,p1.nombre as vocalnombre,p1.preferencias as vocalpreferencias,CONCAT(p1.nombre, " ",p1.apellido)as vocal ,p2.nombre as vocal1nombre,p2.id as idvocal1,p2.preferencias as vocal1preferencias, CONCAT(p2.nombre, " ",p2.apellido)as vocal1,p3.id as idsuplente,p3.nombre as suplentenombre,p3.preferencias as suplentepreferencias,CONCAT(p3.nombre, " ",p3.apellido) as suplente from tribunal t left join tribunalasignatura ta ON t.id=ta.idTribunal left join asignatura a ON a.codAsignatura= ta.idAsignatura left join profesor p on t.presidente = p.id left join profesor p1 on t.vocal = p1.id left join profesor p2 on t.vocal1 = p2.id left join profesor p3 on t.suplente = p3.id
where t.id='.$idtribunal.' and ta.id='.$idtribunalasig.' and a.codAsignatura='.$idasignatura.' 
';

$consulta2 = BDConexion::getInstancia()->query($sql);  



?>
<html>
    <head>
        <meta charset="UTF-8">
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" href="../../lib/select2/dist/css/select2-bootstrap.min.css">
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/select2.js"></script>
    <script type="text/javascript" src="../../lib/select2/dist/js/i18n/es.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Modificar Afectacion</title>
    </head>
    <body>
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="afectacion.modificar.procesar.php" method="post">
               
                <div class="card">
                    <div class="card-header">
                    <p><center><h3>Modificar Afectacion</h3></center></p>
                        
                        <p>
                            Por favor , complete los campos a continuaci&oacute;n.
                            Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                            Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                        </p>
                    </div>
                  
                        <?php

                        
                        while($row = $consulta2->fetch_array())
                        {
                            $datos='';
                          if ($row['idsuplente']== NULL && $row['suplente']== NULL )
                          { ?>
                         <h4 class="card-text">Asignatura</h4>
                        <p> <b><?=$row['nombre'] ?> </b></p>
                 
                     
                        <label for="inputPresidente">Presidente</label>
                        <div class="form-group">
                        <select required='' class="js-example-basic-single form-control" name="presidente">
                            <option  value="<?=$row['idpresidente']?>" selected="selected"><?=$row['presidente']?></option>
                        </select>    
                        </div>

                        <label for="presidentepreferencias">Preferencias de Presidente</label>
                        <div class="form-group">
                        
                        <select  class="form-control " name="presidentepreferencias" >
                        <?=
                        $valor='';
                        if($row['presidentepreferencias'] == NULL)
                        {
                            echo '<option selected="selected" value="">Elegir Dia de preferencia</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['presidentepreferencias'],'Lunes') ==0)
                        {
                          echo  ' <option value="">Elegir Dia de preferencia</option>
                          <option selected="selected" value="Lunes">Lunes</option>
                          <option value="Martes">Martes</option>
                          <option value="Miercoles">Miercoles</option>
                          <option value="Jueves">Jueves</option>
                          <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['presidentepreferencias'],'Martes') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option selected="selected" value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['presidentepreferencias'],'Miercoles') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option selected="selected" value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';

                        }
                        elseif(strcmp($row['presidentepreferencias'],'Jueves') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option selected="selected" value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif($row['presidentepreferencias']== 'Viernes')
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option  value="Jueves">Jueves</option>
                            <option selected="selected" value="Viernes">Viernes</option>';
                        }
                        
                        ?>
                          </select>
                        </div>



                       <label for="inputVocal">Vocal</label>
                        <div class="form-group">
                        <select required='' class="js-example-basic-single form-control" name="vocal">
                            <option  value="<?=$row['idvocal']?>" selected="selected"><?=$row['vocal']?></option>
                        </select>    
                        </div>
                       
                        <label for="inputPresidente">Preferencias de Vocal</label>
                        <div class="form-group">
                        <select  class="form-control " name="vocalpreferencias" >
                        <?=
                        $valor='';
                        if($row['vocalpreferencias'] == NULL)
                        {
                            echo '<option selected="selected" value="">Elegir Dia de preferencia</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocalpreferencias'],'Lunes') ==0)
                        {
                          echo  ' <option value="">Elegir Dia de preferencia</option>
                          <option selected="selected" value="Lunes">Lunes</option>
                          <option value="Martes">Martes</option>
                          <option value="Miercoles">Miercoles</option>
                          <option value="Jueves">Jueves</option>
                          <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocalpreferencias'],'Martes') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option selected="selected" value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocalpreferencias'],'Miercoles') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option selected="selected" value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';

                        }
                        elseif(strcmp($row['vocalpreferencias'],'Jueves') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option selected="selected" value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif($row['vocalpreferencias']== 'Viernes')
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option  value="Jueves">Jueves</option>
                            <option selected="selected" value="Viernes">Viernes</option>';
                        }
                        
                        ?>
                          </select>
                        </div>



                        <label for="inputSegundoVocal">Segundo Vocal</label>
                        <div class="form-group">
                        <select required='' class="js-example-basic-single form-control" name="vocal1">
                            <option  value="<?=$row['idvocal1']?>" selected="selected"><?=$row['vocal1']?></option>
                        </select> 
                        </div>
                      
                         <label for="inputPresidente">Preferencias de Segundo Vocal</label>
                        <div class="form-group">
                        <select  class="form-control " name="vocal1preferencias" >
                        <?=
                        $valor='';
                        if($row['vocal1preferencias'] == NULL)
                        {
                            echo '<option selected="selected" value="">Elegir Dia de preferencia</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocal1preferencias'],'Lunes') ==0)
                        {
                          echo  ' <option value="">Elegir Dia de preferencia</option>
                          <option selected="selected" value="Lunes">Lunes</option>
                          <option value="Martes">Martes</option>
                          <option value="Miercoles">Miercoles</option>
                          <option value="Jueves">Jueves</option>
                          <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocal1preferencias'],'Martes') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option selected="selected" value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocal1preferencias'],'Miercoles') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option selected="selected" value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';

                        }
                        elseif(strcmp($row['vocal1preferencias'],'Jueves') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option selected="selected" value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif($row['vocal1preferencias']== 'Viernes')
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option  value="Jueves">Jueves</option>
                            <option selected="selected" value="Viernes">Viernes</option>';
                        }
                        
                        ?>
                          </select>
                        </div>


                       <label for="inputSuplente">Suplente</label>
                        <div class="form-group">
                        <select  class="js-example-basic-single form-control" name="suplente">
                            
                        </select> 
                        </div>
                      
                         <label for="inputPresidente">Preferencias de Suplente</label>
                        <div class="form-group">
                        <select  class="form-control " name="suplentepreferencias" >
                            <option selected="selected" value="">Elegir Dia de preferencia</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>
                          </select>
                        </div>

                            

                         <?php   
                        }
                        else
                        { ?>


                          
                         <h4 class="card-text">Asignatura</h4>
                        <p> <b><?=$row['nombre'] ?> </b></p>
                 

                        <label for="inputPresidente">Presidente</label>
                        <div class="form-group">
                        <select required='' class="js-example-basic-single form-control" name="presidente">
                            <option  value="<?=$row['idpresidente']?>" selected="selected"><?=$row['presidente']?></option>
                        </select>    
                        </div>

                        <label for="presidentepreferencias">Preferencias de Presidente</label>
                        <div class="form-group">
                        
                        <select  class="form-control " name="presidentepreferencias" >
                        <?=
                        $valor='';
                        if($row['presidentepreferencias'] == NULL)
                        {
                            echo '<option selected="selected" value="">Elegir Dia de preferencia</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['presidentepreferencias'],'Lunes') ==0)
                        {
                          echo  ' <option value="">Elegir Dia de preferencia</option>
                          <option selected="selected" value="Lunes">Lunes</option>
                          <option value="Martes">Martes</option>
                          <option value="Miercoles">Miercoles</option>
                          <option value="Jueves">Jueves</option>
                          <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['presidentepreferencias'],'Martes') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option selected="selected" value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['presidentepreferencias'],'Miercoles') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option selected="selected" value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';

                        }
                        elseif(strcmp($row['presidentepreferencias'],'Jueves') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option selected="selected" value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif($row['presidentepreferencias']== 'Viernes')
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option  value="Jueves">Jueves</option>
                            <option selected="selected" value="Viernes">Viernes</option>';
                        }
                        
                        ?>
                          </select>
                        </div>



                       <label for="inputVocal">Vocal</label>
                        <div class="form-group">
                        <select required='' class="js-example-basic-single form-control" name="vocal">
                            <option  value="<?=$row['idvocal']?>" selected="selected"><?=$row['vocal']?></option>
                        </select>    
                        </div>
                       
                        <label for="inputPresidente">Preferencias de Vocal</label>
                        <div class="form-group">
                        <select  class="form-control " name="vocalpreferencias" >
                        <?=
                        $valor='';
                        if($row['vocalpreferencias'] == NULL)
                        {
                            echo '<option selected="selected" value="">Elegir Dia de preferencia</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocalpreferencias'],'Lunes') ==0)
                        {
                          echo  ' <option value="">Elegir Dia de preferencia</option>
                          <option selected="selected" value="Lunes">Lunes</option>
                          <option value="Martes">Martes</option>
                          <option value="Miercoles">Miercoles</option>
                          <option value="Jueves">Jueves</option>
                          <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocalpreferencias'],'Martes') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option selected="selected" value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocalpreferencias'],'Miercoles') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option selected="selected" value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';

                        }
                        elseif(strcmp($row['vocalpreferencias'],'Jueves') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option selected="selected" value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif($row['vocalpreferencias']== 'Viernes')
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option  value="Jueves">Jueves</option>
                            <option selected="selected" value="Viernes">Viernes</option>';
                        }
                        
                        ?>
                          </select>
                        </div>



                        <label for="inputSegundoVocal">Segundo Vocal</label>
                        <div class="form-group">
                        <select required='' class="js-example-basic-single form-control" name="vocal1">
                            <option  value="<?=$row['idvocal1']?>" selected="selected"><?=$row['vocal1']?></option>
                        </select> 
                        </div>
                      
                         <label for="inputPresidente">Preferencias de Segundo Vocal</label>
                        <div class="form-group">
                        <select  class="form-control " name="vocal1preferencias" >
                        <?=
                        $valor='';
                        if($row['vocal1preferencias'] == NULL)
                        {
                            echo '<option selected="selected" value="">Elegir Dia de preferencia</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocal1preferencias'],'Lunes') ==0)
                        {
                          echo  ' <option value="">Elegir Dia de preferencia</option>
                          <option selected="selected" value="Lunes">Lunes</option>
                          <option value="Martes">Martes</option>
                          <option value="Miercoles">Miercoles</option>
                          <option value="Jueves">Jueves</option>
                          <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocal1preferencias'],'Martes') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option selected="selected" value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['vocal1preferencias'],'Miercoles') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option selected="selected" value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';

                        }
                        elseif(strcmp($row['vocal1preferencias'],'Jueves') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option selected="selected" value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif($row['vocal1preferencias']== 'Viernes')
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option  value="Jueves">Jueves</option>
                            <option selected="selected" value="Viernes">Viernes</option>';
                        }
                        
                        ?>
                          </select>
                        </div>


                       <label for="inputSuplente">Suplente</label>
                        <div class="form-group">
                        <select  class="js-example-basic-single form-control" name="suplente">
                        <option  value="<?=$row['idsuplente']?>" selected="selected"><?=$row['suplente']?></option>    
                        </select> 
                        </div>
                      
                         <label for="inputPresidente">Preferencias de Suplente</label>
                        <div class="form-group">
                        <select  class="form-control " name="suplentepreferencias" >
                        <?=
                        $valor='';
                        if($row['suplentepreferencias'] == NULL)
                        {
                            echo '<option selected="selected" value="">Elegir Dia de preferencia</option>
                            <option value="Lunes">Lunes</option>
                            <option value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['suplentepreferencias'],'Lunes') ==0)
                        {
                          echo  ' <option value="">Elegir Dia de preferencia</option>
                          <option selected="selected" value="Lunes">Lunes</option>
                          <option value="Martes">Martes</option>
                          <option value="Miercoles">Miercoles</option>
                          <option value="Jueves">Jueves</option>
                          <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['suplentepreferencias'],'Martes') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option selected="selected" value="Martes">Martes</option>
                            <option value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif(strcmp($row['suplentepreferencias'],'Miercoles') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option selected="selected" value="Miercoles">Miercoles</option>
                            <option value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';

                        }
                        elseif(strcmp($row['suplentepreferencias'],'Jueves') ==0)
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option selected="selected" value="Jueves">Jueves</option>
                            <option value="Viernes">Viernes</option>';
                        }
                        elseif($row['suplentepreferencias']== 'Viernes')
                        {
                            echo  ' <option value="">Elegir Dia de preferencia</option>
                            <option  value="Lunes">Lunes</option>
                            <option  value="Martes">Martes</option>
                            <option  value"Miercoles" >Miercoles</option>
                            <option  value="Jueves">Jueves</option>
                            <option selected="selected" value="Viernes">Viernes</option>';
                        }
                        
                        ?>
                          </select>
                        </div>

                            


                            <?php
                        }
                   

                         ?>


                         <input type="hidden" name="idt" class="form-control" id="id" value="<?= $idtribunal ?>" >
                         <input type="hidden" name="idp" class="form-control" id="id" value="<?= $idasignatura ?>" >
                         <input type="hidden" name="idv" class="form-control" id="id" value="<?= $idtribunalasig ?>" >

                         
                    </div>
                    <?php
                     }?>
                   
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <span class="oi oi-check"></span> Confirmar
                        </button>
                        <a href="gestionarAfectacion.php">
                            <button type="button" class="btn btn-outline-danger">
                                <span class="oi oi-x"></span> Cancelar
                            </button>
                        </a>
                    </div>
                </div>
            </form>

          

        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
<script>
    $.fn.select2.defaults.set( "theme", "bootstrap" );
    $.fn.select2.defaults.set('language', 'es');




    $('.js-example-basic-single').select2({
             placeholder: 'Ingrese el Profesor',
             language: {
noResults: function() {

 return "No hay resultados";        
},
searching: function() {

 return "Buscando..";
}
},
             ajax: {
                    url: 'buscarnombreprofesor.php',
                    dataType: 'json',
                    delay: 250,
                    data: function (data) {
                   return { 
                       query: data.term // search term
                   };
               },
                   processResults: function (data) {
                  
                       return {
                             results: data
                              };
                   },
                     cache: true
                   }
       });

    $(document.getElementById('#buscarapresidente')).ready(function(){
        $('#buscarpresidente').keyup(function(){
            var query=$(this).val();
            if(query !='')
            {
                $.ajax({
                    url:"buscarnombreprofesortribunal.php",
                    method:"POST",
                    data:{query:query},
                    success:function(data)
                    {

                        $('#listapresidente').fadeIn();
                        $('#listapresidente').html(data);
                    }

                });
            }
            });
         $('#listapresidente').on('click', 'li', function(){  
           $('#buscarpresidente').val($(this).text());  
           $('#listapresidente').fadeOut();  
           }); 
        });
        


</script>




<script>  
function validatevocal() {
    if(document.getElementById('buscarvocal').value == document.getElementById('buscarpresidente').value || document.getElementById('buscarvocal').value == document.getElementById( 'buscarvocal1') || document.getElementById('buscarvocal').value == document.getElementById('buscarsuplente').value ) 
        document.getElementById('buscarvocal').setCustomValidity('El nombre del vocal coincide con otro miembro ');
    else 
        document.getElementById('buscarvocal').style.borderColor="green";
        document.getElementById('buscarvocal').setCustomValidity('');
}
</script>
<script>  
function validatevocal1() {
    if(document.getElementById('buscarvocal1').value == document.getElementById('buscarpresidente').value || document.getElementById('buscarvocal1').value == document.getElementById( 'buscarvocal') || document.getElementById('buscarvocal1').value == document.getElementById('buscarsuplente').value ) 
        document.getElementById('buscarvocal1').setCustomValidity('El nombre del vocal coincide con otro miembro ');
    else 
        document.getElementById('buscarvocal1').setCustomValidity('');
}
</script>
<script>  
function validatesuplente() {
    if(document.getElementById('buscarsuplente').value == document.getElementById('buscarpresidente').value || document.getElementById('buscarsuplente').value == document.getElementById( 'buscarvocal') || document.getElementById('buscarsuplente').value == document.getElementById('buscarvocal1').value ) 
        document.getElementById('buscarsuplente').setCustomValidity('El nombre del suplente coincide con otro miembro ');
    else 
        document.getElementById('buscarsuplente').setCustomValidity('');
}
</script>