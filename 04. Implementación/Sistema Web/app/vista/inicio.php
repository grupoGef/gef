<?php

include_once '../../conf/ControlAcceso.Class.php';





ControlAcceso::requierePermiso(PermisosSistema::PERMISO_INICIO);

$añoactual = date('Y-m-d');




?>

<html>
    <head>
        <!--  Scripts / Links necesarios Pertenecientes al Bootstrap -->
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/bootstrap.css" />
    <link rel="stylesheet" href="../../lib/open-iconic-master/font/css/open-iconic-bootstrap.css" />
    <link rel="stylesheet" href="../../lib/bootstrap-4.1.1-dist/css/uargflow_footer.css" />
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/buttons.dataTables.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="../../lib/dataTables/rowReorder.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../lib/alertifyjs/css/alertify.css" />
    <link rel="stylesheet" href="../../lib/alertifyjs/css/themes/default.css" />
    <script type="text/javascript" src="../../lib/JQuery/jquery-3.3.1.js"></script>
    <script src="../../lib/JQuery/jquery.min.js"></script>
    <script src="../../lib/JQuery/jquery.easing.min.js"></script>
    <script src="../../lib/dataTables/jquery.dataTables.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.buttons.js"></script>
    <script src="../../lib/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="../../lib/dataTables/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="../../lib/bootstrap-4.1.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.min.js"></script>
    <script type="text/javascript" src="../../lib/alertifyjs/alertify.js"></script>
    <script type="text/javascript" src="../../lib/JQuery/validar.js"></script>  
    <!-- <script type="text/javascript" src="../../lib/JQuery/inicio.js"></script>     -->
        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Gestionar Mesa de Examen</title>
    </head>
    <body>

        <?php include_once '../gui/navbar.php'; ?>

<div class="container-fluid "> 
    <div class="container-fluid">
        <div class="row justify-content-around">
          <div class=" col-sm-3">
                <div class="card">
                    <div class="card-header alert-info">
                      Por favor, seleccione un llamado
                    </div>  
                    <div class="card-body">
                    <?php @$NombreMesa = $_POST['NombreMesa'];?>
                        <!-- se crea el formulario "formulario" para poder enviar el ID del LLAMADO al calendario.php-->
                        <form action="calendario.php" method="POST" id="calendar" name="calendar">
                            <!-- se realiza un select para mostrar los tipos de llamados-->
                            <select class="form-control" name="NombreMesa" >

                              
                                    <!-- se obtiene por medio del <opcion> el valor del ID de LLAMADO, y por pantalla se muestra el nombre del LLAMADO  -->
                                    <option value="Febrero-Marzo"  > Febrero-Marzo</option>
                                    <option value="Abril" >Abril </option>
                                    <option value="Mayo" >Mayo </option>
                                    <option value="Junio"  >Junio</option>
                                    <option value="Julio-Agosto"  > Julio-Agosto </option>
                                    <option value="Octubre"  >Octubre </option>
                                    <option value="Diciembre" >Diciembre </option>
                                
                            </select>
                            <br>
                            <input type="submit"  class="btn btn-outline-success" name="reporte"  value="Seleccionar" id="">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header alert-success">
                        <span class="card-title">Ingreso de Fecha</span>
                    </div>
                    <div class="card-body" id="calendarioresult">
                    </div>
                </div>   
            </div>
        </div>    
        <br>
    </div>
</div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>

